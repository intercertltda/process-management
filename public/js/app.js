/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

/*jshint esversion: 6 */
Noty.overrideDefaults({
    callbacks: {
        onTemplate: function onTemplate() {
            if (this.options.subType === 'link') {
                this.barDom.innerHTML = '<div class="noty_body"><p class="noty-reply">' + this.options.text + '</p><a class="text-white" href="' + this.options.link + '">Clique Aqui</a><div>';
            }
        }
    }
});

window.intersig = {
    setMessage: function setMessage(message) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'success';

        new Noty({
            type: type,
            layout: 'topRight',
            theme: 'bootstrap-v4',
            text: message,
            timeout: 3000,
            closeWith: ['click', 'button'],
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight' // Animate.css class names
            }
        }).show();
    },
    sendLink: function sendLink(message, link) {
        var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'success';

        new Noty({
            type: type,
            subType: 'link',
            theme: 'bootstrap-v4',
            layout: 'topRight',
            text: message,
            timeout: 4000,
            link: link,
            closeWith: ['click', 'button'],
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight' // Animate.css class names
            }
        }).show();
    }
};

/*
	Create SLUG from a string
	This function rewrite the string prototype and also
	replace latin and other special characters.

	Forked by Gabriel Froes - https://gist.github.com/gabrielfroes
	Original Author: Mathew Byrne - https://gist.github.com/mathewbyrne/1280286
 */
if (!String.prototype.slugify) {
    String.prototype.slugify = function () {

        return this.toString().toLowerCase().replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
        .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
        .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
        .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
        .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
        .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
        .replace(/[ñÑ]+/g, 'n') // Special Characters #7
        .replace(/[çÇ]+/g, 'c') // Special Characters #8
        .replace(/[ß]+/g, 'ss') // Special Characters #9
        .replace(/[Ææ]+/g, 'ae') // Special Characters #10
        .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
        .replace(/[%]+/g, 'pct') // Special Characters #12
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
    };
}

(function ($) {
    'use strict';

    var clipboard = new ClipboardJS('.copy');

    clipboard.on('success', function (e) {
        window.intersig.setMessage('Item copiado com sucesso!');

        e.clearSelection();
    });

    clipboard.on('error', function (e) {
        window.intersig.setMessage('Não foi possível copiar esse Item.');
    });

    $(window).on('load', function (e) {
        var inner = $('.content-inner'),
            header = $('header.header').innerHeight(),
            footer = $('footer.footer').innerHeight(),
            hWindow = $(window).innerHeight(),
            wWindow = $(window).innerWidth();

        if (wWindow > 758) {
            inner.find('.container-fluid').first().css('minHeight', hWindow - footer - header);
        }
    });

    // ------------------------------------------------------- //
    // Preloader
    // ------------------------------------------------------ //
    $(window).on("load", function () {
        $(".loader").fadeOut();
        $("#preloader").delay(350).fadeOut("slow");
    });

    // ------------------------------------------------------- //
    // Sidebar Functionality
    // ------------------------------------------------------ //
    $('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('active');

        if ($(window).outerWidth() > 1183) {
            if ($('#toggle-btn').hasClass('active')) {
                $('.navbar-header .brand-small').hide();
                $('.navbar-header .brand-big').show();
            } else {
                $('.navbar-header .brand-small').show();
                $('.navbar-header .brand-big').hide();
            }
        }

        if ($(window).outerWidth() < 1183) {
            $('.navbar-header .brand-small').show();
        }
    });
    // Close dropdown after click
    $(function () {
        $(".side-navbar li a").click(function (event) {
            $(".collapse").collapse('hide');
        });
    });

    // ------------------------------------------------------- //
    // Dynamic Height
    // ------------------------------------------------------ //
    $(window).resize(function () {
        var height = $(this).height() - $(".header").height() + $(".main-footer").height();
        $('.d-scroll').height(height);
    });

    $(window).resize();

    // ------------------------------------------------------- //
    // Auto Height Scrollbar
    // ------------------------------------------------------ //
    $(window).resize(function () {
        $('.auto-scroll').height($(window).height() - 130);
    });

    $(window).trigger('resize');

    // ------------------------------------------------------- //
    // Back To Top
    // ------------------------------------------------------ //
    $(function () {
        // Show or hide the sticky footer button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 350) {
                $('.go-top').fadeIn(100);
            } else {
                $('.go-top').fadeOut(200);
            }
        });

        // Animate the scroll to top
        $('.go-top').click(function (event) {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: 0
            }, 800);
        });
    });

    // ------------------------------------------------------- //
    // Custom Checkbox (check, heart, star)
    // ------------------------------------------------------ //
    $('.check-1').click(function () {
        $(this).toggleClass('is-checked');
    });

    // ------------------------------------------------------- //
    // Check / Uncheck all checkboxes
    // ------------------------------------------------------ //
    $("#check-all").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    // ------------------------------------------------------- //
    // Card Close
    // ------------------------------------------------------ //
    $('a.remove').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.col-remove').fadeOut(500);
    });

    // ------------------------------------------------------- //
    // Sidebar Scrollbar
    // ------------------------------------------------------ //
    $(".sidebar-scroll").niceScroll({
        cursorcolor: "transparent",
        cursorborder: "transparent",
        cursoropacitymax: 0,
        boxzoom: false,
        autohidemode: "hidden",
        cursorfixedheight: 80
    });

    // ------------------------------------------------------- //
    // Widget Scrollbar
    // ------------------------------------------------------ //
    $(".widget-scroll").niceScroll({
        railpadding: {
            top: 0,
            right: 3,
            left: 0,
            bottom: 0
        },
        scrollspeed: 100,
        zindex: "auto",
        autohidemode: "leave",
        cursorwidth: "4px",
        cursorcolor: "rgba(52, 40, 104, 0.1)",
        cursorborder: "rgba(52, 40, 104, 0.1)"
    });

    // ------------------------------------------------------- //
    // Table Scrollbar
    // ------------------------------------------------------ //
    $(".table-scroll").niceScroll({
        railpadding: {
            top: 0,
            right: 0,
            left: 0,
            bottom: 0
        },
        scrollspeed: 100,
        zindex: "auto",
        autohidemode: "leave",
        cursorwidth: "4px",
        cursorcolor: "rgba(52, 40, 104, 0.1)",
        cursorborder: "rgba(52, 40, 104, 0.1)"
    });

    // ------------------------------------------------------- //
    // Offcanvas Scrollbar
    // ------------------------------------------------------ //
    $(".offcanvas-scroll").niceScroll({
        railpadding: {
            top: 0,
            right: 2,
            left: 0,
            bottom: 0
        },
        scrollspeed: 100,
        zindex: "auto",
        hidecursordelay: 800,
        cursorwidth: "3px",
        cursorcolor: "rgba(52, 40, 104, 0.1)",
        cursorborder: "rgba(52, 40, 104, 0.1)",
        preservenativescrolling: true,
        boxzoom: false
    });

    // ------------------------------------------------------- //
    // Search Box
    // ------------------------------------------------------ //
    $('#search').on('click', function (e) {
        e.preventDefault();
        $('.search-box').slideDown();
    });
    $('.dismiss').on('click', function () {
        $('.search-box').slideUp();
    });

    // ------------------------------------------------------- //
    // Adding slide effect to dropdown
    // ------------------------------------------------------ //
    $('.dropdown').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
    });

    $('.dropdown').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
    });

    // ------------------------------------------------------- //
    // Options hover effect to dropdown
    // ------------------------------------------------------ //
    $('.widget-options > .dropdown, .actions > .dropdown, .quick-actions > .dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(350);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(350);
    });

    // ------------------------------------------------------- //
    // Offcanvas Sidebar
    // ------------------------------------------------------ //
    $(function () {
        //open
        $('.open-sidebar').on('click', function (event) {
            event.preventDefault();
            $('.off-sidebar').addClass('is-visible');
        });
        //close
        $('.off-sidebar').on('click', function (event) {
            if ($(event.target).is('.off-sidebar') || $(event.target).is('.off-sidebar-close')) {
                $('.off-sidebar').removeClass('is-visible');
                event.preventDefault();
            }
        });
    });

    // ------------------------------------------------------- //
    // Close Modal After Time Period
    // ------------------------------------------------------ //
    $(function () {
        $('#delay-modal').on('show.bs.modal', function () {
            var myModal = $(this);
            clearTimeout(myModal.data('hideInterval'));
            myModal.data('hideInterval', setTimeout(function () {
                myModal.modal('hide');
            }, 2500));
        });
    });
})(jQuery);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);