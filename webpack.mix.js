let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix

    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/system.js', 'public/js/system.min.js')
    .js('resources/assets/js/ordering.js', 'public/js/ordering.min.js')
    .js('resources/assets/js/manager.js', 'public/js/phases-process.min.js')
    .js('resources/assets/js/tables.js', 'public/js/tables.min.js')
    .js('resources/assets/js/more-fields.js', 'public/js/more-fields.min.js')
    .js('resources/assets/js/uploads.js', 'public/js/uploads.min.js')


    .sass('resources/assets/sass/app.scss', 'public/css/app.min.css')



;
