#!/usr/bin/env bash

php artisan module:migrate Clients --database=system
php artisan module:migrate Permissions --database=system --seed
php artisan module:migrate Users --database=system --seed
php artisan module:migrate Settings --database=system
php artisan module:migrate Notifications --database=system
