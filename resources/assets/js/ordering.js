/*jshint esversion: 6 */
(($) => {
    'use strict';

    const listSortable = $('ul.js-sortable');

    let adjustment;

    $(window).on('load', () => {
        listSortable.css('minHeight', listSortable.innerHeight());
    });

    listSortable.sortable({
        group: 'no-drop',
        handle: 'i.js-sort',
        pullPlaceholder: true,
        onDragStart: ($item, container, _super) => {
            // Duplicate items of the no drop area
            if(!container.options.drop) {
                $item.clone().insertAfter($item);
            }

            _super($item, container);

            var offset = $item.offset(),
                pointer = container.rootGroup.pointer;

            adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
            };

            _super($item, container);

        },
        // animation on drop
        onDrop: ($item, container, _super) => {
            const $clonedItem = $('<li/>').css({height: 0});
            $item.before($clonedItem);

            $item.animate($clonedItem.position(), () => {
                $clonedItem.detach();
                _super($item, container);
            });

            const relation = [];

            window.Pace.restart();

            $.each(listSortable.find('li.list-group-item'), (i, item) => {
                const $new = $(item);

                relation.push({
                    reference: $new.data('reference'),
                    index: i
                });

            });

            $.ajax({
                url: $('[name=api-url]').attr('content') + '/' + listSortable.data('type')  + '/ordenacao',
                method: 'POST',
                data: {
                    items: relation
                },
                success: (response) => {
                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: response.message,
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();

                    $.each(listSortable.find('li.list-group-item'), (i, item) => {
                        const $ordered = $(item);

                        $ordered.find('.js-sortable-index').text(i + 1);
                    });
                },
                error: (res, xhr, status) => {
                    const response = res.responseJSON;
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: response.message,
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }
            });
        },

        onDrag: ($item, position) => {
            $item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
            });

            $('.js-sortable li.placeholder').css({
                minHeight: listSortable.find('.list-group-item').innerHeight(),
                background: '#A6BDBF'
            });
        }
    });

    $('body').on('click', '.js-status', (e) => {
        e.preventDefault();
        const element = $(e.currentTarget),
            parent = element.parents('li.list-group-item'),
            status = parent.data('status');

        const getConfirm = status === 'on' ? 'Deseja desabilitar esse item?' : 'Deseja ativar esse item?';
        
        if (confirm(getConfirm)) {
            $.ajax({
                url: $('[name=api-url]').attr('content') + '/' + listSortable.data('type') + '/status',
                method: 'POST',
                data: {
                    reference: parent.data('reference'),
                    status: status
                },
                success: (response) => {
                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: response.message,
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();

                    setTimeout(() => {
                        window.location.reload();
                    }, 500);
                },
                error: (res, xhr, status) => {
                    const response = res.responseJSON;
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: response.message,
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }
            });
        }
    });
})(jQuery);