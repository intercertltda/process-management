(($) => {
    'use strict';

    const translation = {
        emptyTable: "Nenhum registro encontrado",
        info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        infoEmpty: "Mostrando 0 até 0 de 0 registros",
        infoFiltered: "(Filtrados de _MAX_ registros)",
        infoPostFix: "",
        lengthMenu: "_MENU_ resultados por página",
        loadingRecords: "Carregando...",
        processing: "Processando...",
        zeroRecords: "Nenhum registro encontrado",
        search: "Pesquisar",
        paginate: {
            next: "Próximo",
            previous: "Anterior",
            first: "Primeiro",
            last: "Último"
        },
        aria: {
            sortAscending: ": Ordenar colunas de forma ascendente",
            sortDescending: ": Ordenar colunas de forma descendente"
        }
    };

    // ------------------------------------------------------- //
    // Auto Hide
    // ------------------------------------------------------ //

    const SortingTable = $('#sorting-table');

    SortingTable.DataTable({
        lengthMenu: [
            [10, 15, 20, -1],
            [10, 15, 20, "All"]
        ],
        language: translation
    });

    $('#export-table').DataTable({
        dom: 'Bfrtip',
        language: translation,
        buttons: {
            buttons: [{
                extend: 'copy',
                text: 'Copiar',
                title: $('h1').text(),
                exportOptions: {
                    columns: ':not(.no-print)'
                },
                footer: true
            },{
                extend: 'excel',
                text: 'Planilha',
                title: $('h1').text(),
                exportOptions: {
                    columns: ':not(.no-print)'
                },
                footer: true
            },{
                extend: 'csv',
                text: 'Csv',
                title: $('h1').text(),
                exportOptions: {
                    columns: ':not(.no-print)'
                },
                footer: true
            },{
                extend: 'pdf',
                text: 'Pdf',
                title: $('h1').text(),
                exportOptions: {
                    columns: ':not(.no-print)'
                },
                footer: true
            },{
                extend: 'print',
                text: 'Imprimir',
                title: $('h1').text(),
                exportOptions: {
                    columns: ':not(.no-print)'
                },
                footer: true,
                autoPrint: true
            }],
            dom: {
                container: {
                    className: 'dt-buttons'
                },
                button: {
                    className: 'btn btn-primary btn-square'
                }
            }
        }
    });

})(jQuery);