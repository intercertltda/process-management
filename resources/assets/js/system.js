/*jshint esversion: 6 */

(($) => {
    'use strict';

    $('.toggle-group').find('.toggle-handle').addClass('btn-square');

    const ConfirmAction = $('.js-confirm-action');

    ConfirmAction.on('click', (e) => {
        const $this = $(e.currentTarget);

        if (confirm('Você deseja realmente deletar esse item?')) {
            $.ajax({
                url: $this.data('local') + '/' + $this.data('reference'),
                method: 'DELETE',
                data: {
                    test: 'test',
                },
                success: (response) => {
                    window.intersig.setMessage(response.message);

                    $this.parents('tr').hide();

                    setTimeout((e) => {
                        $this.parents('tr').remove();
                    }, 700);
                },
                error: (err) => {
                    window.intersig.setMessage(err.message, 'error');
                }
            });
        }

        return false;
    });


})(jQuery);
