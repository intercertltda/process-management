/*jshint esversion: 6 */
(($) => {
    'use strict';

    const selectCompany = $('select.js-select-companies'),
        selectCollaborators = $('select.collaborators'),
        selectDepartments = $('select.department'),
        selectDepends = $('select.depend');

    const selectCompanies = (reference) => {
        $.ajax({
            url: $('[name=api-url]').attr('content') + '/empresas/' + reference + '/data',
            success: (response) => {
                window.intersig.setMessage(response.message);

                selectCollaborators
                    .selectpicker('destroy')
                    .find('option')
                    .remove()
                    .end();

                selectDepartments
                    .selectpicker('destroy')
                    .find('option')
                    .remove()
                    .end();

                selectDepends
                    .selectpicker('destroy')
                    .find('option')
                    .remove()
                    .end();

                $.each(response.collaborators, (c, collaborator) => {
                    let op = $('<option />');

                    op.attr('value', c).text(collaborator);

                    selectCollaborators
                        .append(op)
                        .removeAttr('disabled')
                        .selectpicker('refresh');
                });

                $.each(response.departments, (d, department) => {
                    let op = $('<option />');

                    op.attr('value', d).text(department);

                    selectDepartments
                        .append(op)
                        .removeAttr('disabled')
                        .selectpicker('refresh');
                });

                $.each(response.depends, (d, depend) => {
                    let op = $('<option />');

                    op.attr('value', d).text(depend);

                    selectDepends
                        .append(op)
                        .removeAttr('disabled')
                        .selectpicker('refresh');
                });

                if (response.depends.length === 0) {
                    selectDepends.attr('disabled', 'disabled');
                }

                selectDepends.selectpicker('refresh');
            },
            error: (res, xhr, status) => {
                const response = res.responseJSON;

                selectCollaborators
                    .attr('disabled', 'disabled')
                    .selectpicker('refresh')
                    .find('option')
                    .remove()
                    .end();

                selectDepartments
                    .attr('disabled', 'disabled')
                    .selectpicker('refresh')
                    .find('option')
                    .remove()
                    .end();

                selectDepends
                    .attr('disabled', 'disabled')
                    .selectpicker('refresh')
                    .find('option')
                    .remove()
                    .end();

                if (response.type === 'link') {
                    window.intersig.sendLink(response.message, response.link, 'error');
                } else {
                    window.intersig.setMessage(response.message, 'error');
                }
            }
        });
    };

    if (selectCompany.val()) {
        selectCompanies(selectCompany.val());
    } else {
        selectCompany.on('change', (e) => {
            e.preventDefault();
            let reference = $(e.currentTarget).val();

            selectCompanies(reference);
        });
    }

})(jQuery);

