/*jshint esversion: 6 */
(($) => {
    'use strict';

    const fileUpload = $('#fileupload');

    fileUpload.fileupload({
        url: '/api/uploads',
        dataType: 'json',
        done: (e, data) => {
            const image = data.result.image;

            let item = '<li class="list-group-item d-flex justify-content-between align-items-center"><a style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; padding-right: 15px" href="' + image.url + '" target="_blank">'+ image.originalName +'</a><a href="#" class="badge badge-primary badge-pill js-remove-upload" data-reference="'+ image.reference +'">Excluir</a></li>';

            window.intersig.setMessage(data.result.message);

            $('#uploadList').append(item);

            fileUpload.val('');
        },
        error: (e) => {
            let data = e.responseJSON;

            window.intersig.setMessage(data.message, 'error');
        },
        progressall: (e, data) => {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');


    $('body').on('click', '.js-remove-upload', (e) => {
        const element = $(e.currentTarget),
            reference = element.data('reference'),
               parent = element.parents('li');

        if (confirm('Deseja realmente remover esse arquivo?')) {
            $.ajax({
                url: '/api/uploads/' + reference,
                method: 'DELETE',
                dataType: 'json',

                success: (response) => {
                    window.intersig.setMessage(response.message);

                    parent.remove();
                },

                error: (err) => {
                    window.intersig.setMessage(err.responseJSON.message, 'error');
                }
            });
        }
    });

})(jQuery);
