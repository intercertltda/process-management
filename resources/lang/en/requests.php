<?php

return [
    'success' => [
        'created' => 'Successfully created content.',
        'updated' => 'Successfully updated content.',
        'deleted' => 'Successfully deleted content.',
    ],
    'fail' => [
        'created' => 'Content could not be created.',
        'updated' => 'Content could not be updated.',
        'deleted' => 'Content could not be deleted.',
    ],

    'errors' => [
        'exception' => 'Something went wrong.',
    ],

    'notfound' => 'Content not found.',
];
