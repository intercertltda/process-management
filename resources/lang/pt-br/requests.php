<?php

return [
    'success' => [
        'created' => 'Conteúdo criado com sucesso!',
        'updated' => 'Conteúdo atualizado com sucesso!',
        'deleted' => 'Conteúdo deletado com sucesso!',
    ],
    'fail' => [
        'created' => 'Não foi possível criar o conteúdo!',
        'updated' => 'Não foi possível atualizar o conteúdo!',
        'deleted' => 'Não foi possível deletar o conteúdo!',
    ],

    'errors' => [
        'exception' => 'Algo deu errado!',
    ],

    'notfound' => 'Conteúdo não foi encontrado.',
];
