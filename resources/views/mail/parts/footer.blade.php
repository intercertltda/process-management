<!--Email Footer : BEGIN -->
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
    <tr>
        <td style="padding: 40px 10px; color: #777 !important; font-family: sans-serif; font-size: 12px; line-height: 140%; text-align: center; color: #888888;" class="x-gmail-data-detectors">
            <p>
                <a href="https://www.facebook.com/intercert.tecnologia" title="InterCert - Facebook" target="_blank" style="cursor: pointer;">facebook</a> -
                <a href="https://www.instagram.com/intercert.tecnologia" title="InterCert - Instagram" target="_blank" style="cursor: pointer;">intagram</a>
            </p>
            <address>
            </address>
            <br>
            <a href="mailto:contato@intercert.com.br">contato@intercert.com.br</a>
            @if(isset($unsubscribe))
            <unsubscribe style="color: #888888; text-decoration: underline;">{{ __('Unsubscribe') }}</unsubscribe>
            @endif
        </td>
    </tr>
</table>
<!-- Email Footer : END
