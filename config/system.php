<?php

return [
    'name' => 'InterSIG - Gestão de Processos',

    'prefix' => 'painel',

    'menu' => [
        'dashboard' => [
            'url'   => '/',
            'route' => 'dashboard.index',
            'icon'  => 'la la-columns',
            'text'  => 'Dashboard',
            'type'  => 'link',

            'check' => 'permission',
            'param' => ['panel'],

            'show' => ['app', 'tenancy'],
        ],

        'workflow' => [
            'route' => 'workflow.index',
            'icon'  => 'la la-building-o',
            'text'  => 'Workflow',
            'type'  => 'link',

            'check' => 'role',
            'param' => ['directors', 'responsible'],

            'show' => ['tenancy'],
        ],

        'clients' => [
            'route' => 'clients.index',
            'icon'  => 'la la-puzzle-piece',
            'text'  => 'Clients',
            'type'  => 'items',

            'check' => 'role',
            'param' => ['admin', 'directors', 'responsible'],

            'show' => ['app'],

            'items' => [
                'clients.index' => [
                    'route' => 'clients.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Listing',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'clients.create' => [
                    'route' => 'clients.create',
                    'icon'  => 'la la-plus',
                    'text'  => 'Create',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],

        'phases' => [
            'route' => 'phases.index',
            'icon'  => 'la la-puzzle-piece',
            'text'  => 'Phases',
            'type'  => 'items',

            'check' => 'role',
            'param' => ['admin', 'directors', 'responsible'],

            'show' => ['tenancy'],

            'items' => [
                'phases.index' => [
                    'route' => 'phases.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Listing',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'phases.create' => [
                    'route' => 'phases.create',
                    'icon'  => 'la la-plus',
                    'text'  => 'Create',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],
        'viability'     => [
            'route' => 'viability.index',
            'icon'  => 'la la-list-alt',
            'text'  => 'Viability',
            'type'  => 'items',

            'check' => 'role',
            'param' => ['responsible', 'admin'],

            'show' => ['tenancy'],

            'items' => [
                'viability.index' => [
                    'route' => 'viability.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Listing',
                    'type'  => 'link',

                    'check' => 'role',
                    'param' => ['admin', 'responsible'],
                ],

                'viability.create' => [
                    'route' => 'viability.create',
                    'icon'  => 'la la-plus',
                    'text'  => 'Create',
                    'type'  => 'link',

                    'check' => 'role',
                    'param' => ['admin', 'responsible'],
                ],

                'viability.responses' => [
                    'route' => 'viability.responses',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Responses',
                    'type'  => 'link',

                    'check' => 'role',
                    'param' => ['responsible', 'admin'],
                ],
            ],
        ],
        'departments'   => [
            'route' => 'departments.index',
            'icon'  => 'la la-building',
            'text'  => 'Departments',
            'type'  => 'items',

            'check' => 'role',
            'param' => ['responsible', 'admin'],

            'show' => ['tenancy'],

            'items' => [
                'departments.index' => [
                    'route' => 'departments.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Listing',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'departments.create' => [
                    'route' => 'departments.create',
                    'icon'  => 'la la-plus',
                    'text'  => 'Create',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],
        'branches'      => [
            'route' => 'branches.index',
            'icon'  => 'la la-building',
            'text'  => 'Branches',
            'type'  => 'link',

            'check' => 'role',
            'param' => ['responsible', 'admin', 'directors'],

            'show' => ['tenancy'],
        ],
        'leads'         => [
            'route' => 'leads.index',
            'icon'  => 'la la-users',
            'text'  => 'Leads',
            'type'  => 'items',
            'check' => 'role',
            'param' => ['admin', 'marketing', 'commercial'],

            'show' => ['tenancy'],

            'items' => [
                'leads.index'  => [
                    'route' => 'leads.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Listing',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
                'leads.create' => [
                    'route' => 'leads.create',
                    'icon'  => 'la la-plus',
                    'text'  => 'Create',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],
        'users'         => [
            'route' => 'users.index',
            'icon'  => 'la la-users',
            'text'  => 'Collaborators',
            'type'  => 'items',
            'check' => 'role',
            'param' => ['directors', 'admin'],

            'show' => ['app', 'tenancy'],

            'items' => [
                'users.index' => [
                    'route' => 'users.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => 'Listing',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'users.create' => [
                    'route' => 'users.create',
                    'icon'  => 'la la-plus',
                    'text'  => 'Create',
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['admin'],
                ],
            ],
        ],
        'settings'      => [
            'route' => 'settings.index',
            'icon'  => 'la la-cog',
            'text'  => 'Settings',
            'type'  => 'items',
            'check' => 'role',
            'param' => ['directors', 'admin'],
            'show'  => ['app', 'tenancy'],
            'items' => [
                'settings.index' => [
                    'route'        => 'settings.index',
                    'route-params' => [
                        'setting' => 'tipos-de-documentos',
                    ],
                    'icon'         => 'la la-list-ul',
                    'text'         => 'Processes',
                    'type'         => 'link',

                    'check' => 'role',
                    'param' => ['admin'],
                ],
            ],
        ],
    ],

    'settings' => [
        'pages' => [
            [
                'slug' => 'tipos-de-documentos',
                'key'  => 'settings.processes.type.documents',
                'text' => 'Type of documents',
            ],
        ],
    ],
];
