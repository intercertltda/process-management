<?php

namespace App\Traits;


trait Token
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->token = str_random();
        });
    }
}
