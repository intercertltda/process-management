<?php

namespace App\Traits;

trait ModelHelpers
{
    /**
     * @param $query
     * @param $reference
     *
     * @return mixed
     */
    public static function scopeReference($query, $reference)
    {
        return $query
            ->where('reference', $reference)
            ->first();
    }

    /**
     * @param $query
     * @param $token
     *
     * @return mixed
     */
    public static function scopeToken($query, $token)
    {
        return $query
            ->where('token', $token)
            ->first();
    }

    /**
     * @param $query
     * @param $token
     *
     * @return mixed
     */
    public static function scopeSlug($query, $token)
    {
        return $query
            ->where('slug', $token)
            ->first();
    }
}
