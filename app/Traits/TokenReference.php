<?php

namespace App\Traits;


trait TokenReference
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->token = str_random();
            $model->reference = uuid();
        });
    }
}
