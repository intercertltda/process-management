<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputArgument;

class CompanyTenancy extends Command
{
    /**
     * The name of argument name.
     *
     * @var string
     */
    protected $argumentName = 'company';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenancy:make {company}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new tenancy database for companies.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['company', InputArgument::REQUIRED, 'The name of company will be created.'],
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $default = database_path('database.sqlite');
        $new = database_path(sprintf('companies/%s.sqlite', $this->argument('company')));

        if (File::exists($default) && !File::exists($new) && File::copy($default, $new)) {
            $this->info(sprintf('Banco de dados criado com sucesso!'));
        } else {
            $this->error('Não foi possível criar o banco de dados, a base de dados já existe! ');
        }
    }
}
