<?php

namespace App\Classes;

use Modules\Raank\Facades\Collection as Collecting;

class Collection extends Collecting
{
    /**
     * @param $items
     * @param string $key
     * @param string $value
     *
     * @return array
     */
    public static function setKey($items, string $key, string $value): array
    {
        foreach ($items as $item) {
            $list[$item->{$key}] = $item->{$value};
        }

        return $list ?? [];
    }
}
