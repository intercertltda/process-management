<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Modules\Clients\Entities\Client;

class Tenancy
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param array                    $only
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$only)
    {
        $standard = ['api', 'app'];


        if (env('APP_ENV') !== 'testing') {// route only to subdomain
            if (!in_array('all', $only)) {
                if (!in_array($request->subdomain, $only)) {
                    abort(404);
                }
            }

            // check if company exists
            if (!in_array($request->subdomain, $standard)) {
                $clients = Client::where('slug', $request->subdomain)->first();
                if ($clients) {
                    $drive = 'tenancy';
                    $fileDB = database_path(sprintf('companies/%s.sqlite', $request->subdomain));
                } else {
                    abort(404);
                }
            } else {
                $drive = 'system';
                $fileDB = database_path('system.sqlite');
            }

            if (File::exists($fileDB)) {
                DB::disconnect();
                Config::set('database.default', $drive);
                Config::set(sprintf('database.connections.%s.database', $drive), $fileDB);
                DB::reconnect();
            } else {
                abort(404);
            }
        }

        dd(config('database.connections.system'));

        return $next($request);
    }
}
