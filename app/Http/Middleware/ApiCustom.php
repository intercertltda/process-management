<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate;

class ApiCustom
{
    /**
     * Handle an incoming request.
     *
     * @param          $request
     * @param \Closure $next
     *
     * @throws \Exception
     *
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') === 'testing') {
            return $next($request);
        } else {
            return app(Authenticate::class)->handle($request, function ($request) use ($next) {
                //Then process the next request if every tests passed.
                return $next($request);
            });
        }
    }
}
