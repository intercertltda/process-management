<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Modules\Companies\Entities\Company;
use Monolog\Handler\IFTTTHandler;
use ZipArchive;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $zip = new ZipArchive();

        $filename = sprintf('companies-%s.zip', now()->format('Ymd'));
        $dir = sprintf('%s/%s', date('Y'), date('m'));
        $path = storage_path(sprintf('app/backups/%s/%s', $dir, $filename));

        if (!Storage::disk('backups')->allDirectories($dir)) {
            Storage::disk('backups')->makeDirectory($dir);
        }

        /**
         * Making zip file
         */
        if ($zip->open($path, ZipArchive::CREATE) === true) {
            foreach (Company::whereNotNull('slug')->get() as $company) {
                $companyDB = sprintf('database/comanies/%s.sqlite');
                if (File::exists(database_path($companyDB))) {
                    $zip->addFile($companyDB, $company->slug . '.sqlite');
                }
            }

            $zip->addFile(database_path('database.sqlite'), 'database.sqlite');

            $zip->close();
        }

        return $this->view('mail.tests')
            ->attach($path, [
                'as' => $filename,
                'mime' => 'application/octet-stream'
            ]);
    }
}
