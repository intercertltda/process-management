<?php

namespace App\Observers;

use Illuminate\Support\Facades\Artisan;
use Modules\Clients\Entities\Client;

class ClientObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param \Modules\Clients\Entities\Client $client
     *
     * @return void
     */
    public function created(Client $client)
    {
        Artisan::call('tenancy:make', [
            'company' => $client->slug
        ]);


        // Make charge
        // Send notification
    }

    /**
     * Handle the user "updated" event.
     *
     * @param \Modules\Clients\Entities\Client $client
     *
     * @return void
     */
    public function updated(Client $client)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param \Modules\Clients\Entities\Client $client
     *
     * @return void
     */
    public function deleted(Client $client)
    {
    }

    /**
     * Handle the user "restored" event.
     *
     * @param \Modules\Clients\Entities\Client $client
     *
     * @return void
     */
    public function restored(Client $client)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param \Modules\Clients\Entities\Client $client
     *
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        //
    }
}
