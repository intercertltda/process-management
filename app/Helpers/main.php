<?php
/**
 * Function of helpers for this system.
 */

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Modules\System\Facades\HelpersFacade;
use Webpatser\Uuid\Uuid;

if (!function_exists('api_url')) {
    /**
     * Function for fixing bug in URL on env testing.
     *
     * @param string $path
     *
     * @return string
     */
    function api_url(string $path)
    {
        $env = config('app.env');
        $port = request()->server('SERVER_PORT');
        $api = config('app.api');
        $separator = substr($path, 0, 1) === '/';

        // fix bug of url in application env testing
        $apiTesting = str_replace('api:'.$port, 'api', $api);

        $uri = sprintf('%s%s%s', $env === 'testing' ? $apiTesting : $api, !$separator ? '/' : '', $path);

        return $uri;
    }
}

if (!function_exists('str_faker_location')) {
    /**
     * @return array
     */
    function str_faker_location()
    {
        $states = toArray(states()->estados);
        $total = count($states);
        $rand = rand(0, ($total - 1));
        $get = $states[$rand];

        $totalCities = count($get['cidades']);
        $randCities = rand(0, ($totalCities - 1));
        $getCity = $get['cidades'][$randCities];

        return [
            'state' => $get['sigla'],
            'city'  => $getCity,
        ];
    }
}

if (!function_exists('uuid')) {
    /**
     * @throws Exception
     *
     * @return string
     */
    function uuid()
    {
        return Uuid::generate(5, rand(0, 1000).'-'.date('Y-m-d H:i:s').'-'.rand(0, 10000), Uuid::NS_DNS)->string;
    }
}

if (!function_exists('app_title')) {
    /**
     * @param string|null $title
     * @param string|null $subtitle
     * @param string      $separator
     *
     * @return \Illuminate\Config\Repository|mixed|string
     */
    function app_title(string $title = null, string $subtitle = null, string $separator = '|')
    {
        $name = config('system.name');

        if (!isset($title)) {
            return $name;
        }

        $string = sprintf('%s %s', $title, $separator);

        if (isset($subtitle)) {
            $string .= sprintf(' %s %s', $subtitle, $separator);
        }

        return sprintf('%s %s', $string, $name);
    }
}

if (!function_exists('get_route')) {
    /**
     * @param string $route
     * @param string $default
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function get_route(string $route, string $default = '/')
    {
        if (\Route::has($route)) {
            return route($route, ['subdomain' => request()->subdomain]);
        } else {
            return url($default);
        }
    }
}

if (!function_exists('in_route')) {
    /**
     * @param string $check
     *
     * @return bool
     */
    function in_route(string $check)
    {
        return str_contains(\Route::currentRouteName(), $check);
    }
}

if (!function_exists('caching')) {
    /**
     * @return \Modules\System\Facades\CachingFacade
     */
    function caching()
    {
        return new \Modules\System\Facades\CachingFacade();
    }
}

if (!function_exists('collection')) {
    function collection($data)
    {
        return new \Modules\System\Facades\CollectionFacade($data);
    }
}

if (!function_exists('is_collect')) {
    /**
     * @param $collection
     *
     * @return bool
     */
    function is_collect($collection)
    {
        return is_object($collection) or is_array($collection);
    }
}

if (!function_exists('is_string_int')) {
    /**
     * @param $str
     *
     * @return bool
     */
    function is_string_int($str)
    {
        return is_string($str) or is_int($str);
    }
}

if (!function_exists('array_search_recursive')) {
    /**
     * @param       $needle
     * @param array $array
     *
     * @return bool
     */
    function array_search_recursive($needle, array $array)
    {
        foreach ($array as $a => $b) {
            if (is_string_int($b)) {
                if (str_search($b, $needle)) {
                    return true;
                }
            } elseif (is_collect($b)) {
                return array_search_recursive($needle, $b);
            }
        }

        return false;
    }
}

if (!function_exists('simple_search')) {
    /**
     * @param $array
     * @param $needle
     *
     * @return bool
     */
    function simple_search($array, $needle): bool
    {
        if (is_string_int($array)) {
            if (str_search($array, $needle)) {
                return true;
            }
        } elseif (is_collect($array)) {
            foreach ($array as $c => $d) {
                if (is_string_int($d)) {
                    if (str_search($d, $needle)) {
                        return true;
                    }
                } elseif (is_collect($d)) {
                    foreach ($d as $e => $f) {
                        if (is_string_int($f) && str_search($f, $needle)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}

if (!function_exists('url_replace')) {
    /**
     * @param string|null $removal
     * @param array|null  $appends
     *
     * @return string
     */
    function url_replace(string $removal = null, array $appends = null, string $url = null): string
    {
        if (isset($url)) {
            $queryString = str_replace([request()->url(), '?'], '', request()->fullUrl());
        } else {
            $queryString = request()->getQueryString();
        }

        $items = explode('&', $queryString);

        if (isset($items)) {
            foreach ($items as $item) {
                $queryEx = explode('=', $item);
                if (!empty($item) && isset($queryEx)) {
                    $queries[$queryEx[0]] = $queryEx[1];
                }
            }
        }

        if (isset($queries)) {
            if (isset($queries[$removal])) {
                unset($queries[$removal]);
            }

            if (isset($appends)) {
                $queries = array_merge($queries, $appends);
            }

            $queries = http_build_query($queries);
            $uri = sprintf('%s?%s', request()->url(), $queries);
        }

        return $uri ?? request()->url();
    }
}

if (!function_exists('helpers')) {
    /**
     * @return HelpersFacade
     */
    function helpers(): HelpersFacade
    {
        return new HelpersFacade();
    }
}

if (!function_exists('get_setting')) {
    /**
     * @param string   $key
     * @param int|null $company
     *
     * @return mixed
     */
    function get_setting(string $key, int $company = null)
    {
        $find = new \Modules\Settings\Facades\FindSetting($key, $company);

        return $find->get() ?? null;
    }
}

if (!function_exists('router')) {
    /**
     * @param string $section
     * @param string $namespace
     *
     * @return \Modules\System\Facades\Router
     */
    function router(string $section = null, string $namespace = null)
    {
        return new \Modules\System\Facades\Router($section, $namespace);
    }
}

if (!function_exists('api')) {
    /**
     * @return \Modules\System\Facades\ApiRouterFacade
     */
    function api()
    {
        return new \Modules\System\Facades\ApiRouterFacade();
    }
}

if (!function_exists('is_app')) {
    /**
     * @return bool
     */
    function is_app(): bool
    {
        return router()->type() === 'app';
    }
}

if (!function_exists('is_tenancy')) {
    /**
     * @return bool
     */
    function is_tenancy(): bool
    {
        return router()->type() === 'tenancy';
    }
}

if (!function_exists('is_api')) {
    /**
     * @return bool
     */
    function is_api(): bool
    {
        return router()->type() === 'api';
    }
}

if (!function_exists('is_subdomain')) {
    /**
     * @param string|null $is
     *
     * @return bool
     */
    function is_subdomain(string $is = null)
    {
        return isset(request()->subdomain);
    }
}
