<?php
/**
 * File for check user.
 */
if (!function_exists('is_admin')) {
    /**
     * Check user if is admin.
     *
     * @return mixed
     */
    function is_admin()
    {
        $user = request()->user();

        return $user->hasRole('admin');
    }
}

if (!function_exists('is_director')) {
    /**
     * @return mixed
     */
    function is_director()
    {
        $user = request()->user();

        return $user->hasRole('directors');
    }
}

if (!function_exists('is_partner')) {
    /**
     * @return bool
     */
    function is_partner()
    {
        return request()->user()->type === 'user';
    }
}

if (!function_exists('has_role')) {
    /**
     * @param $role
     *
     * @return bool
     */
    function has_role($role)
    {
        $roles = config('permissions.list');
        $user = request()->user();

        if (is_array($role)) {
            return $user->hasAnyRole($role);
        }

        if (!isset($roles[$role])) {
            return false;
        }

        return $user->hasRole($role);
    }
}

if (!function_exists('has_perms')) {
    /**
     * @param $perms
     *
     * @return mixed
     */
    function has_perms($perms)
    {
        $user = request()->user();

        if (is_array($perms)) {
            return $user->hasAnyPermission($perms);
        }

        return $user->hasPermissionTo($perms);
    }
}

if (!function_exists('is_agent')) {
    /**
     * @return mixed
     */
    function is_agent()
    {
        $user = request()->user();

        return $user->hasRole('agent');
    }
}

if (!function_exists('is_check')) {
    /**
     * @param        $param
     * @param string $type
     *
     * @return bool|mixed
     */
    function is_check($param, string $type)
    {
        switch ($type) {
            case 'role':
                return has_role($param);
                break;
            case 'permission':
                return has_perms($param);
                break;
            case 'type':
                switch ($param) {
                    case 'partner':
                        return is_partner();
                        break;
                    default:
                        return is_admin();
                        break;
                }
                break;
            default:
        }

        return false;
    }
}
