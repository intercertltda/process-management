<?php

if (!function_exists('tenant'))
{
    function tenant()
    {
        return new \Modules\Tenancy\Facades\TenancyFacade();
    }
}
