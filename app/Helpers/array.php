<?php

if (!function_exists('array_find')) {
    /**
     * @param             $haystack
     * @param             $needle
     * @param string|null $inColumn
     *
     * @return array
     */
    function array_find($haystack, $needle, string $inColumn = null)
    {
        // 1 - array principal
        foreach ($haystack as $a => $b) {
            if (is_collect($b)) {
                // 2 - array with named keys
                foreach ($b as $c => $d) {
                    if (is_collect($d)) {
                        // 3 - array of data and named keys or with collection
                        if (isset($inColumn)) {
                            $ex = explode('.', $inColumn);

                            if (str_contains($ex[0], ',')) {
                                $exSub = explode(',', $ex[0]);
                                $keyPrimary = in_array($c, $exSub);
                            } else {
                                $keyPrimary = $ex[0] === $c;
                            }

                            $keySecondary = $ex[1];

                            foreach ($d as $e => $f) {
                                if (is_collect($f)) {
                                    // 4 - array of collection
                                    foreach ($f as $g => $h) {
                                        if ($keyPrimary && $keySecondary === $g && is_string($h) && stripos($h, $needle) !== false) {
                                            $items[$a] = $b;
                                        }
                                    }
                                } else {
                                    if ($keyPrimary && $keySecondary === $e && stripos($f, $needle) !== false) {
                                        $items[$a] = $b;
                                    }
                                }
                            }
                        } else {
                            foreach ($d as $e => $f) {
                                if (is_collect($f)) {
                                    // 4 - array of collection
                                    foreach ($f as $g => $h) {
                                        if (is_string($h) && stripos($h, $needle) !== false) {
                                            $items[$a] = $b;
                                        }
                                    }
                                } else {
                                    if (stripos($f, $needle) !== false) {
                                        $items[$a] = $b;
                                    }
                                }
                            }
                        }
                    } else {
                        if (stripos($d, $needle) !== false) {
                            $items[$a] = $b;
                        }
                    }
                }
            } else {
                if (is_string($b) && stripos($b, $needle) !== false) {
                    $items[$a] = $b;
                }
            }
        }

        return $items ?? [];
    }
}
