<?php

namespace App\Providers;

use App\Observers\ClientObserver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Modules\Clients\Entities\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function boot(Request $request)
    {
        $this->observers();

        Route::resourceVerbs([
            'create' => 'criar',
            'edit'   => 'editar',
        ]);

        Route::pattern('subdomain', '[-a-z0-9]+');

        Schema::defaultStringLength(191);

        view()->composer('*', function ($view) use ($request) {
            $params['user'] = auth()->user();

            if (isset($params)) {
                foreach ($params as $key => $data) {
                    $view->with($key, $data);
                }
            }
        });
    }

    /**
     * @return void
     */
    public function observers()
    {
        Client::observe(ClientObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
