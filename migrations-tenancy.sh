#!/usr/bin/env bash

php artisan module:migrate Addresses --database=tenancy
php artisan module:migrate Permissions --database=tenancy --seed
php artisan module:migrate Users --database=tenancy --seed
php artisan module:migrate Notifications --database=tenancy
php artisan module:migrate Departments --database=tenancy
php artisan module:migrate Phases --database=tenancy
php artisan module:migrate Processes --database=tenancy
php artisan module:migrate Viability --database=tenancy
php artisan module:migrate Settings --database=tenancy
php artisan module:migrate Viability --database=tenancy

