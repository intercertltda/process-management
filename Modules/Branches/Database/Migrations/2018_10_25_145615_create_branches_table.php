<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('branches', function (Blueprint $table) {
            $table->increments('id');

            $table->string('social_name')->nullable();
            $table->string('cnpj')->nullable();

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('branches');
    }
}
