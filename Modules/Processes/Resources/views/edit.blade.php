@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', ['widgetTitle' => __('Data of Process')])
        {{ Form::open([
            'url' => route('processes.update', $item->reference),
        ]) }}

        <div class="row form-group">
            <div class="col-md-6">
                {{ Form::label('title', __('Title')) }}
                {{ Form::text('title', old('title') ?? $item->title, ['class' => 'form-control', 'id' => 'title', 'required']) }}
            </div>

            <div class="col-md-6">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'collaborators',
                        'name-array' => 'collaborators[]',
                        'title' => __('Select user responsible'),
                        'label' => __('Responsible'),
                        'list' => $collaborators,
                        'old' => old('collaborators') ?? $relationCollaborators,
                        'multiple' => true,
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {{ Form::label('phase', __('Phase of Process')) }}
                {{ Form::hidden('phase', request()->reference) }}
                <p>{{ $item->phase->title }}</p>
            </div>
            <div class="col-md-6">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'type',
                        'title' => __('Select type'),
                        'label' => __('Type of Process'),
                        'list' => $types,
                        'old' => old('type') ?? $item->type,
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {{ Form::label('description', __('Description')) }}
                {{ Form::textarea('description', old('description') ?? $item->description, ['class' => 'form-control description', 'id' => 'description', 'required', 'rows' => '4']) }}
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Next') <i class="la la-angle-right"></i></button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent
@endsection
