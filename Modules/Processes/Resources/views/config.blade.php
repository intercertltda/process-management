@extends('system::layouts.master')
@section('content')
    @if($item->type === 'upload_information')
        @include('processes::types.' . $item->type, [
            'data' => $data ?? null,
            'item' => $item
        ])
    @else
        @component('system::components.page-content', [
            'widgetTitle' => sprintf('%s%s', __('Data of Process'), isset($types[$item->type]) ? sprintf(' - %s', $types[$item->type]) : '')
        ])
            {{ Form::open([
                'url' => router()->route('processes.type.' . (isset($update) ? 'update' : 'store'), ['reference' => request()->reference]),
                'class' => 'js-submit-config'
            ]) }}

            @include('processes::types.' . $item->type, [
                'data' => $data ?? null,
                'item' => $item
            ])

            @if($item->status === 'disabled')
                <div class="row form-group">
                    <div class="col-md-12">
                        {{ Form::label('status', __('Do you want to enable this process?')) }}
                        <br>
                        <label for="status-true">
                            {{ Form::radio('status', true, ['class' => 'form-control', 'id' => 'status-true']) }} Sim
                        </label>

                        <label for="status-false">
                            {{ Form::radio('status', false, ['class' => 'form-control', 'id' => 'status-false']) }} Não
                        </label>
                    </div>
                </div>
            @endif

            <div class="row form-group">
                <div class="col-md-12">
                    <div class="pull-right">
                        @stack('config-buttons')
                        {{ Form::submit($submit, ['class' => 'btn btn-primary']) }}
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        @endcomponent
    @endif
@endsection
