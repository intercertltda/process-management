@php
    if (isset($data->viability)) {
        $oldOn = $data->viability;
    } else {
        $oldOn = old('options');
    }
@endphp

<div class="row form-group">
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('Question')</th>
                <th scope="col">@lang('Qnt. Options')</th>
                <th scope="col">@lang('Active or Desactive')</th>
            </tr>
            </thead>
            <tbody class="multiple-options">
                @foreach ($params['viability'] as $question)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $question->title }}</td>
                        <td>{{ $question->options()->count() }}</td>
                        <td>
                            <div class="styled-checkbox">
                                <input type="checkbox" name="{{ sprintf('viability[%s]', $loop->index + 1) }}" value="{{ $question->reference }}" class="input-multiple" id="input-toggle-{{ $loop->index + 1 }}" {{ isset($data->viability) && in_array($question->reference, $data->viability) ? 'checked' : '' }}>
                                <label for="input-toggle-{{ $loop->index + 1 }}"></label>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@push('config-buttons')
    <a href="javascript:void(0)" class="btn btn-info" data-toggle=""><i class="la la-plus"></i> @lang('Add question')</a>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const organizeNameFields = () => {
                $.each(multiple.find('tr'), (i, item) => {
                    let e = $(item),
                        n = (i + 1);

                    e.find('.input-label').attr('name', 'options[' + n + '][label]');
                    e.find('.input-value').attr('name', 'options[' + n + '][value]');
                });
            };
        })(jQuery);
    </script>
@endpush
