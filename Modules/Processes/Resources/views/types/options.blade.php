@php
    if (isset($data->options)) {
        $oldOn = old('options') ?? $data->options;
    } else {
        $oldOn = old('options');
    }

    $subtype = old('subtype') ?? $data->subtype ?? '';
@endphp

<div class="row form-group">
    <div class="col-md-4">
        <label for="type_radio">
            <input type="radio" id="type_radio" name="type" value="radio" class="form-controll" {{ $subtype === 'radio' ? 'checked' : '' }}>
            @lang('Single choice') (radio)
        </label>
    </div>

    <div class="col-md-4">
        <label for="type_checkbox">
            <input type="radio" id="type_checkbox" name="type" value="checkbox" class="form-controll" {{ $subtype === 'checkbox' ? 'checked' : '' }}>
            @lang('Multiples choices') (checkbox)
        </label>
    </div>

    <div class="col-md-4">
        <label for="type_select">
            <input type="radio" id="type_select" name="type" value="select" class="form-controll" {{ $subtype === 'select' ? 'checked' : '' }}>
            @lang('Select choice') (select)
        </label>
    </div>
</div>

<div class="row form-group">
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('Name of Field')</th>
                <th scope="col">@lang('Value of Field')</th>
                <th scope="col">@lang('Actions')</th>
            </tr>
            </thead>
            <tbody class="multiple-options">
            @if(isset($oldOn))
                @foreach ($oldOn as $option)
                    <tr class="js-to-clone">
                        <td scope="row">{{ $loop->index + 1 }}</td>
                        <td>{{ Form::text(sprintf('options[%s][label]', $loop->index + 1), $option->label, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a label for the field'), 'required' => true]) }}</td>
                        <td>{{ Form::text(sprintf('options[%s][value]', $loop->index + 1), $option->value, ['data-type' => 'value', 'class' => 'form-control input-value', 'placeholder' => __('Please enter a value for the field'), 'required' => true]) }}</td>
                        <td>
                            <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="la la-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="js-to-clone">
                    <th scope="row">1</th>
                    <td>{{ Form::text('options[1][label]', null, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a label for the field'), 'required' => true]) }}</td>
                    <td>{{ Form::text('options[1][value]', null, ['data-type' => 'value', 'class' => 'form-control input-value', 'placeholder' => __('Please enter a value for the field'), 'required' => true]) }}</td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="la la-trash"></i></a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>

@push('config-buttons')
    <a href="javascript:void(0)" class="btn btn-info js-more-field"><i class="la la-plus"></i> @lang('Add field')</a>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const button = $('.js-more-field'),
                multiple = $('.multiple-options');

            button.on('click', (e) => {
                let row = multiple.find('.js-to-clone');
                let clone = row.first().clone();

                let lastLabel = row.last().find('.input-label'),
                    lastValue = row.last().find('.input-value');

                if (lastLabel.val() && lastValue.val()) {
                    multiple.append(clone);
                    let last = multiple.find('.js-to-clone').last();
                    last.find('input').removeAttr('value');
                    last.find('[scope=row]').text(multiple.find('tr').length);

                    organizeNameFields();
                } else {
                    toastr.error('@lang('Fill in the last fields')')
                }
            });

            $('body').on('click', '.js-remove-field', (e) => {
                const element = $(e.currentTarget),
                    parent = element.parents('tr');

                if (multiple.find('tr').length > 1) {
                    parent.remove();
                } else {
                    toastr.error('@lang('Must contain at least one choice')');
                }

                $.each(multiple.find('tr'), (i, item) => {
                    $(item).find('[scope=row]').text(i + 1);

                    organizeNameFields();
                });
            });

            const organizeNameFields = () => {
                $.each(multiple.find('tr'), (i, item) => {
                    let e = $(item),
                        n = (i + 1);

                    e.find('.input-label').attr('name', 'options[' + n + '][label]');
                    e.find('.input-value').attr('name', 'options[' + n + '][value]');
                });
            };
        })(jQuery);
    </script>
@endpush
