@php
    if (isset($data->upload_files)) {
        $oldOn = old('data') ?? $data->upload_files;
    } else {
        $oldOn = old('data');
    }

    $types = \App\Classes\Collection::setKey($item->phase->company->settings()->where('key', 'companies.config.documents')->first()->data, 'value', 'label');
@endphp

<div class="row form-group">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">@lang('Name of Field')</th>
                    <th scope="col">@lang('Value of Field')</th>
                    <th scope="col">@lang('Multiple')</th>
                    <th scope="col">@lang('Actions')</th>
                </tr>
            </thead>
            <tbody class="multiple-options">
            @if(isset($oldOn))
                @foreach ($oldOn as $option)
                    <tr class="js-to-clone">
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ Form::text(sprintf('data[%s][label]', $loop->index + 1), $option->label, [
                            'data-type' => 'label',
                            'class' => 'form-control input-label',
                            'placeholder' => __('Please enter a label for the field'),
                            'required' => true
                        ]) }}</td>
                        <td>
                            @component('system::components.select', [
                                'select' => [
                                    'name' => 'data',
                                    'name-array' => sprintf('data[%s][value]', $loop->index + 1),
                                    'title' => __('Please enter a value for the field'),
                                    'label' => null,
                                    'list' => $types,
                                    'old' => $option->value,
                                    'required' => true,
                                    'class' => 'input-value',
                                    'attrs' => [
                                        'data-type' => 'value'
                                    ]
                                ]
                            ])
                            @endcomponent
                        </td>
                        <td>
                            <div class="styled-checkbox">
                                <input type="checkbox" name="{{ sprintf('data[%s][multiple]', $loop->index + 1) }}" class="input-multiple" id="input-toggle-{{ $loop->index + 1 }}" {{ isset($option->multiple) ? 'checked' : '' }}>
                                <label for="input-toggle-{{ $loop->index + 1 }}"></label>
                            </div>
                        </td>
                        <td class="td-actions text-center not-print">
                            <a href="javascript:void(0);" class="js-remove-field"><i class="la la-close"></i></a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="js-to-clone">
                    <td scope="row">1</td>
                    <td>{{ Form::text('data[1][label]', null, [
                        'data-type' => 'label',
                        'class' => 'form-control input-label',
                        'placeholder' => __('Please enter a label for the field'),
                        'required' => true
                    ]) }}</td>
                    <td class="td-actions text-center not-print">
                        @component('system::components.select', [
                            'select' => [
                                'name' => 'data',
                                'name-array' => 'data[1][value]',
                                'title' => __('Please enter a value for the field'),
                                'label' => null,
                                'list' => $types,
                                'old' => null,
                                'required' => true,
                                'class' => 'input-value',
                                'attrs' => [
                                    'data-type' => 'value'
                                ]
                            ]
                        ])
                        @endcomponent
                    </td>
                    <td>
                        <div class="styled-checkbox">
                            <input type="checkbox" class="input-multiple" name="data[1][multiple]" id="input-toggle-1">
                            <label for="input-toggle-1"></label>
                        </div>
                    <td>
                        <a href="javascript:void(0);" class="js-remove-field"><i class="la la-close"></i></a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>

@push('config-buttons')
    <a href="javascript:void(0)" class="btn btn-info js-more-field"><i class="la la-plus"></i> @lang('Add field')</a>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const multiple = $('.multiple-options'),
                body = $('body');

            body.on('click', '.js-more-field', (e) => {
                let row = multiple.find('.js-to-clone'),
                    select = row.first().find('select').clone(),
                    clone = row.first().clone(),
                    lastLabel = row.last().find('.input-label'),
                    lastValue = row.last().find('select');

                if (lastLabel.val() && lastValue.val()) {

                    let dropdown = clone.find('.dropdown'),
                        parent = dropdown.parents('td');

                    dropdown.remove();

                    select.find('.bs-title-option').remove();

                    clone.find(parent).append(select);

                    multiple.append(clone);
                    let last = multiple.find('.js-to-clone').last();

                    last.find('input.input-label').val('');
                    last.find('select').val('');
                    last.find('input[type=checkbox]').val('');

                    last.find('select').selectpicker('render');
                    last.find('[scope=row]').text(multiple.find('tr').length);


                    organizeNameFields();
                } else {
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: '@lang('Fill in the last fields')',
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }
            });

            body.on('click', '.js-remove-field', (e) => {
                const element = $(e.currentTarget),
                    parent = element.parents('tr');

                if (multiple.find('tr').length > 1) {
                    parent.remove();
                } else {
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: '@lang('Must contain at least one choice')',
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }

                $.each(multiple.find('tr'), (i, item) => {
                    $(item).find('[scope=row]').text(i + 1);

                    organizeNameFields();
                });
            });

            const organizeNameFields = () => {
                $.each(multiple.find('tr'), (i, item) => {
                    let e = $(item),
                        n = (i + 1);

                    e.find('td').first().text(n);
                    e.find('.input-label').attr('name', 'data[' + n + '][label]');
                    e.find('.input-value').attr('name', 'data[' + n + '][value]');

                    let inputMultiple = e.find('.input-multiple'),
                        labelMultiple = inputMultiple.parent().find('label');


                    inputMultiple.attr('name', 'data[' + n + '][multiple]')
                        .attr('id', 'input-toggle-' + n);

                    labelMultiple.attr('for', 'input-toggle-' + n);
                });
            };
        })(jQuery);
    </script>
@endpush
