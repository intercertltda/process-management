@php
    if (isset($data->upload_files)) {
        $oldOn = old('data') ?? $data->upload_files;
    } else {
        $oldOn = old('data');
    }
@endphp


<div class="row form-group">
    <div class="col-md-8 padding-bottom-15">
        {{ Form::open([
            'url' => router()->route('api.files.store'),
            'class' => 'row',
            'id' => 'uploadForm',
            'style' => 'border: none;',
            'files' => true
        ]) }}

        {{ Form::hidden('company_id', $item->phase->company->id, ['id' => 'company_id']) }}
        {{ Form::hidden('attach_id', $item->id, ['id' => 'attach_id']) }}
        {{ Form::hidden('attach_type', 'process', ['id' => 'attach_type']) }}

        <div class="col-12 form-group">
            <span class="btn btn-success fileinput-button">
                <i class="la la-plus"></i>
                <span>Insira um arquivo...</span>
                <input id="fileupload" type="file" name="file">
            </span>

            <!-- The global progress bar -->
            <div id="progress" class="progress margin-top-15" style="height: 1px;">
                <div class="progress-bar progress-bar-success"></div>
            </div>

            <!-- The container for the uploaded files -->
            <div id="files" class="files"></div>
        </div>

        {{ Form::close() }}
    </div>

    <div class="col-md-4">
        <ul id="uploadList" class="list-group margin-top-15">
            @if(isset($item->files))
                @foreach ($item->files as $file)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; padding-right: 15px" href="{{ $file->url }}" target="_blank">{{ $file->originalName }}</a>
                        <a href="#" class="badge badge-primary badge-pill js-remove-upload" data-reference="{{ $file->reference }}">Excluir</a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>

@push('styles')
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/uploads.min.js') }}"></script>
@endpush
