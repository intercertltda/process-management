@php
    if (isset($data->content)) {
        $oldOn = old('content') ?? $data->content;
    } else {
        $oldOn = old('content');
    }
@endphp
<div class="row form-group">
    <div class="col-md-12">
        {{ Form::text('content', $oldOn, ['class' => 'form-control content', 'id' => 'content', 'required', 'placeholder' => __('Please paste the url here.')]) }}
    </div>
</div>
