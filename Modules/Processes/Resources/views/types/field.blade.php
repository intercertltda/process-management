@php
    if (isset($data->name)) {
        $oldOn = old('name') ?? $data->name;
    } else {
        $oldOn = old('name');
    }
@endphp
<div class="row form-group">
    <div class="col-md-6">
        @component('system::components.select', [
            'select' => [
                'name' => 'name',
                'title' => __('Select type of Field'),
                'label' => __('Type of field'),
                'list' => config('processes.fields'),
                'old' => $oldOn,
                'required' => true
            ]
        ])
        @endcomponent
    </div>
</div>
