@php
    if (isset($data->form)) {
        $oldOn = old('form') ?? $data->form;
    } else {
        $oldOn = old('form');
    }

@endphp

<div class="row form-group">
    <div class="col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">@lang('Name of Field')</th>
                    <th scope="col" style="width: 280px;">@lang('Type of Field')</th>
                    <th scope="col">@lang('Rules of Field')</th>
                    <th scope="col">@lang('Actions')</th>
                </tr>
            </thead>
            <tbody class="multiple-options">
            @if(isset($oldOn))
                @foreach ($oldOn as $option)
                    <tr class="js-to-clone">
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ Form::text(sprintf('form[%s][label]', $loop->index + 1), $option->label, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a label for the field'), 'required' => true]) }}</td>
                        <td style="width: 250px;">
                            @component('system::components.select', [
                                'select' => [
                                    'name' => 'form',
                                    'name-array' => sprintf('form[%s][type]', $loop->index + 1),
                                    'title' => __('Please enter a value for the field'),
                                    'label' => null,
                                    'list' => config('processes.fields'),
                                    'old' => $option->type,
                                    'required' => true,
                                    'class' => 'input-type',
                                    'attrs' => [
                                        'data-type' => 'type'
                                    ]
                                ]
                            ])
                            @endcomponent
                        </td>
                        <td>
                            <div class="styled-checkbox">
                                <input type="checkbox" name="{{ sprintf('form[%s][required]', $loop->index + 1) }}" class="input-required" id="input-toggle-{{ $loop->index + 1 }}" {{ isset($option->required) ? 'checked' : '' }}>
                                <label for="input-toggle-{{ $loop->index + 1 }}">Obrigatório?</label>
                            </div>
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="la la-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="js-to-clone">
                    <td>1</td>
                    <td>{{ Form::text('form[1][label]', null, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a label for the field'), 'required' => true]) }}</td>
                    <td style="width: 200px;">
                        @component('system::components.select', [
                                'select' => [
                                    'name' => 'form',
                                    'name-array' => 'form[1][type]',
                                    'title' => __('Please enter a value for the field'),
                                    'label' => null,
                                    'list' => config('processes.fields'),
                                    'old' => old('options.1.type'),
                                    'required' => true,
                                    'class' => 'input-type',
                                    'attrs' => [
                                        'data-type' => 'type'
                                    ]
                                ]
                            ])
                        @endcomponent
                    </td>
                    <td>
                        <div class="styled-checkbox">
                            <input type="checkbox" name="form[1][required]" class="input-required" id="input-toggle-1">
                            <label for="input-toggle-1">Obrigatório?</label>
                        </div>
                    </td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="la la-trash"></i></a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>

@push('config-buttons')
    <a href="javascript:void(0)" class="btn btn-info js-more-field"><i class="fa fa-plus"></i> @lang('Add field')</a>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const multiple = $('.multiple-options'),
                body = $('body');

            body.on('click', '.js-more-field', (e) => {
                let row = multiple.find('.js-to-clone'),
                    select = row.first().find('select').clone(),
                    clone = row.first().clone(),
                    lastLabel = row.last().find('.input-label'),
                    lastValue = row.last().find('select');

                if (lastLabel.val() && lastValue.val()) {

                    let dropdown = clone.find('.dropdown'),
                        parent = dropdown.parents('td');

                    dropdown.remove();

                    select.find('.bs-title-option').remove();

                    clone.find(parent).append(select);

                    multiple.append(clone);
                    let last = multiple.find('.js-to-clone').last();

                    last.find('input.input-label').val('');
                    last.find('select').val('');
                    last.find('input[type=checkbox]').val('');

                    last.find('select').selectpicker('render');
                    last.find('td').first().text(multiple.find('tr').length);


                    organizeNameFields();
                } else {
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: '@lang('Fill in the last fields')',
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }
            });

            body.on('click', '.js-remove-field', (e) => {
                const element = $(e.currentTarget),
                    parent = element.parents('tr');

                if (multiple.find('tr').length > 1) {
                    parent.remove();
                } else {
                    new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: '@lang('Must contain at least one choice')',
                        closeWith: ['click', 'button'],
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }

                $.each(multiple.find('tr'), (i, item) => {
                    $(item).find('td').first().text(i + 1);

                    organizeNameFields();
                });
            });

            const organizeNameFields = () => {
                $.each(multiple.find('tr'), (i, item) => {
                    let e = $(item),
                        n = (i + 1);

                    e.find('td').first().text(n);
                    e.find('.input-label').attr('name', 'form[' + n + '][label]');
                    e.find('.input-type').attr('name', 'form[' + n + '][type]');

                    let inputMultiple = e.find('.input-required'),
                        labelMultiple = inputMultiple.parent().find('label');


                    inputMultiple.attr('name', 'form[' + n + '][required]')
                        .attr('id', 'input-toggle-' + n);

                    labelMultiple.attr('for', 'input-toggle-' + n);
                });
            };
        })(jQuery);
    </script>
@endpush
