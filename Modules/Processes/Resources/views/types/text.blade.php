@php
    if (isset($data->content)) {
        $oldOn = old('content') ?? $data->content;
    } else {
        $oldOn = old('content');
    }
@endphp
<div class="row form-group">
    <div class="col-md-12">
        {{ Form::label('content', __('Informative text')) }}
        {{ Form::textarea('content', $oldOn, ['class' => 'form-control content', 'id' => 'content', 'required', 'rows' => '4']) }}
    </div>
</div>
