@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @component('system::components.table', [
                            'thead' => [
                                'name' => __('Name'),
                                'phase' => __('Etapa'),
                                'company' => __('Company'),
                                'actions' => __('Actions')
                            ]
                        ])
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->phase->title }} <a href="{{ router()->route('manager.phases.edit', $item->phase->reference) }}"><i class="fa fa-external-link-alt"></i></a></td>
                                    <td>{{ $item->phase->company->social_name }}</td>
                                    <td class="text-center">
                                        <a href="{{ router()->route('manager.processes.edit', $item->reference) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="#" data-local="processos" data-reference="{{ $item->reference }}" class="btn btn-danger js-confirm-action"><i class="la la-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
