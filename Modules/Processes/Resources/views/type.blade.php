
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{ Form::open([
                        'url' => router()->route('manager.processes.type.store', ['reference' => request()->reference]),
                    ]) }}

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="pull-left">@lang('Data of Process')</h4>
                        </div>
                    </div>
                    <br>

                    {{ $slot }}

                    <div class="row form-group">
                        <div class="col-md-12">
                            {{ Form::submit($submit, ['class' => 'btn btn-primary pull-right']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
