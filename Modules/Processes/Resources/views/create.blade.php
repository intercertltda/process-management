@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', ['widgetTitle' => __('Data of Process')])
        {{ Form::open([
            'url' => router()->route('processes.store', $phase->reference),
        ]) }}
        <div class="row form-group">
            <div class="col-md-6">
                {{ Form::label('title', __('Title')) }}
                {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'required']) }}
            </div>

            <div class="col-md-6">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'collaborators',
                        'name-array' => 'collaborators[]',
                        'title' => __('Select user responsible'),
                        'label' => __('Responsible'),
                        'list' => $collaborators,
                        'old' => old('collaborators'),
                        'multiple' => true,
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                {{ Form::label('phase', __('Step of Process')) }}
                {{ Form::hidden('phase', $phase->reference) }}
                <p>{{ $phase->title }}</p>
            </div>
            <div class="col-md-6">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'type',
                        'title' => __('Select type'),
                        'label' => __('Type of Process'),
                        'list' => $types,
                        'old' => old('type'),
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {{ Form::label('description', __('Description')) }}
                {{ Form::textarea('description', old('description'), ['class' => 'form-control description', 'id' => 'description', 'required', 'rows' => '4']) }}
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Next') <i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent
@endsection
