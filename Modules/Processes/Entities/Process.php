<?php

namespace Modules\Processes\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Files\Entities\File;
use Modules\Phases\Entities\Phase;
use Modules\Users\Entities\User;

class Process extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'processes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['type_desc', 'active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function phase(): HasOne
    {
        return $this->hasOne(Phase::class, 'id', 'phase_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'processes_users', 'process_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany(File::class, 'attachable');
    }

    /**
     * Decode data.
     *
     * @return mixed
     */
    public function getDataAttribute()
    {
        return json_decode($this->attributes['data']);
    }

    /**
     * @return string
     */
    public function getTypeDescAttribute(): string
    {
        return config(sprintf('processes.description.%s', $this->attributes['type']));
    }

    /**
     * @return bool
     */
    public function getActiveAttribute(): bool
    {
        return $this->attributes['status'] === 'on';
    }
}
