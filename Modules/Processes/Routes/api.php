<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'processos',
    'as'     => 'api.processes.',
], function () {
    Route::post('ordenacao', 'ApiController@ordering')->name('ordering');
    Route::post('status', 'ApiController@status')->name('status');
});
