<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'middleware' => ['auth'],
], function () {
    Route::get('', 'ProcessesController@index')->name('index');

    Route::group([
        'prefix' => 'tipo',
        'as'     => 'type.',
    ], function () {
        Route::get('{reference}/inserir', 'TypesController@create')->name('create');
        Route::post('{reference}/inserir', 'TypesController@store')->name('store');

        Route::get('{reference}/alterar', 'TypesController@edit')->name('edit');
        Route::post('{reference}/alterar', 'TypesController@update')->name('update');
    });

    // create single process
    Route::get('{reference}/criar', 'ProcessesController@create')->name('create');
    Route::post('{reference}/criar', 'ProcessesController@store')->name('store');

    // create single process
    Route::get('{reference}/editar', 'ProcessesController@edit')->name('edit');
    Route::post('{reference}/editar', 'ProcessesController@update')->name('update');
});
