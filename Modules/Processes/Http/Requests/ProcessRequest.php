<?php

namespace Modules\Processes\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'         => 'required',
            'collaborators' => 'required',
            'type'          => 'required',
            'description'   => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required'         => 'O título é obrigatório',
            'collaborators.required' => 'Pelo menos um colaborador é obrigatório',
            'type.required'          => 'Tipo é obrigatório',
            'description.required'   => 'Descrição da Etapa é obrigatória',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
