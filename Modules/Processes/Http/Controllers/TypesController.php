<?php

namespace Modules\Processes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Facades\MyCompanies;
use Modules\Processes\Entities\Process as Model;
use Modules\Processes\Facades\TreatmentFacade;

class TypesController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $model = Model::reference($request->reference);

        return view('processes::config', [
            'item'  => $model,
            'types' => config('processes.description'),

            'params'     => [
                'viability' => $model->phase->company->questions,
            ],

            'title'      => __('Config of Process'),
            'subtitle'   => null,
            'submit'     => __('Insert config'),
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.edit', $model->phase->reference),
                    'title'   => __('Step'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'url'     => router()->route('processes.edit', $model->reference),
                    'title'   => __('Process'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Config of Process'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $model = Model::reference($request->reference);

            $params = [
                'label'              => $model->title,
                'content'            => $request->input('content'),
                'name'               => $request->name ?? $model->type,
                'options'            => $request->options,
                'multiple'           => $request->multiple,
                'form'               => $request->form,
                'viability'          => $request->viability,
                'upload_files'       => $request->data,
                'upload_information' => $request->upload_information,
            ];

            $data = TreatmentFacade::contentOnSave($params, $model->type);

            $model->data = $data;
            if (isset($request->status)) {
                $model->status = $request->status ? 'enabled' : 'disabled';
            }

            if ($model->save()) {
                return redirect()
                    ->route('phases.edit', ['reference' => $model->phase->reference])
                    ->with('success', __('Process has been added to phase successfully!'));
            } else {
                return redirect()
                    ->route('processes.type.create', [
                        'reference' => $request->reference,
                    ])
                    ->with('fail', __('This process could not be added to the process!'))
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            return redirect()
                ->route('processes.type.edit', [
                    'reference' => $request->reference,
                ])
                ->with('fail', sprintf('[%s:%s] %s', $exception->getFile(), $exception->getLine(), $exception->getMessage()))
                ->withInput($request->all());
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $model = Model::reference($request->reference);

        return view('processes::config', [
            'item'   => $model,
            'types'  => config('processes.description'),
            'data'   => $model->data,
            'update' => true,

            'params' => [
                'viability'      => $model->phase->company->questions,
            ],

            'title'      => __('Alter Config of Process'),
            'subtitle'   => null,
            'submit'     => __('Alter config'),
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.edit', $model->phase->reference),
                    'title'   => __('Step'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'url'     => route('processes.edit', $model->reference),
                    'title'   => __('Process'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Config of Process'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            $model = Model::reference($request->reference);

            $params = [
                'type'               => $request->type ?? $model->type,
                'label'              => $model->title,
                'content'            => $request->input('content'),
                'name'               => $request->name ?? $model->type,
                'options'            => $request->options,
                'multiple'           => $request->multiple,
                'form'               => $request->form,
                'viability'          => $request->viability,
                'upload_files'       => $request->data,
                'upload_information' => $request->upload_information,
            ];

            $data = TreatmentFacade::contentOnSave($params, $model->type);

            $model->data = $data;
            if (isset($request->status)) {
                $model->status = $request->status ? 'enabled' : 'disabled';
            }

            if ($model->save()) {
                return redirect()
                    ->route('phases.edit', ['reference' => $model->phase->reference])
                    ->with('success', __('Process has been updated successfully!'));
            } else {
                return redirect()
                    ->route('processes.type.edit', [
                        'reference' => $request->reference,
                    ])
                    ->with('fail', __('This process could not be updated!'))
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            return redirect()
                ->route('processes.type.create', [
                    'reference' => $request->reference,
                ])
                ->with('fail', sprintf('[%s:%s] %s', $exception->getFile(), $exception->getLine(), $exception->getMessage()))
                ->withInput($request->all());
        }
    }
}
