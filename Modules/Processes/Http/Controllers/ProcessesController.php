<?php

namespace Modules\Processes\Http\Controllers;

use App\Classes\Collection;
use App\Classes\Find;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Facades\MyCompanies;
use Modules\Phases\Entities\Phase;
use Modules\Processes\Entities\Process as Model;
use Modules\Processes\Http\Requests\ProcessRequest;
use Modules\Users\Entities\User;

class ProcessesController extends Controller
{
    /**
     * MainController constructor.
     */
    public function __construct()
    {
//        $this->middleware(['forbid:responsible']);
    }

    /**
     * @return array
     */
    public function data()
    {
        if (is_admin()) {
            return [
                'collaborators' => User::all(),
                'processes'     => Model::all(),
                'phases'        => Phase::all(),
            ];
        } else {
            return [
                'collaborators' => MyCompanies::collaborators(),
                'processes'     => MyCompanies::processes(),
                'phases'        => MyCompanies::phases(),
            ];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = (object) $this->data();

        return view('processes::index', [
            'items' => $data->processes,

            'title'      => __('Processes'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Processes'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $phase = Phase::reference($request->reference);

        return view('processes::create', [
            'collaborators' => Collection::setKey($phase->company->users, 'reference', 'name'),

            'types' => config('processes.description'),

            'phase' => $phase,

            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.edit', ['reference' => $request->reference]),
                    'title'   => __('Step'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProcessRequest $request
     *
     * @return Response
     */
    public function store(ProcessRequest $request)
    {
        try {
            $fields = (object) $request->all();

            $model = new Model();

            $model->phase_id = Find::db('phases', ['reference' => $request->reference]);
            $model->type = $fields->type;
            $model->title = $fields->title;
            $model->description = $fields->description;

            if ($model->save()) {

                foreach ($fields->collaborators as $item) {
                    $collaborator = Find::db('users', ['reference' => $item]);
                    $model->users()->attach($collaborator);
                }

                return redirect()
                    ->route('processes.type.create', $model->reference)
                    ->with('success', __('Process has been added to phase successfully!'));
            } else {
                return redirect()
                    ->route('processes.create', $request->reference)
                    ->with('fail', __('Could not create process.'));
            }
        } catch (\Exception $exception) {
            $request->session()->flash('fail', sprintf('[%s:%s] %s', $exception->getFile(), $exception->getLine(), $exception->getMessage()));

            return redirect()
                ->route('processes.create', [
                    'reference' => $request->reference,
                ])
                ->withInput($request->all());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $data = (object) $this->data();
        $model = Model::reference($request->reference);

        $users = $model->users()->get()->map(function ($responsible) {
            return $responsible->reference;
        });

        return view('processes::edit', [
            'item'                  => $model,
            'collaborators'         => Collection::setKey($data->collaborators, 'reference', 'name'),
            'phases'                => Collection::setKey($data->phases, 'reference', 'title'),
            'relationCollaborators' => $users,
            'types'                 => config('processes.description'),
            'title'                 => __('Edit'),
            'subtitle'              => null,
            'breadcrumb'            => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.edit', $model->phase->reference),
                    'title'   => __('Step'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Edit'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProcessRequest $request
     *
     * @return Response
     */
    public function update(ProcessRequest $request)
    {
        try {
            $fields = (object) $request->all();

            $model = Model::reference($request->reference);

            $model->type = $fields->type;
            $model->title = $fields->title;
            $model->description = $fields->description;

            if ($model->save()) {
                foreach ($fields->collaborators as $item) {
                    $collaborators[] = Find::db('users', ['reference' => $item]);
                }

                $model->users()->sync($collaborators);

                return redirect()
                    ->route('processes.type.edit', $model->reference)
                    ->with('success', __('Process has been updated successfully!'));
            } else {
                return redirect()
                    ->route('processes.edit', $model->reference)
                    ->with('fail', __('Could not update process.'));
            }
        } catch (\Exception $exception) {
            $request->session()->flash('fail', sprintf('[%s:%s] %s', $exception->getFile(), $exception->getLine(), $exception->getMessage()));

            return redirect()
                ->route('processes.create', [
                    'reference' => $request->reference,
                ])
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
    }
}
