<?php

namespace Modules\Processes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Manager\Entities\Process as Main;
use Modules\Manager\Http\Requests\StepRequest;
use Modules\Manager\Transformers\Manager as Resource;
use Modules\Users\Entities\User;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()
            ->json(Resource::collection(Main::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StepRequest $request
     *
     * @return Response
     */
    public function store(StepRequest $request)
    {
        $fields = (object) $request->all();

        $model = new Main();

        $model->type = 'text';

        $model->title = $fields->title;
        $model->description = $fields->description;
        $model->phase_id = $fields->phase_id;
        $model->order = $fields->order;

        if ($model->save()) {
            if ($fields->responsible) {
                foreach ($fields->responsible as $user) {
                    $user = User::reference($user);
                }
            }
            $model->responsible()->attach($user);

            return response()
                ->json(new Resource($model), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.'),
                ], 402);
        }
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function show(Request $request)
    {
        return response()
            ->json(new Resource(Main::reference($request->reference)));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StepRequest $request
     *
     * @return Response
     */
    public function update(StepRequest $request)
    {
        $fields = (object) $request->all();

        $model = Main::reference($request->reference);

        $model->type = 'text';
        $model->title = $fields->title;
        $model->description = $fields->description;
        $model->phase_id = $fields->phase_id;
        $model->order = $fields->order;

        if ($model->save()) {
            if ($fields->responsible) {
                foreach ($fields->responsible as $user) {
                    $ids[] = User::reference($user)->id;
                }
            }

            $model->responsible()->sync($ids);

            return response()
                ->json(new Resource($model), 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.'),
                ], 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Main::reference($request->reference);

        if (!$model->count()) {
            return response()
                ->json(['message' => __('Resource not found.')], 402);
        }

        if ($model->delete()) {
            return response()
                ->json([
                    'message' => __('Item moved to trash.'),
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be moved to the trash.'),
                ], 402);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ordering(Request $request)
    {
        try {
            foreach ($request->items as $item) {
                DB::table('processes')
                    ->where('reference', $item['reference'])
                    ->update([
                        'order' => $item['index'],
                    ]);
            }

            return response()
                ->json([
                    'success' => true,
                    'message' => __('Order changed successfully!'),
                ], 200);
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success'  => false,
                    'message'  => __('Could not change the order!'),
                    'code'     => $exception->getCode(),
                    '_message' => $exception->getMessage(),
                    'line'     => $exception->getLine(),
                    'file'     => $exception->getFile(),
                ], 402);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        try {
            $item = DB::table('processes')
                ->where('reference', $request->reference)
                ->update([
                    'status' => $request->status === 'off' ? 'on' : 'off',
                ]);

            if ($item) {
                return response()
                    ->json([
                        'success'   => true,
                        'message'   => __('Content updated successfully.'),
                        'reference' => $request->reference,
                        'status'    => $request->status,
                    ], 200);
            } else {
                return response()
                    ->json([
                        'success'   => false,
                        'message'   => __('Content could not be updated.'),
                        'reference' => $request->reference,
                        'status'    => $request->status,
                    ], 402);
            }
        } catch (\Exception $exception) {
            return response()
                ->json([
                    'success'   => false,
                    'message'   => __('Content could not be updated.'),
                    'code'      => $exception->getCode(),
                    '_message'  => $exception->getMessage(),
                    'line'      => $exception->getLine(),
                    'file'      => $exception->getFile(),
                    'reference' => $request->reference,
                    'status'    => $request->status,
                ], 402);
        }
    }
}
