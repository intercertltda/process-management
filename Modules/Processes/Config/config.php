<?php

return [
    'name' => 'Processes',

    'types' => [
        // se o campo ter "upload" deve conter também um campo para informação

        'text', // Texto simples - Informativo
        'field text', // Campo de Texto Simples
        'field date', // Campo de data
        'field number', // Campo de número,
        'field email', // Campo de E-mail

        'select', // Campos de select
        'checkbox', // Campos de checkbox
        'radio', // Campos de Radio
        'textarea', // Campo de textarea
        'upload files', // Upload Imagens,
        'upload information', // Upload de Arquivo Informativos = Vídeos, Imagens, PDF

        'video', // Video of Youtube or Vimeo

        'form', // O adm poderá criar um formulário simples
        'viability', // Selecionar um Formulário de Viabilidade
    ],

    'description' => [
        'text'               => 'Texto Informativo',
        'field'              => 'Campo de texto',
        'options'            => 'Campo de escolhas',
        'upload_files'       => 'Upload de Arquivos',
        'upload_information' => 'Upload de Informações',
        'video'              => 'URL de Vídeo (Youtube ou Vímeo)',

        'form'      => 'Formulário Simples',
        'viability' => 'Montar formulário de Viabilidade',

        'field text'   => 'Campo de Texto Simples',
        'field date'   => 'Campo de data',
        'field number' => 'Campo de número,',
        'field email'  => 'Campo de E-mail',

        'select'             => 'Campos de select',
        'checkbox'           => 'Campos de checkbox',
        'radio'              => 'Campos de Radio',
        'textarea'           => 'Campo de textarea',
        'upload files'       => 'Upload Imagens,',
        'upload information' => 'Upload de Arquivo Informativos = Vídeos, Imagens, PDF',
    ],

    'fields' => [
        'text'       => 'Campo de texto simples',
        'date'       => 'Campo de data',
        'number'     => 'Campo númerico',
        'textarea'   => 'Campo de texto',
        'email'      => 'Campo de e-mail',
        'cpf'        => 'Campo para CPF',
        'cnpj'       => 'Campo para CNPJ',
        'phone'      => 'Campo para telefone',
        'postalcode' => 'Campo para CEP',
    ],

    'documents' => [
        'cpf' => 'CPF',
    ],
];
