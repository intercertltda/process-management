<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $types = config('processes.types');

        Tenancy::migrate(['tenancy'])->create('processes', function (Blueprint $table) use ($types) {
            $table->increments('id');

            $table->integer('phase_id')
                ->unsigned();

            $table->string('type')->default(array_first($types));
            $table->string('status')->default('on');

            $table->string('title');
            $table->string('description');
            $table->integer('order')->default(1);

            /*
            * Tipos:
            * text: {"content": "Aqui é uma descrição para o tipo 'text'", "label": "Informação Básica", "name": "information"}
            * field text: {"content": null, "label": "Campo para preencher nome", "name": "name"}
             * field date: {"content": null, "label": "Campo para preencher data", "name": "along_config"}
             * field number: {"content": null, "label": "Campo para preencher um número", "name": "number"}
             * field email: {"content": null, "label": "Campo para preencher um email", "name": "email"}
             *
             * checkbox: {"content": null, "label": "Campo de múltiplas escolhas", "name": "checkbox", "options":[{"label": "Opção 1", "value": "op1"}, {"label": "Opção 2", "value": "op2"}]}
             * select: {"content": null, "label": "Campo de múltiplas escolhas", "name": "select", "options":[{"label": "Opção 1", "value": "op1"}, {"label": "Opção 2", "value": "op2"}]}
             * radio: {"content": null, "label": "Campo de múltiplas escolhas", "name": "radio", "options":[{"label": "Opção 1", "value": "op1"}, {"label": "Opção 2", "value": "op2"}]}
             *
             * textarea: {"content": null, "label": "Campo para preencher um texto qualquer", "name": "textarea"}
             * upload files: {"content": null, "label": "Campo para upload de arquivos", "name": "uploads", "multiple": true}
             * upload information: {"content": "url-para-o-arquivo", "label": "Campo para upload de arquivos informativos", "name": "uploads_information", "multiple": false}
             *
             * form {"content": null, "label": "Combo de inputs", "name": "form"},
             * viability: {"content": "viability", "label": "Escolher as perguntas de Viabilidade", "name": "viabilty"}
             */
            $table->longText('data')->nullable();

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('processes', function (Blueprint $table) {
            $table->foreign('phase_id')
                ->references('id')
                ->on('phases')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('processes');
    }
}
