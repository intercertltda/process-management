<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Processes\Entities\Process::class, function (Faker $faker) {
    $title = $faker->title;
    $description = $faker->words(10);

    return [
        'phase_id'    => factory(\Modules\Phases\Entities\Phase::class)->create()->id,
        'type'        => 'text',
        'status'      => 'on',
        'title'       => $title,
        'description' => $description,
        'order'       => 1,
        'data'        => json_encode([
            'content' => $description,
            'label'   => $title,
            'name'    => 'information',
        ]),
    ];
});
