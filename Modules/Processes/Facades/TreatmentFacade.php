<?php

namespace Modules\Processes\Facades;

class TreatmentFacade
{
    /**
     * @param array  $params
     * @param string $type
     *
     * @return false|string
     */
    public static function contentOnSave(array $params, string $type)
    {
        $default = [
            'type'    => $type,
            'content' => $params['content'] ?? null,
            'label'   => $params['label'] ?? null,
        ];

        switch ($type) {
            case 'options':
                if (isset($params['options'])) {
                    $data['options'] = $params['options'];
                }

                if (isset($params['type'])) {
                    $data['subtype'] = $params['type'];
                }
                break;

            case 'upload_files':
                $data['upload_files'] = $params['upload_files'] ?? false;
                break;

            case 'upload_information':
                $data['multiple'] = false;
                break;

            case 'form':
                foreach ($params['form'] as $row) {
                    $data['form'][] = [
                        'label'    => $row['label'],
                        'type'     => $row['type'],
                        'name'     => sprintf('%s_%s', str_slug($row['label']), $row['type']),
                        'required' => $row['required'] ?? false,
                    ];
                }

                break;

            case 'viability':
                foreach ($params['viability'] as $param) {
                    $data['viability'][] = $param;
                }
                break;

            default:
                break;
        }

        $data['name'] = $params['name'] ?? $type;

        return json_encode(array_merge($default, $data));
    }
}
