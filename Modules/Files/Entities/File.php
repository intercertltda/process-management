<?php

namespace Modules\Files\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url', 'full_path', 'dir'];

    /**
     * Make full path to file.
     *
     * @return string
     */
    public function getFullPathAttribute(): string
    {
        return storage_path(sprintf('app/public/uploads/%s', $this->attributes['path']));
    }

    /**
     * @return string
     */
    public function getUrlAttribute(): string
    {
        return sprintf('/storage/uploads/%s', $this->attributes['path']);
    }

    /**
     * @return string
     */
    public function getDirAttribute()
    {
        return dirname(Storage::disk('uploads')->path($this->attributes['path']));
    }
}
