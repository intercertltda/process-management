<?php

namespace Modules\Files\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Files\Entities\File as ModelFile;
use Modules\Files\Facades\ProcessFile;

class ApiController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (!config('files.who.'.$request->attach_type)) {
            return response()
                ->json([
                    'status'  => 400,
                    'message' => 'Relação não existe nas configurações',
                ], 400);
        }

        $getFile = $request->file('file');
        $process = new ProcessFile($getFile, set_uuid($getFile->getClientOriginalName()));
        $file = new ModelFile();

        $file->company_id = $request->company_id;
        $file->name = $process->name;
        $file->path = $process->path();
        $file->type = $getFile->getClientMimeType();
        $file->originalName = $getFile->getClientOriginalName();
        $file->size = $getFile->getSize();

        // for relation
        $file->attachable_id = $request->attach_id;
        $file->attachable_type = config('files.who.'.$request->attach_type);

        $getFile->storeAs($process->dir(), $process->name, 'uploads');

        if (Storage::disk('uploads')->exists($process->path()) && $file->save()) {
            return response()
                ->json([
                    'image'   => $file->toArray(),
                    'message' => 'Arquivo enviado com sucesso!'
                ], 200);
        } else {
            return response()
                ->json([
                    'status'  => 400,
                    'message' => 'Não foi possivel salvar o arquivo',
                ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = ModelFile::reference($request->reference);

        try {
            // deleting dir
            shell_exec('rm -rf '.$model->dir);

            if (!Storage::disk('uploads')->exists($model->path) && $model->forcedelete()) {
                return response()
                    ->json([
                        'message' => 'Arquivo removido com sucesso',
                    ], 200);
            } else {
                return response()
                    ->json([
                        'message' => 'Não foi possível deletar o arquivo.',
                    ], 400);
            }
        } catch (\Exception $e) {
            return response()
                ->json([
                    'message' => $e->getMessage(),
                    'line'    => $e->getLine(),
                    'file'    => $e->getFile(),
                ], 400);
        }
    }
}
