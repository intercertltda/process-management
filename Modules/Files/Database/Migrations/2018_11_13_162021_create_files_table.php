<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['system', 'tenancy'])->create('files', function (Blueprint $table) {
            $table->increments('id');

            $table->morphs('attachable');

            $table->string('name');
            $table->string('path');
            $table->string('type');
            $table->string('originalName');
            $table->string('size');

            $table->uuid('reference')
                ->unique();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['system', 'tenancy'])->dropIfExists('files');
    }
}
