<?php

namespace Modules\Files\Facades;

use Illuminate\Http\UploadedFile;

class ProcessFile
{
    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string
     */
    public $name;

    /**
     * ProcessFile constructor.
     *
     * @param UploadedFile $file
     * @param string       $name
     */
    public function __construct(UploadedFile $file, string $name)
    {
        $this->file = $file;
        $this->name = sprintf('%s.%s', $name, $file->getClientOriginalExtension());
    }

    /**
     * Get base dir.
     *
     * @return string
     */
    public function dir(): string
    {
        return sprintf('%s/%s/%s', date('Y'), date('m'), md5($this->name));
    }

    /**
     * Get path to file.
     *
     * @return string
     */
    public function path(): string
    {
        return sprintf('%s/%s', $this->dir(), $this->name);
    }
}
