<?php

namespace Modules\Auth\Facades;

class LoginFacade
{
    /**
     * @param string $value
     *
     * @return string
     */
    public static function check(string $value)
    {
        switch ($value) {
            case str_contains($value, '@'):
                return 'email';
                break;
            case is_numeric($value):
                return 'document';
                break;
            default:
                return 'login';
                break;
        }
    }
}
