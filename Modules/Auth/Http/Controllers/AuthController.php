<?php

namespace Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Auth\Facades\LoginFacade;
use Modules\Auth\Http\Requests\RequestLogin;
use Modules\Users\Facades\UsersActionsFacade;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AuthController extends Controller
{
    public $redirect = '/';

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        $this->performLogout($request);

        return router()
            ->redirect('login');
    }

    /**
     * Display a login resource.
     *
     * @return Response
     */
    public function showLogin()
    {
        return view('auth::login', [
            'title' => __('Login'),
        ]);
    }

    /**
     * Login Action.
     *
     * @param \Modules\Auth\Http\Requests\RequestLogin $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(RequestLogin $request)
    {
        $login = $request->input('login');
        $pass = $request->input('password');
        $typeLogin = LoginFacade::check($login);

        $credentials[$typeLogin] = $login;
        $credentials['password'] = $pass;

        if (Auth::attempt($credentials)) {
            if (session()->has('url.intended')) {
                return redirect(session()->get('url.intended'));
            } else {
                UsersActionsFacade::loggedAt(\Auth::user());

                return router()
                    ->redirect('dashboard.index');
            }
        } else {
            return back()
                ->withErrors([
                    'Autenticação falhou',
                ]);
        }
    }
}
