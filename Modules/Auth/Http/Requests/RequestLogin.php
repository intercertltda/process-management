<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Auth\Rules\LoginTypeRule;

class RequestLogin extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'login'    => ['required', new LoginTypeRule()],
            'password' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'login.required'   => 'Login é obrigatório.',
            'password.requred' => 'A senha é obrigatória.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
