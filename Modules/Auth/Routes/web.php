<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get(__('auth.login'), 'AuthController@showLogin')->name('login');
Route::post(__('auth.login'), 'AuthController@postLogin')->name('login.post');

Route::get(__('auth.register'), 'AuthController@showRegister')->name('register');
Route::post(__('auth.register'), 'AuthController@postRegister')->name('register.post');

Route::get(sprintf('%s/{token}', __('auth.reset-password')), 'AuthController@showPasswordReset')->name('password.reset');
Route::post(sprintf('%s/{token}', __('auth.reset-password')), 'AuthController@postPasswordReset')->name('password.reset.post');

Route::get(__('auth.request-password'), 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post(__('auth.request-password'), 'AuthController@postPasswordRequest')->name('password.request.post');

Route::get(__('auth.logout'), 'AuthController@logout')->name('logout');
