<?php

return [
    'auth' => 'conta',
    'login' => 'entrar',
    'register' => 'cadastrar',
    'reset-password' => 'resetar-senha',
    'request-password' => 'esqueci-senha',
    'logout' => 'sair'
];
