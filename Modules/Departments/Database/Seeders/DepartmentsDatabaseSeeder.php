<?php

namespace Modules\Departments\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Departments\Entities\Department;
use Modules\Users\Entities\User;

class DepartmentsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        $departments = [
            ['name' => 'Compliance', 'description' => 'Setor de Compliance'],
            ['name' => 'Financeiro', 'description' => 'Setor Financeiro'],
            ['name' => 'Comercial', 'description' => 'Setor Comercial'],
        ];

        foreach ($departments as $department) {
            $model = new Department();
            $model->role_id = 1;
            $model->company_id = 1;
            $model->name = $department['name'];
            $model->description = $department['description'];
            $model->save();
            $user = User::where('login', 'felipe.rank')->first();
            $model->users()->attach($user);
        }
    }
}
