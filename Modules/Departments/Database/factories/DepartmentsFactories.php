<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Departments\Entities\Department::class, function (Faker $faker) {
    return [
        'role_id'     => 1,
        'name'        => $faker->words(rand(3, 6), true),
        'description' => $faker->text(50),
    ];
});

$factory->afterCreating(\Modules\Departments\Entities\Department::class, function ($company, $faker) {
    $company->users()
        ->attach(factory(\Modules\Users\Entities\User::class, 3)->create());
});
