@extends('system::layouts.master')
@section('content')
    @php
        $thead['name'] = __('Name');
        $thead['companies'] = __('Company');
        $thead['collaborators'] = __('Collaborators');
        $thead['updated_at'] = __('Modified At');
        $thead['actions'] = __('Actions');
    @endphp
    @component('system::components.table', [
        'thead' => $thead
    ])
        @foreach ($items as $department)
            <tr>
                <td>{{ $department->name }}</td>
                <td>{{ $department->company->social_name }}</td>
                <td>{{ $department->collaborators }}</td>
                <td>{{ $department->updated_at->format('d/m/Y \à\s H:i') }}</td>
                <td class="text-center">
                    <a href="{{ router()->route('departments.edit', $department->reference) }}" class="btn btn-primary"><i class="la la-edit"></i></a>
                    <a href="{{ router()->route('departments.show', $department->reference) }}" class="btn btn-info"><i class="la la-eye"></i></a>
                    <a href="#" data-local="departamentos" data-reference="{{ $department->reference }}" class="btn btn-danger js-confirm-action"><i class="la la-trash"></i></a>
                </td>
            </tr>
        @endforeach
    @endcomponent
@endsection
