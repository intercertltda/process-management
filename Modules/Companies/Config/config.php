<?php

return [
    'name' => 'Companies',

    'type-documents' => [
        'comprovante-residencia' => 'Comprovante de Residência',
        'cpf'                    => 'Cópia de CPF',
        'cnh'                    => 'Cópia de CNH',
        'identidade'             => 'Cópia da Identidade',
    ],
];
