<?php

$factory->define(\Modules\Companies\Entities\Company::class, function () {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'social_name'       => $faker->company,
        'document'          => $faker->cnpj(false),
        'phone'             => str_clean($faker->phoneNumber),
        'email'             => $faker->email,
        'representative_id' => factory(\Modules\Users\Entities\User::class)->create()->id,
    ];
});

$factory->afterCreating(\Modules\Companies\Entities\Company::class, function ($company, $faker) {
    $company->address()->save(factory(\Modules\Addresses\Entities\Address::class)->create([
        'addressable_type' => 'Modules\\Companies\\Entities\\Company',
        'addressable_id'   => $company->id,
    ]));

    $company->responsible()
        ->attach(factory(\Modules\Users\Entities\User::class, 3)->create());

    factory(\Modules\Departments\Entities\Department::class, 4)->create([
        'company_id' => $company->id
    ]);

    factory(\Modules\Viability\Entities\Question::class, 5)->create([
        'company_id' => $company->id
    ]);
});
