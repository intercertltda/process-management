<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('companies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('representative_id')
                ->unsigned();

            $table->string('slug');
            $table->string('social_name');
            $table->string('document');
            $table->string('phone');
            $table->string('email');

            $table->string('token', 16)->unique();
            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('companies', function (Blueprint $table) {
            $table->foreign('representative_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('companies');
    }
}
