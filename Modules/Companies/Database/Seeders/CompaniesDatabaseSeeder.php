<?php

namespace Modules\Companies\Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Companies\Entities\Company;
use Modules\Users\Entities\User;

class CompaniesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();

        factory(Company::class, 10)
            ->create();
    }
}
