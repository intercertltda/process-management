<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'empresas',
    'as'     => 'api.companies.',
], function () {
    Route::get('{reference}/data', 'ApiController@data')->name('data');
    Route::post('set-url', 'ApiController@url')->name('url');
});
