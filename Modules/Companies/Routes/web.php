<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\System\Facades\RouteFacade;

RouteFacade::group([
    'prefix'     => 'empresas',
    'as'         => 'companies.',
], 'CompaniesController');

Route::get('empresas/{reference}/configuracoes', 'SettingsController@index')
    ->name('companies.config');

Route::group([
    'prefix'     => 'empresas',
    'as'         => 'companies.config.',
], function () {
    Route::get('{reference}/configuracoes', 'SettingsController@index')->name('index');
    Route::post('{reference}/configuracoes', 'SettingsController@save')->name('save');
});


Route::group([
    'prefix'     => 'minhas-empresas',
    'as'         => 'companies.my.',
], function () {
    Route::get('', 'MyCompaniesController@index')->name('index');
    Route::get('{reference}/editar', 'MyCompaniesController@edit')->name('edit');
    Route::get('{reference}/editar', 'MyCompaniesController@edit')->name('edit');
    Route::post('{reference}', 'MyCompaniesController@update')->name('update');
});
