<?php

namespace Modules\Companies\Facades;

use Illuminate\Support\Collection;

class MyCompanies
{
    /**
     * Get user auth.
     *
     * @return mixed
     */
    public static function get()
    {
        return auth()->user()->companies;
    }

    /**
     * Get collection custom.
     *
     * @param string $call
     *
     * @return \Illuminate\Support\Collection
     */
    public static function collection(string $call): Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->{$call})) {
                foreach ($company->{$call} as $item) {
                    $items[] = $item;
                }
            }
        }

        return collect($items ?? []);
    }

    /**
     * Get Steps collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function phases(): Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->phases)) {
                foreach ($company->phases as $phase) {
                    $phases[] = $phase;
                }
            }
        }

        return collect($phases ?? []);
    }

    /**
     * Get departments of user.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function departments(): Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->departments)) {
                foreach ($company->departments as $department) {
                    $departments[] = $department;
                }
            }
        }

        return collect($departments ?? []);
    }

    /**
     * Get collaborators of companies.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function collaborators(): Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->users)) {
                foreach ($company->users as $collaborator) {
                    $collaborators[] = $collaborator;
                }
            }
        }

        return collect($collaborators ?? []);
    }

    /**
     * Get processes of companies.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function processes(): Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->processes)) {
                foreach ($company->processes as $process) {
                    $processes[] = $process;
                }
            }
        }

        return collect($processes ?? []);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function questions(): Collection
    {
        foreach (self::get() as $company) {
            if (isset($company->questions)) {
                foreach ($company->questions as $question) {
                    $questions[] = $question;
                }
            }
        }

        return collect($question ?? []);
    }
}
