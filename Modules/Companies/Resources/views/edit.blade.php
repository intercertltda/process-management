@php
    $social_name = $companiesData->social_name ?? old('name');
    $document = $companiesData->document ?? old('document');
    $email = $companiesData->email ?? old('email');
    $phone = $companiesData->phone ?? old('phone');
    $representative = $companiesData->representative->reference ?? old('representative');
    $collaborators = $collaborators ?? old('collaborators');
@endphp
@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', [
        'widgetTitle' => __('Data of Company'),
        'widgetButtons' => [
            [
                'label' => __('Settings'),
                'url' => router()->route('companies.config.index', $companiesData->reference),
                'icon' => 'la la-wrench'
            ],
            [
                'label' => __('URL of Company'),
                'url' => '#',
                'icon' => 'la la-chain',
                'attributes' => 'data-toggle="modal" data-target="#setCompanyUrl"'
            ],
            [
                'label' => __('Copy URL for share'),
                'icon' => 'la la-clipboard',
                'class' => 'copy',
                'attributes' => sprintf('data-clipboard-text="%s"', $companiesData->url)
            ]
        ]
    ])
        {{ Form::open([
            'url' => router()->route('companies.update', $companiesData->reference),
        ]) }}
        {{ Form::hidden('__method', 'PUT') }}

        <div class="row form-group">
            <div class="col-md-4">
                {{ Form::label('social_name', __('Fantasy name')) }}
                {{ Form::text('social_name', $social_name, ['class' => 'form-control', 'id' => 'social_name', 'required']) }}
            </div>
            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'representative',
                        'title' => 'Selecione um usuário',
                        'label' => __('Legal Representative'),
                        'list' => $representatives,
                        'old' => $representative,
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'collaborators',
                        'name-array' => 'collaborators[]',
                        'title' => 'Selecione um usuário',
                        'label' => __('Collaborators'),
                        'list' => $representatives,
                        'old' => $collaborators,
                        'multiple' => true
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                {{ Form::label('document', sprintf('%s (CNPJ)', __('Document'))) }}
                {{ Form::text('document', $document, ['class' => 'form-control document_cnpj', 'id' => 'document', 'required']) }}
            </div>

            <div class="col-md-4">
                {{ Form::label('email', __('E-mail')) }}
                {{ Form::email('email', $email, ['class' => 'form-control', 'id' => 'email', 'required']) }}
            </div>

            <div class="col-md-4">
                {{ Form::label('phone', __('Phone')) }}
                {{ Form::text('phone', $phone, ['class' => 'form-control cellphone', 'id' => 'phone', 'required']) }}
            </div>
        </div>

        <hr class="col-12">

        @component('addresses::form', [
            'isArray' => true,
                'data' => $address
        ])
        @endcomponent

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Update')</button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent
@endsection

@push('modals')
    <div id="setCompanyUrl" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@lang('Configure URL to access')</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>{!! sprintf('%s: <strong>%s</strong>', __('Suggestion'), str_slug($companiesData->social_name)) !!}</p>
                    <div class="input-group">
                        <input class="form-control" id="makeUrl" type="text" value="{{ $companiesData->slug ?? '' }}">
                        <span class="input-group-addon addon-secondary">.{{ config('app.domain') }}</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow" data-dismiss="modal">@lang('Cancel')</button>
                    <button type="button" class="btn btn-primary {{ !isset($companiesData->slug) ? 'disabled' : '' }}" id="set-url">@lang('Save')</button>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('components/inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        (($) => {
            'use strict';
            $('.cellphone').inputmask({mask:'(99) [9] 9999-9999'});
            $('.document_cnpj').inputmask({mask:'99.999.999/9999-99'});

            const slug = $('#makeUrl'),
                button = $('#set-url');

            slug.on('keyup keydown', (e) => {
                let input = $(e.currentTarget);

                if (input.val().length > 0) {
                    button.removeClass('disabled');
                } else {
                    button.addClass('disabled');
                }
            });

            button.on('click', (e) => {
                let btn = $(e.currentTarget);

                if (!btn.hasClass('disabled'))  {
                    $.ajax({
                        url: '/api/empresas/set-url',
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            slug: slug.val(),
                            reference: '{{ $companiesData->reference }}'
                        },
                        success: (res) => {
                            console.log(res, 'res');

                            slug.parent().removeClass('has-danger');

                            window.intersig.setMessage(res.message);
                            $('#setCompanyUrl').modal('hide');
                        },
                        error: (res) => {
                            let response = res.responseJSON;

                            console.log(response, 'res');
                            window.intersig.setMessage(response.message, 'error');

                            slug.parent().addClass('has-danger');
                        },
                    });
                }
            });
        })(jQuery);
    </script>
@endpush
