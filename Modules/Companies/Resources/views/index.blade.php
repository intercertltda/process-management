@extends('system::layouts.master')
@section('content')
    @component('system::components.table', [
        'thead' => [
            'name' => 'Nome',
            'responsible' => 'Responsável',
            'document' => 'CNPJ',
            'phone' => 'Telefone',
            'email' => 'E-mail',
            'actions' => 'Ações'
        ]
    ])
        @foreach ($items as $item)
            <tr>
                <td>{{ $item->social_name }}</td>
                <td>{{ $item->representative->name }}</td>
                <td>{{ raank()->document($item->document)->masked() }}</td>
                <td>{{ helpers()->phone($item->phone) }}</td>
                <td>{{ $item->email }}</td>
                <td class="text-center">
                    <a href="{{ router()->route('companies.edit', $item->reference) }}" class="btn btn-primary"><i class="la la-edit"></i></a>
                    <a href="{{ router()->route('workflow.company', $item->reference) }}" class="btn btn-info"><i class="la la-building"></i></a>
                    <a href="#" class="btn btn-success copy" data-clipboard-text="{{ $item->url }}"><i class="la la-clipboard"></i></a>
                    @if(is_admin())<a href="#" data-local="empresas" data-reference="{{ $item->reference }}" class="btn btn-danger js-confirm-action"><i class="la la-trash"></i></a>@endif
                </td>
            </tr>
        @endforeach
    @endcomponent
@endsection
