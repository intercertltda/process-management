@php
    $genre = old('profile.genre');
    $social_name = old('name');
    $document = old('document');
    $email = old('email');
    $phone = old('profile.phone');
    $representative = old('representative');
    $collaborators = old('collaborators');
@endphp
@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', [
        'widgetTitle' => __('Data of Company')
    ])
        {{ Form::open([
            'url' => router()->route('companies.store'),
        ]) }}
            <div class="row form-group">
                <div class="col-md-4">
                    {{ Form::label('social_name', __('Fantasy name')) }}
                    {{ Form::text('social_name', $social_name, ['class' => 'form-control', 'id' => 'social_name', 'required']) }}
                </div>
                <div class="col-md-4">
                    @component('system::components.select', [
                        'select' => [
                            'name' => 'representative',
                            'title' => 'Selecione um usuário',
                            'label' => __('Legal Representative'),
                            'list' => $representatives,
                            'old' => $representative,
                            'required' => true
                        ]
                    ])
                    @endcomponent
                </div>

                <div class="col-md-4">
                    @component('system::components.select', [
                        'select' => [
                            'name' => 'collaborators',
                            'name-array' => 'collaborators[]',
                            'title' => 'Selecione um usuário',
                            'label' => __('Collaborators'),
                            'list' => $representatives,
                            'old' => $collaborators,
                            'multiple' => true
                        ]
                    ])
                    @endcomponent
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    {{ Form::label('document', sprintf('%s (CNPJ)', __('Document'))) }}
                    {{ Form::text('document', $document, ['class' => 'form-control document_cnpj', 'id' => 'document', 'required']) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('email', __('E-mail')) }}
                    {{ Form::email('email', $email, ['class' => 'form-control', 'id' => 'email', 'required']) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('phone', __('Phone')) }}
                    {{ Form::text('phone', $phone, ['class' => 'form-control cellphone', 'id' => 'phone', 'required']) }}
                </div>
            </div>

            <hr class="col-12">

            @component('addresses::form', [
                'isArray' => true
            ])
            @endcomponent

            <div class="row form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">@lang('Create')</button>
                </div>
            </div>
        {{ Form::close() }}
    @endcomponent
@endsection
@push('styles')
    <style>
        .radio-multiple label {
            margin-bottom: 0;
            line-height: 40px;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('components/inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        $(".cellphone").inputmask({mask:'(99) [9] 9999-9999'});
        $(".document_cnpj").inputmask({mask:'99.999.999/9999-99'});
    </script>
@endpush
