@php
@endphp
@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', [
        'widgetTitle' => __('Settings of Company'),
        'widgetClasses' => 'sliding-tabs'
    ])
        <ul class="nav nav-tabs" id="example-one" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="base-tab-documents" data-toggle="tab" href="#tab-documents" role="tab" aria-controls="tab-documents" aria-selected="true"><i class="la la-file-text-o"></i> Tipos de Documentos</a>
            </li>
            {{--<li class="nav-item">
                <a class="nav-link" id="base-tab-notifications" data-toggle="tab" href="#tab-notifications" role="tab" aria-controls="tab-notifications" aria-selected="false"><i class="la la-bell"></i> Notificações</a>
            </li>--}}
        </ul>
        <div class="tab-content pt-3">
            <div class="tab-pane fade show active" id="tab-documents" role="tabpanel" aria-labelledby="base-tab-documents">
                @include('companies::config.documents', [
                    'params' => [
                        'key' => 'settings.companies.documents.'.$company->reference,
                        'slug' => str_slug($company->social_name),
                    ],
                    'company' => $company
                ])
            </div>
            {{--<div class="tab-pane fade" id="tab-notifications" role="tabpanel" aria-labelledby="base-tab-notifications">
                Nam sagittis nec velit vitae molestie. Donec eget luctus sem. Nullam tortor tortor, consequat id lacinia nec, tempus in diam. Phasellus vel molestie purus, ac hendrerit risus. Phasellus non purus lacinia purus fringilla hendrerit. Sed pharetra odio a ante volutpat aliquam id non lacus.
            </div>--}}
        </div>
    @endcomponent
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('components/inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        $(".cellphone").inputmask({mask:'(99) [9] 9999-9999'});
        $(".document_cnpj").inputmask({mask:'99.999.999/9999-99'});
    </script>
@endpush
