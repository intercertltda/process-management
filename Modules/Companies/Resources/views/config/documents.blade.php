@php
    $oldOn = $company
        ->settings()
        ->where('key', 'companies.config.documents')
        ->first();
@endphp

{{ Form::open([
    'url' => route('companies.config.save', $company->reference),
    'class' => 'js-submit-settings'
]) }}

    {{ Form::hidden('key', 'companies.config.documents') }}
    {{ Form::hidden('redirect', route('companies.config.index', $company->reference)) }}

    <div class="row form-group">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">@lang('Name of Field')</th>
                    <th scope="col">@lang('Value of Field')</th>
                    <th scope="col">@lang('Actions')</th>
                </tr>
                </thead>
                <tbody class="multiple-options">
                @if($oldOn->data)
                    @foreach ($oldOn->data as $option)
                        <tr class="js-to-clone">
                            <th scope="row">{{ $loop->index + 1 }}</th>
                            <th>{{ Form::text(sprintf('data[%s][label]', $loop->index + 1), $option->label, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a name for the field'), 'required' => true]) }}</th>
                            <th>{{ Form::text(sprintf('data[%s][value]', $loop->index + 1), $option->value, ['data-type' => 'value', 'class' => 'form-control input-value', 'placeholder' => __('Please enter a value for the field'), 'required' => true]) }}</th>
                            <th>
                                <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="la la-trash"></i></a>
                            </th>
                        </tr>
                    @endforeach
                @else
                    @foreach(config('companies.type-documents') as $key => $type)
                        <tr class="js-to-clone">
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ Form::text(sprintf('data[%s][label]', $loop->index + 1), $type, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a label for the field'), 'required' => true]) }}</td>
                            <td>{{ Form::text(sprintf('data[%s][value]', $loop->index + 1), $key, ['data-type' => 'value', 'class' => 'form-control input-value', 'placeholder' => __('Please enter a value for the field'), 'required' => true]) }}</td>
                            <td>
                                <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="la la-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="javascript:void(0)" class="btn btn-info js-more-field"><i class="fa fa-plus"></i> @lang('Add field')</a>
                {{ Form::submit(__('Save'), ['class' => 'btn btn-primary']) }}
            </div>
        </div>
    </div>
{{ Form::close() }}

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const button = $('.js-more-field'),
                multiple = $('.multiple-options');

            button.on('click', (e) => {
                let row = multiple.find('.js-to-clone');
                let clone = row.first().clone();

                let lastLabel = row.last().find('.input-label'),
                    lastValue = row.last().find('.input-value');

                if (lastLabel.val() && lastValue.val()) {
                    multiple.append(clone);
                    let last = multiple.find('.js-to-clone').last();
                    last.find('input').removeAttr('value');
                    last.find('td').first().text(multiple.find('tr').length);

                    organizeNameFields();
                } else {
                    window.intersig.setMessage('@lang('Fill in the last fields')', 'error')
                }
            });

            $('body').on('click', '.js-remove-field', (e) => {
                const element = $(e.currentTarget),
                    parent = element.parents('tr');

                if (multiple.find('tr').length > 1) {
                    parent.remove();
                } else {
                    window.intersig.setMessage('@lang('Must contain at least one choice')', 'error');
                }

                $.each(multiple.find('tr'), (i, item) => {
                    $(item).find('[scope=row]').text(i + 1);

                    organizeNameFields();
                });
            });

            const organizeNameFields = () => {
                $.each(multiple.find('tr'), (i, item) => {
                    let e = $(item),
                        n = (i + 1);

                    e.find('.input-label').attr('name', 'data[' + n + '][label]');
                    e.find('.input-value').attr('name', 'data[' + n + '][value]');
                });
            };
        })(jQuery);
    </script>
@endpush
