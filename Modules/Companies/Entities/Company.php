<?php

namespace Modules\Companies\Entities;

use App\Traits\ModelHelpers;
use App\Traits\TokenReference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Addresses\Entities\Address;
use Modules\Departments\Entities\Department;
use Modules\Phases\Entities\Phase;
use Modules\Settings\Entities\Setting;
use Modules\Users\Entities\User;
use Modules\Viability\Entities\Question;
use Modules\Workflow\Entities\Workflow;

class Company extends Model
{
    use ModelHelpers,
        SoftDeletes,
        TokenReference;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'tenancy';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['users', 'document_masked', 'collaborators', 'url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function representative(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'representative_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function responsible(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'companies_users', 'company_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings(): HasMany
    {
        return $this->hasMany(Setting::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function departments(): HasMany
    {
        return $this->hasMany(Department::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phases(): HasMany
    {
        return $this->hasMany(Phase::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workflow(): HasMany
    {
        return $this->hasMany(Workflow::class, 'id', 'company_id');
    }

    /**
     * @return mixed
     */
    public function getUsersAttribute()
    {
        $representative = $this->representative()->latest()->get();
        $responsible = $this->responsible()->latest()->get();

        return $representative->merge($responsible);
    }

    /**
     * @return string
     */
    public function getDocumentMaskedAttribute(): string
    {
        return raank()->document($this->attributes['document'])->masked();
    }

    /**
     * @return string
     */
    public function getCollaboratorsAttribute(): string
    {
        foreach ($this->responsible as $user) {
            $users[] = $user->name;
        }

        return implode(', ', $users);
    }

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return router()->route('manager.index', $this->attributes['slug']);
    }
}
