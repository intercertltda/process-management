<?php

namespace Modules\Companies\Http\Controllers;

use App\Classes\Collection;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Companies\Entities\Company as Model;
use Modules\Companies\Facades\MyCompanies;
use Modules\Companies\Http\Requests\CompaniesRequest;
use Modules\Companies\Transformers\CompanyResource as Resource;
use Modules\Raank\Facades\Locations;
use Modules\Users\Entities\User;

class CompaniesController extends Controller
{
    /**
     * CompaniesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['is:admin'])
            ->only(['index', 'create']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (is_admin()) {
            $companies = Model::all();
        } else {
            $companies = MyCompanies::get();
        }

        return view('companies::index', [
            'items' => Resource::collection($companies),

            'title'      => __('Companies'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Companies'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $users = DB::table('users')
            ->select(['reference', 'name'])
            ->get();

        return view('companies::create', [
            'representatives' => Collection::setKey($users, 'reference', 'name'),

            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('companies.index'),
                    'title'   => __('Companies'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompaniesRequest $request
     *
     * @return Response
     */
    public function store(CompaniesRequest $request)
    {
        $fields = (object) $request->all();
        $model = new Model();
        $user = User::reference($fields->representative);

        $model->social_name = $fields->social_name;
        $model->document = (string) helpers()->clean($fields->document);
        $model->phone = (string) helpers()->clean($fields->phone);
        $model->email = $fields->email;

        $model->representative_id = $user->id;

        if ($model->save()) {
            if (isset($fields->collaborators)) {
                foreach ($fields->collaborators as $u) {
                    if ($u !== $fields->representative) {
                        $us = User::reference($u);
                        $model->responsible()->attach($us);
                        $us->assignRole('directors');
                    }
                }
            }

            $model->address()->create($fields->address);

            return redirect()
                ->route('companies.edit', $model->reference)
                ->with('success', __('Successfully created content.'));
        } else {
            return redirect()
                ->route('companies.create')
                ->with('fail', __('Content could not be created.'))
                ->withInput(toArray($fields));
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $companies = Model::reference($request->reference);

        return view('companies::show', [
            'companies'  => $companies,
            'title'      => __('Show'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('companies.index'),
                    'title'   => __('Companies'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Show'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $companies = Model::reference($request->reference);

        foreach ($companies->responsible as $collaborator) {
            $collaborators[] = $collaborator->reference;
        }

        if (is_client()) {
            $users = MyCompanies::collaborators();
        } else {
            $users = DB::table('users')
                ->select(['reference', 'name'])
                ->get();
        }

        return view('companies::edit', [
            'companiesData'   => $companies,
            'collaborators'   => $collaborators ?? [],
            'representatives' => Collection::setKey($users, 'reference', 'name') ?? [],
            'address'         => $companies->address,

            'title'      => __('Edit'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('companies.index'),
                    'title'   => __('Companies'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Edit'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompaniesRequest $request
     *
     * @return Response
     */
    public function update(CompaniesRequest $request)
    {
        $fields = (object) $request->all();
        $model = Model::reference($request->reference);
        $user = User::reference($fields->representative);

        $model->slug = str_slug($fields->slug);
        $model->social_name = $fields->social_name;
        $model->document = (string) helpers()->clean($fields->document);
        $model->phone = (string) helpers()->clean($fields->phone);
        $model->email = $fields->email;
        $model->representative_id = $user->id;

        foreach ($model->responsible as $uc) {
            $inModel[] = $uc->reference;
        }

        if ($model->save()) {
            if ($fields->collaborators) {
                foreach ($fields->collaborators as $u) {
                    if (isset($inModel)) {
                        if (!in_array($user, $inModel) && $u !== $fields->representative) {
                            $us = User::reference($u);
                            $sync[] = $us->id;
                        }
                    } else {
                        $us = User::reference($u);
                        $sync[] = $us->id;
                    }
                }
                $model->responsible()->sync($sync);
            }

            if ($model->address) {
                $model->address()->update($fields->address);
            } else {
                $model->address()->create($fields->address);
            }

            return redirect()
                ->route('companies.edit', $request->reference)
                ->with('success', __('Successfully updated content.'));
        } else {
            return redirect()
                ->route('companies.edit', $request->reference)
                ->with('fail', __('Content could not be updated.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Model::reference($request->reference);

        if (isset($model->phases) && $model->phases->processes->count() > 0) {
            $this->message($request, [
                'type'    => 'error',
                'message' => __('You can not delete steps with processes.'),
                'status'  => 403,
            ], 'companies.index');
        }

        if (!$model->count()) {
            abort('404', __('Resource not found.'));
        }

        return $this->delete_method($request, $model, 'companies.index');
    }
}
