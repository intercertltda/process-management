<?php

namespace Modules\Companies\Http\Controllers;

use App\Classes\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Companies\Entities\Company;
use Modules\Manager\Entities\Step;

class ApiController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $company = Company::reference($request->reference);

        $depends = $company->steps
            ->filter(function ($step) use ($request) {
                if ($request->step) {
                    $current = Step::reference($request->step);

                    return $step->order > $current->order and $step !== $request->reference;
                }

                return $step->reference !== $request->reference;
            });

        if (!count($company->departments)) {
            return response()
                ->json([
                    'type'    => 'link',
                    'link'    => router()->route('departments.create'),
                    'message' => __('Company nothing found departments, please register this data.'),
                ], 404);
        }

        return response()
            ->json([
                'collaborators' => Collection::setKey($company->users, 'reference', 'name'),
                'departments'   => Collection::setKey($company->departments, 'reference', 'name'),
                'depends'       => Collection::setKey($depends, 'reference', 'title'),
                'message'       => __('Company data has been loaded successfully!'),
            ], 200);
    }

    public function url(Request $request)
    {
        $has = DB::table('companies')
            ->where([
                ['reference', '!=', $request->reference],
                ['slug', '=', $request->slug]
            ])
            ->count();

        if ($has) {
            return response()
                ->json([
                    'message' => __('This url already exists.')
                ], 400);
        }

        $company = Company::reference($request->reference);

        if ($company->slug === $request->slug) {
            return response()
                ->json([
                    'message' => __('Must be a different url!')
                ], 400);
        }

        $company->slug = $request->slug;

        if ($company->save()) {
            return response()
                ->json([
                    'message' => __('This url changed as successfully!')
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('Do you can\'t update url!')
                ], 400);
        }
    }
}
