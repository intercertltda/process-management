<?php

namespace Modules\Companies\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Company;
use Modules\Settings\Entities\Setting;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $company = Company::reference($request->reference);

        return view('companies::config', [
            'title'      => __('Settings'),
            'subtitle'   => sprintf('%s "%s"', __('Company'), $company->social_name),

            'company' => $company,

            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard'
                ],
                [
                    'title'   => __('Companies'),
                    'current' => false,
                    'icon'    => null,
                    'url'     => router()->route('companies.index')
                ],
                [
                    'title'   => __('Edit'),
                    'current' => false,
                    'icon'    => null,
                    'url'     => router()->route('companies.edit', $company->reference)
                ],
                [
                    'title'   => __('Settings'),
                    'current' => true,
                    'icon'    => null
                ]
            ]
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $company = Company::reference($request->reference);

        $make = [
            'key'         => $request->key,
            'data'        => is_array($request->data) ? json_encode($request->data) : $request->data,
            'others_data' => is_array($request->others_data) ? json_encode($request->others_data) : $request->others_data,
        ];

        if ($company->settings()->updateOrCreate([], $make)) {
            return redirect()
                ->to($request->redirect ?? $request->getRequestUri())
                ->with('success', __('Successfully updated content.'));
        } else {
            return redirect()
                ->to($request->redirect ?? $request->getRequestUri())
                ->with('fail', __('Content could not be updated.'));
        }
    }
}
