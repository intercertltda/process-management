<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Addresses\Entities\Address::class, function (Faker $faker) {
    $fakerState = states()->faker();

    return [
        'street'       => $faker->streetName,
        'neighborhood' => $faker->streetSuffix,
        'number'       => $faker->buildingNumber,
        'complement'   => $faker->streetSuffix,
        'code_postal'  => $faker->postcode,
        'city'         => (string) $fakerState['city'],
        'state'        => (string) $fakerState['uf']->sigla,
    ];
});
