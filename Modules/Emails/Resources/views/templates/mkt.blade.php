@extends('mail.layouts.main')

@section('content')
    @component('mail.components.text', [
        'margin' => 0
    ])
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td style="font-size: 0; padding: 0px; font-family: sans-serif; line-height: 140%; color: #555555;">
                        <a href="https://bit.ly/2Mk6HtS">
                            <img style="width: 100%;" src="https://intercert.com.br/emails/2.jpg" alt="">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 0; padding: 0px; font-family: sans-serif; line-height: 140%; color: #555555;">
                        <a href="https://bit.ly/2OMQeju">
                            <img style="width: 100%;" src="https://intercert.com.br/emails/3.jpg" alt="">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 0; padding: 0px; font-family: sans-serif; line-height: 140%; color: #555555;">
                        <img style="width: 100%;" src="https://intercert.com.br/emails/4.jpg" alt="">
                    </td>
                </tr>
            </tbody>
        </table>

        <div style="padding: 40px; font-size: 16px;">
            <p>(…)<br>
                IV – de 1º de agosto a 31 de outubro de 2018, conforme cronograma estabelecido pela Célula de Laboratório Fiscal (CELAB) da
                Coordenadoria de Administração Tributária (CATRI), para os contribuintes enquadrados em um dos seguintes grupos/subclasses
                da Classificação Nacional de Atividade Econômico-Fiscais (CNAEFiscal):
            </p>

            <ul style="padding: 0; list-style: none;">
                <li><strong>a)</strong> 451 Comércio de veículos automotores;</li>
                <li><strong>b)</strong> 4732-6/00 Comércio varejista de lubrificantes;</li>
                <li><strong>c)</strong> 474 Comércio varejista de material de construção;</li>
                <li><strong>d)</strong> 4751-2/01 Comércio varejista especializado de equipamentos e suprimentos de informática;</li>
                <li><strong>e)</strong> 4751-2/02 Recarga de cartuchos para equipamento de informática;</li>
                <li><strong>f)</strong> 4752-1/00 Comércio varejista especializado de equipamentos de telefonia e comunicação;</li>
                <li><strong>g)</strong> 4754-7/03 Comércio varejista de artigos de iluminação;</li>
                <li><strong>h)</strong> 4756-3/00 Comércio varejista especializado de instrumentos musicais e acessórios;</li>
                <li><strong>i)</strong> 4757-1/00 Comércio varejista especializado de peças e acessórios para aparelhos eletroeletrônicos para uso doméstico, exceto informática e comunicação;</li>
                <li><strong>j)</strong> 4772-5/00 Comércio varejista de cosméticos, produtos de perfumaria e de higiene pessoal;</li>
                <li><strong>k)</strong> 4773-3/00 Comércio varejista de artigos médicos e ortopédicos;</li>
                <li><strong>l)</strong> 4774-1/00 Comércio varejista de artigos de óptica;</li>
                <li><strong>m)</strong> 4782-2/01 Comércio varejista de calçados;</li>
                <li><strong>n)</strong> 551 Hotéis e similares.</li>
            </ul>

            <p style="text-align: center; color: #777; font-size: 14px; margin: 30px 0 0;">Fonte: <a href="http://imagens.seplag.ce.gov.br/PDF/20180802/do20180802p01.pdf">http://www.seplag.ce.gov.br</a></p>
        </div>

        <div style="padding: 40px 0; margin-bottom: 5px; background: #058cba; display: inline-block; width: 100%;">
            <div style="padding: 0 40px; text-align: left;">
                <a href="https://intercert.com.br" style="padding-top: 3px; float: left; color: #fff; text-decoration: none; font-size: 18px; height: 26px; line-height: 26px; text-transform: uppercase; font-family: Arial, sans-serif; font-weight: bold;">intercert.com.br</a>
            </div>
            <div style="padding: 0 40px; text-align: right;">
                <a style="margin-left: 10px;" title="Facebook" href="http://facebook.com/intercert.tecnologia"><img src="https://intercert.com.br/emails/facebook-logo.png" width="22" alt="Facebook InterCert"></a>
                <a style="margin-left: 10px;" title="Intagram" href="http://instagram.com/intercert.tecnologia"><img src="https://intercert.com.br/emails/instagram-logo.png" width="22" alt="Instagram InterCert"></a>
            </div>
        </div>
    @endcomponent
@endsection