@extends('mail.layouts.main')

@section('content')
    @component('mail.components.text')
        @slot('title')
            Atenção
        @endslot

        <p style="font-size: 14px;">
            Viemos por meio deste, informar que houve um equívoco em nosso sistema, diagnosticamos que, no domingo (<strong>19/08/2018 por volta das 19:20</strong>), onde foi dispararado erroneamente um e-mail contendo um possível fechamento de sua Filial em um ambiente de <strong>TESTES</strong> da InterCert, o conteúdo pode haver <strong>URLS</strong> como: <strong>stage.intercert.com.br</strong> e também com o boleto contendo o código de barra inválido.
            <br>
            <br>Favor desconsider o e-mail anterior.
            <br>Pedimos desculpas pelo inconveniente.
            <br>Att;
            <br>Equipe InterCert
        </p>
    @endcomponent
@endsection