@extends('mail.layouts.main')

@section('content')
    @component('mail.components.text')
        @slot('title')
            Olá, {{ $name ?? 'Prezado(a)' }}.
        @endslot

        <blockquote style="background: #f05b4f; color: #fff; font-size: 14px; margin: 0; padding: 30px;">Viemos por meio desta, informar que, o prazo foi prorrogado até o dia <strong>04/10/2018</strong>, caso haja atraso ou descumprimento até a data definida o mesmo será suspenso, ficando inapto à emitir qualquer tipo de Certificado.</blockquote>
        <br>
        <h4>Mensagem Retroativa</h4>
        <p style="font-size: 14px;">Viemos através deste e-mail, informar-lhe que faz-se necessário realizar cadastro de trabalhador em nossa base de dados, de acordo com o novo projeto <strong>eSocial</strong> (instituido pelo decreto <strong>8.373/14</strong>) para que o mesmo possa continuar de forma regularizada em nossos sistemas.</p>
        <p style="font-size: 14px;">Vale ressaltar que o prazo estipulado para regularização irá até a data de <strong>28/09/2018</strong>, portanto não deixar de nos enviar conforme solicitado.</p>
        <p style="font-size: 14px;">Segue em anexo, a planilha que deverá ser preenchida e assinada de forma manual (escrita a próprio punho) e, encaminhada de forma digitalizada e legível  após conclusão para o seguinte e-mail;
            <a href="mailto:compliance@intercert.com.br">compliance@intercert.com.br</a>.</p>
        <p style="font-size: 14px;">Quaisquer dúvidas quanto a este procedimento, disponibilizamos um canal de comunicação via Whatsapp, no presente número; <strong>(88) 9 9821-0088</strong>. Ou ligue para o Especialista Ronaldo <strong>(88) 9 9921-2286</strong>.</p>
        <p style="font-size: 14px;">Quanto ao processo de envio da planilha digitalizada, por gentileza queira nos encaminhar em formato de PDF, com o assunto; Formulário de Cadastro do Trabalhador E-Social - {{ $name or '[Escrever o seu nome completo aqui]' }}</p>

        <p>Os anexos são necessários apenas em casos de depentendes.
            <br>Documentos dos dependentes: <strong>RG/CPF</strong>, <strong>Certidão de Nascimento</strong> e <strong>Cartão de Vacina</strong>.
        </p>
    @endcomponent
@endsection
