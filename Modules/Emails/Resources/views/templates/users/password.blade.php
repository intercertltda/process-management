@extends('mail.layouts.main')

@section('content')
    @component('mail.components.text')
        @isset($title)
            @slot('title')
                {{ $title or '' }}
            @endslot
        @endisset

        <p style="margin: 0 0 30px; font-size: 14px;">{{ $text or '' }}</p>

        <table width="100%" style="font-size: 14px; padding-bottom: 30px;" cellspacing="0" cellpadding="0" align="left">
            <tbody>
                <tr class="row-desc-item">
                    <td>Seu acesso por nome de usuário ou e-mail</td>
                </tr>
                <tr class="row-content-item">
                    <td>{{ $login or '' }}</td>
                </tr>
                <tr class="row-content-item">
                    <td>{{ $email or '' }}</td>
                </tr>
                <tr class="row-spacing-item">
                    <td style="display: block; padding: 10px;"></td>
                </tr>
                <tr class="row-desc-item">
                    <td>Sua nova senha:</td>
                </tr>
                <tr class="row-content-item">
                    <td>{{ $pass }}</td>
                </tr>
            </tbody>
        </table>

        @if(isset($button))
            <div style="padding: 25px;">
                <!-- Button : BEGIN -->
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                    <tr>
                        <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                            <a href="{{ $button['url'] }}" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 110%; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                                <span style="color:#ffffff;">{{ $button['title'] }}</span>
                            </a>
                        </td>
                    </tr>
                </table>
                <!-- Button : END -->
            </div>
        @endif
    @endcomponent
@endsection