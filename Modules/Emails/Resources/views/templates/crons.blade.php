@extends('mail.layouts.main')

@section('content')
	@component('mail.components.text')
		@isset($title)
			@slot('title')
				{{ $title or '' }}
			@endslot
		@endisset

		<p style="font-size: 14px;">
			{!! $text or '' !!}
		</p>
	@endcomponent
@endsection