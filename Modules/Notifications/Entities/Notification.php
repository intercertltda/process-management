<?php

namespace Modules\Notifications\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'tenancy';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

}
