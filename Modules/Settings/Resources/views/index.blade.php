@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open([
                            'url' => router()->route('settings.manager'),
                            'class' => 'js-submit-settings'
                        ]) }}

                        {{ Form::hidden('key', $params->key) }}
                        {{ Form::hidden('slug', $params->slug) }}
                        {{ Form::hidden('company_id', $params->company_id) }}

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav justify-content-start nav-pills">
                                    @foreach (config('system.settings.pages') as $link)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $setting === $link['slug'] ? 'active' : '' }}" href="{{ router()->route('settings.index', ['setting' => $link['slug']]) }}">{{ $link['text'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <br>

                        @include('settings::pages.' . $setting, [
                            'data' => $data
                        ])

                        <div class="row form-group">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    @stack('form-buttons')
                                    {{ Form::submit(__('Save'), ['class' => 'btn btn-primary']) }}
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
