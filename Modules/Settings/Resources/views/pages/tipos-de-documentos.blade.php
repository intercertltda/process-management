@php
    if (isset($data)) {
        $oldOn = old('data') ?? $data;
    }
@endphp

<div class="row form-group">
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('Name of Field')</th>
                <th scope="col">@lang('Value of Field')</th>
                <th scope="col">@lang('Actions')</th>
            </tr>
            </thead>
            <tbody class="multiple-options">
            @if(isset($oldOn))
                @foreach ($oldOn as $option)
                    <tr class="js-to-clone">
                        <th scope="row">{{ $loop->index + 1 }}</th>
                        <td>{{ Form::text(sprintf('data[%s][name]', $loop->index + 1), $option->name, ['data-type' => 'name', 'class' => 'form-control input-name', 'placeholder' => __('Please enter a name for the field'), 'required' => true]) }}</td>
                        <td>{{ Form::text(sprintf('data[%s][value]', $loop->index + 1), $option->value, ['data-type' => 'value', 'class' => 'form-control input-value', 'placeholder' => __('Please enter a value for the field'), 'required' => true]) }}</td>
                        <td>
                            <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="js-to-clone">
                    <th scope="row">1</th>
                    <td>{{ Form::text('data[1][label]', null, ['data-type' => 'label', 'class' => 'form-control input-label', 'placeholder' => __('Please enter a label for the field'), 'required' => true]) }}</td>
                    <td>{{ Form::text('data[1][value]', null, ['data-type' => 'value', 'class' => 'form-control input-value', 'placeholder' => __('Please enter a value for the field'), 'required' => true]) }}</td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-danger js-remove-field"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>

@push('form-buttons')
    <a href="javascript:void(0)" class="btn btn-info js-more-field"><i class="fa fa-plus"></i> @lang('Add field')</a>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const button = $('.js-more-field'),
                multiple = $('.multiple-options');

            button.on('click', (e) => {
                let row = multiple.find('.js-to-clone');
                let clone = row.first().clone();

                let lastLabel = row.last().find('.input-name'),
                    lastValue = row.last().find('.input-value');

                if (lastLabel.val() && lastValue.val()) {
                    multiple.append(clone);
                    let last = multiple.find('.js-to-clone').last();
                    last.find('input').removeAttr('value');
                    last.find('[scope=row]').text(multiple.find('tr').length);

                    organizeNameFields();
                } else {
                    toastr.error('@lang('Fill in the last fields')')
                }
            });

            $('body').on('click', '.js-remove-field', (e) => {
                const element = $(e.currentTarget),
                    parent = element.parents('tr');

                if (multiple.find('tr').length > 1) {
                    parent.remove();
                } else {
                    toastr.error('@lang('Must contain at least one choice')');
                }

                $.each(multiple.find('tr'), (i, item) => {
                    $(item).find('[scope=row]').text(i + 1);

                    organizeNameFields();
                });
            });

            const organizeNameFields = () => {
                $.each(multiple.find('tr'), (i, item) => {
                    let e = $(item),
                        n = (i + 1);

                    e.find('.input-name').attr('name', 'data[' + n + '][name]');
                    e.find('.input-value').attr('name', 'data[' + n + '][value]');
                });
            };
        })(jQuery);
    </script>
@endpush
