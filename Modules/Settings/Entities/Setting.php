<?php

namespace Modules\Settings\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Companies\Entities\Company;

class Setting extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'data', 'others_data'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * @return mixed
     */
    public function getDataAttribute()
    {
        return is_json($this->attributes['data']) ? json_decode($this->attributes['data']) : $this->attributes['data'];
    }

    /**
     * @return mixed
     */
    public function getOthersDataAttribute()
    {
        return is_json($this->attributes['others_data']) ? json_decode($this->attributes['others_data']) : $this->attributes['others_data'];
    }
}
