<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$pages = config('system.settings.pages');

Route::group([
    'middleware' => ['auth', 'is:admin'],
], function () use ($pages) {
    Route::post('', 'SettingsController@store')->name('manager');

    Route::get('/', 'SettingsController@index')
        ->name('index');
});
