<?php

return [
    'name' => 'Settings',

    'pages' => [
        'settings.processes.type.documents' => [
            'text' => __('Type of documents'),
            'url'  => '/processos/tipos-de-documentos',
        ],
    ],
];
