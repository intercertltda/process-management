<?php

namespace Modules\Settings\Facades;

use Illuminate\Support\Facades\DB;

class FindSetting
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var int
     */
    private $company;

    /**
     * FindSetting constructor.
     *
     * @param string   $key
     * @param int|null $company
     */
    public function __construct(string $key, int $company = null)
    {
        $this->key = $key;
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function query()
    {
        if (isset($this->company)) {
            return DB::table('settings')
                ->where([
                    'key'        => $this->key,
                    'company_id' => $this->company,
                ])
                ->first();
        }

        return DB::table('settings')
            ->where('key', $this->key)
            ->orWhereNull('company_id')
            ->first();
    }

    /**
     * @param string $field
     *
     * @return mixed
     */
    public function get(string $field = 'data')
    {
        $query = $this->query();

        if (isset($query)) {
            return is_json($query->{$field}) ? json_decode($query->{$field}) : $query->{$field};
        } else {
            return [];
        }
    }
}
