<?php

namespace Modules\Settings\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Settings\Entities\Setting;
use Modules\Settings\Facades\FindSetting;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pages = config('system.settings.pages');
        $find = (object) array_first(array_find($pages, $request->setting, 'slug'));

        if (!isset($find->slug)) {
            abort('404', __('Page not found'));
        }

        $findSetting = new FindSetting($find->key);
        $query = $findSetting->query();
        $data = $findSetting->get();

        return view('settings::index', [
            'title'    => __('Settings'),
            'subtitle' => $find->text,
            'data'     => $data,
            'pages'    => $find,
            'setting'  => $request->setting,

            'params' => (object) [
                'slug'       => $request->setting,
                'key'        => $find->key,
                'company_id' => @$query->company_id,
            ],

            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Settings'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('settings::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (isset($request->company_id)) {
            $setting = Setting::where([
                    'key'        => $request->key,
                    'company_id' => $request->company_id,
                ])->first() ?? new Setting();
        } else {
            $setting = Setting::whereNull('company_id')
                    ->where('key', $request->key)
                    ->first() ?? new Setting();
        }

        $setting->key = $request->key;

        if (isset($request->company_id)) {
            $setting->company_id = $request->company_id;
        }

        $setting->data = is_array($request->data) ? json_encode($request->data) : $request->data;

        if (isset($make['company_id'])) {
            $setting->others_data = is_array($request->others_data) ? json_encode($request->others_data) : $request->others_data;
        }

        if ($setting->save()) {
            return redirect()
                ->route('settings.index', [
                    'setting' => $request->slug,
                ])
                ->with('success', __('Settings updated successfully!'));
        } else {
            return redirect()
                ->route('settings.index', [
                    'setting' => $request->slug,
                ])
                ->with('error', __('Settings updated successfully!'))
                ->withInput($request->all());
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('settings::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('settings::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
    }
}
