<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Settings\Entities\Setting;

class ProcessSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $setting = new Setting();

        $setting->key = 'settings.processes.type.documents';
        $setting->data = json_encode([
            [
                'name'  => 'certidao-nascimento',
                'value' => 'Certidão de Nascimento',
            ],
        ]);

        $setting->save();
    }
}
