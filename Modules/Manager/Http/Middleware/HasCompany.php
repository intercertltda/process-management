<?php

namespace Modules\Manager\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HasCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $has = DB::table('companies')
            ->where('slug', $request->company)
            ->first();

        if (!$has) {
            abort(404);
        }

        return $next($request);
    }
}
