<?php

namespace Modules\Manager\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class MakeWorkflowRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $company = DB::table('companies')
            ->where('slug', $this->company)
            ->first();

        return [
            'name'      => 'required',
            'email'     => ['required', 'email', Rule::unique('leads')->ignore($company->id, 'company_id')],
            'interest'  => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required'      => 'O seu nome é obrigatório.',
            'email.required'     => 'O seu e-mail é obrigatório.',
            'email.email'        => 'O e-mail precisa ser válido',
            'interest.required'  => 'O campo de origem é obrigatório.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
