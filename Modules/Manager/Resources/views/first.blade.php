@extends('manager::layouts.master')

@section('content')
    <div class="row d-flex justify-content-center mt-5 mb-5">
        <div class="col-xl-4 col-lg-4 col-md-6 col-12">
            <div class="widget widget-25 has-shadow">
                <div class="widget-header">
                    <h2 class="d-flex justify-content-center">@lang('Did you are interested?')</h2>
                </div>

                <div class="widget-body">
                    {{ Form::open([
                        'url' => route('manager.store', $company->slug)
                    ]) }}

                    <div class="row form-group">
                        <div class="col-12">
                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => __('Your Name')]) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => __('Your E-mail')]) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            {{ Form::text('interest', old('interest'), ['class' => 'form-control', 'placeholder' => __('How did you find us?')]) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-12 d-flex justify-content-center">
                            {{ Form::submit('Quero participar', ['class' => 'btn btn-success']) }}
                            <a href="#" class="btn btn-link">Já está negociando?</a>
                        </div>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
