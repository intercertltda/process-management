@extends('manager::layouts.master')

@section('content')
    @foreach($company->phases as $phase)
        @foreach($phase->processes as $process)
            @include('manager::phases.' . $process->type)
        @endforeach
    @endforeach
@endsection
