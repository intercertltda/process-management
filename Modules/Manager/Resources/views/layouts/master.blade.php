<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ app_title($company->social_name, @$subtitle) }}</title>
    <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/base/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/base/elisyam-1.5.css') }}">

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body class="bg-white">
    @include('system::components.preloader')

    <div class="db-smarthome page">

        <div class="page-content">
            <div class="content-inner boxed w-100">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-xl-9 col-12">
                            <header class="header">
                                <nav class="navbar">
                                    <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
                                        <div class="navbar-header">
                                            <a href="{{ $company->url }}" class="navbar-brand">
                                                <div class="brand-image brand-big">
                                                    <img src="{{ asset('images/logotipo-standard.svg') }}" alt="logo" class="logo-big">
                                                </div>
                                                <div class="brand-image brand-small">
                                                    <img src="{{ asset('images/logo-standard.svg') }}" alt="logo" class="logo-small">
                                                </div>
                                            </a>
                                        </div>
                                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
                                            <li class="nav-item"><a href="{{ route('login') }}" class="open-sidebar" title="Acessar Painel"><i class="la la-user"></i></a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </header>

                            <div class="row">
                                <div class="page-header mt-3">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h2 class="page-header-title text-center" style="margin-right: 0 !important;">Gestão de Processos</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @yield('content')

                            <footer class="second-footer text-center">
                                <p>© 2018 {{ $company->social_name }}<br />Desenvolvido por InterCert SoftHouse</p>
                                <ul class="nav mt-3 justify-content-center">
                                    <li class="nav-item">
                                        <a class="nav-link" href="documentation.html">Suporte</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="documentation.html">Contato</a>
                                    </li>
                                </ul>
                            </footer>

                        </div>
                    </div>
                </div>

                <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/vendors/js/base/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/base/core.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/app/app.min.js') }}"></script>
    <script src="{{ asset('assets/js/components/tabs/animated-tabs.min.js') }}"></script>
</body>
</html>
