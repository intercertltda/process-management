<header class="header">
    <nav class="navbar">

        <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">

            <div class="navbar-header">
                <a href="db-smarthome.html" class="navbar-brand">
                    <div class="brand-image brand-big">
                        <img src="assets/img/logo-big.png" alt="logo" class="logo-big">
                    </div>
                    <div class="brand-image brand-small">
                        <img src="assets/img/logo.png" alt="logo" class="logo-small">
                    </div>
                </a>
            </div>


            <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">

                <li class="nav-item dropdown"><a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="la la-bell animated infinite swing"></i><span class="badge-pulse"></span></a>
                    <ul aria-labelledby="notifications" class="dropdown-menu notification">
                        <li>
                            <div class="notifications-header-2">
                                <div class="title">Notifications (4)</div>
                            </div>
                        </li>
                        <li>
                            <a href="#">
                                <div class="message-icon">
                                    <i class="la la-unlock"></i>
                                </div>
                                <div class="message-body">
                                    <div class="message-body-heading">
                                        Front door unlocked
                                    </div>
                                    <span class="date">Just now</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="message-icon">
                                    <i class="la la-wifi"></i>
                                </div>
                                <div class="message-body">
                                    <div class="message-body-heading">
                                        Wifi connection lost
                                    </div>
                                    <span class="date">30 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="message-icon">
                                    <i class="la la-video-camera"></i>
                                </div>
                                <div class="message-body">
                                    <div class="message-body-heading">
                                        Outdoor camera 3 signal lost
                                    </div>
                                    <span class="date">1 hour ago</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="message-icon">
                                    <i class="la la-fire"></i>
                                </div>
                                <div class="message-body">
                                    <div class="message-body-heading">
                                        Kitchen gas activated
                                    </div>
                                    <span class="date">2 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a rel="nofollow" href="#" class="dropdown-item all-notifications text-center">View All Notifications</a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item dropdown"><a id="user" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><img src="assets/img/avatar/avatar-01.jpg" alt="..." class="avatar rounded-circle"></a>
                    <ul aria-labelledby="user" class="user-size dropdown-menu">
                        <li class="welcome">
                            <a href="#" class="edit-profil"><i class="la la-gear"></i></a>
                            <img src="assets/img/avatar/avatar-01.jpg" alt="..." class="rounded-circle">
                        </li>
                        <li>
                            <a href="pages-profile.html" class="dropdown-item">
                                Profile
                            </a>
                        </li>
                        <li>
                            <a href="app-mail.html" class="dropdown-item">
                                Messages
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-item no-padding-bottom">
                                Settings
                            </a>
                        </li>
                        <li class="separator"></li>
                        <li>
                            <a href="pages-faq.html" class="dropdown-item no-padding-top">
                                Faq
                            </a>
                        </li>
                        <li><a rel="nofollow" href="pages-login.html" class="dropdown-item logout text-center"><i class="ti-power-off"></i></a></li>
                    </ul>
                </li>


                <li class="nav-item"><a href="#off-canvas" class="open-sidebar"><i class="la la-cog"></i></a></li>

            </ul>

        </div>

    </nav>
</header>
