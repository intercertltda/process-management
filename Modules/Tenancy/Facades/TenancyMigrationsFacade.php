<?php

namespace Modules\Tenancy\Facades;

use Illuminate\Support\Facades\Schema;

class TenancyMigrationsFacade
{
    /**
     * @var array
     */
    private $connections;

    /**
     * Create a new facade instance.
     *
     * @param array $connections
     */
    public function __construct(array $connections)
    {
        $this->connections = $connections;
    }

    /**
     * @param string $table
     * @param        $callback
     *
     * @return void
     */
    public function create(string $table, $callback)
    {
        foreach ($this->connections as $connection) {
            if (!Schema::connection($connection)->hasTable($table)) {
                Schema::connection($connection)->create($table, $callback);
            }
        }
    }

    /**
     * @param string $table
     *
     * @return void
     */
    public function dropIfExists(string $table)
    {
        foreach ($this->connections as $connection) {
            if (Schema::connection($connection)->hasTable($table)) {
                Schema::connection($connection)->dropIfExists($table);
            }
        }
    }

    /**
     * @param string $table
     * @param        $callback
     *
     * @return void
     */
    public function table(string $table, $callback)
    {
        foreach ($this->connections as $connection) {
            if (Schema::connection($connection)->hasTable($table)) {
                Schema::connection($connection)->table($table, $callback);
            }
        }
    }
}
