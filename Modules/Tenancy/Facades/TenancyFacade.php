<?php

namespace Modules\Tenancy\Facades;

use Illuminate\Http\Request;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class TenancyFacade
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var string
     */
    protected $company;

    /**
     * Tenant constructor.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request = null)
    {
        $this->request = request();
        $this->company = $this->request->company;
    }

    /**
     * @return string
     */
    public static function currentDB()
    {
        return sprintf('database/companies/%s.sqlite', (new static())->company);
    }

    /**
     * @param array  $connections
     * @param string $table
     *
     * @return \Modules\Tenancy\Facades\TenancyMigrationsFacade
     */
    public static function migrate(array $connections)
    {
        return new TenancyMigrationsFacade($connections);
    }
}
