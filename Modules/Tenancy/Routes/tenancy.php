<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

Artisan::command('tenancy:db-tests', function () {
    $tenancy = database_path('tenancy.sqlite');
    $system = database_path('system.sqlite');

    $tenancy_testing = database_path('testing/tenancy_testing.sqlite');
    $system_testing = database_path('testing/system_testing.sqlite');

    if (File::exists($tenancy_testing) && File::exists($system_testing)) {
        File::delete([$tenancy_testing, $system_testing]);
    }

    File::copy($tenancy, $tenancy_testing);
    File::copy($system, $system_testing);

    if (File::exists($tenancy_testing) && File::exists($tenancy_testing)) {
        $this->info('Database copied, run tests.');
    } else {
        $this->error('Something went wrong.');
    }
});

Artisan::command('tenancy:refresh', function () {
    $default = database_path('database.sqlite');
    $system = database_path('system.sqlite');
    $tenancy = database_path('tenancy.sqlite');

    File::delete([$system, $tenancy]);

    File::copy($default, $tenancy);
    File::copy($default, $system);

    if (File::exists($tenancy) && File::exists($system)) {
        $this->info('Databases refreshed. Run migrate.');
    } else {
        $this->error('Not possible removal this databases.');
    }
});

Artisan::command('tenancy:migrate {--database=?}', function ($database = 'tenancy') {

});
