<?php

namespace Modules\Tenancy\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Clients\Entities\Client;
use Modules\Departments\Entities\Department;
use Modules\Users\Entities\User;

class TenancyDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        /**
         * 1 - Clients
         * 1 - Users
         * 2 - Departments
         */

        foreach (config('tenancy.clients') as $client) {
            $modelClient = new Client();

            $modelClient->setConnection('system');

            $modelClient->name = $client['name'];
            $modelClient->slug = $client['slug'];
            $modelClient->document = '0000000000';

            $modelClient->save();
        }

        foreach (config('tenancy.users') as $user) {
            $modelUser = new User();

            $modelUser->setConnection('system');

            $modelUser->name = $user['name'];
            $modelUser->email = $user['email'];
            $modelUser->login = $user['login'];
            $modelUser->document = '00000000000';
            $modelUser->password = bcrypt('12345');

            $modelUser->save();

            $modelUser->assignRole($user['role']);
        }

        /*foreach (config('tenancy.departments') as $department) {
            $modelDepartment = new Department();

            $modelDepartment->name = $department['name'];
            $modelDepartment->description = $department['description'];

            $userDepartment = User::where('login', $department['login'])->first();
            $modelDepartment->users()->attach($userDepartment);
        }*/
    }
}
