<?php

return [
    'name' => 'Tenancy',

    'except' => ['api', 'app'],

    'prefix' => __('panel'),

    'clients' => [
        [
            'name' => 'AR InterCert',
            'slug' => 'ar',
        ],
        [
            'name' => 'AR Crato',
            'slug' => 'ar-crato',
        ],
        [
            'name' => 'AC Link',
            'slug' => 'ac-link',
        ]
    ],


    'users' => [
        [
            'name'  => 'Setor do Comercial',
            'email' => 'comercial@empresa.com',
            'login' => 'comercial',
            'role'  => 'commercial',
        ],
        [
            'name'  => 'Setor de Marketing',
            'email' => 'marketing@empresa.com',
            'login' => 'marketing',
            'role'  => 'marketing',
        ],
        [
            'name'  => 'Setor Financeiro',
            'email' => 'financeiro@empresa.com',
            'login' => 'financeiro',
            'role'  => 'financial',
        ],
        [
            'name'  => 'Setor Suporte',
            'email' => 'suporte@empresa.com',
            'login' => 'suporte',
            'role'  => 'support',
        ],
        [
            'name'  => 'Setor do Compliance',
            'email' => 'compliance@empresa.com',
            'login' => 'compliance',
            'role'  => 'commercial',
        ],
        [
            'name'  => 'Setor de Treinamento',
            'email' => 'treinamentos@empresa.com',
            'login' => 'treinamento',
            'role'  => 'support',
        ],
        [
            'name'  => 'Administração da Empresa',
            'email' => 'gerencia@empresa.com',
            'login' => 'gerente',
            'role'  => 'directors',
        ],
        [
            'name'  => 'Analista de Sistemas',
            'email' => 'developer@empresa.com',
            'login' => 'developer',
            'role'  => 'admin',
        ],
    ],

    'departments' => [
        [
            'name'        => 'Setor do Comercial',
            'description' => 'comercial@empresa.com',
            'login'       => 'compliance',
        ],
        [
            'name'        => 'Setor de Marketing',
            'description' => 'marketing@empresa.com',
            'login'       => 'marketing',
        ],
        [
            'name'        => 'Setor Financeiro',
            'description' => 'financeiro@empresa.com',
            'login'       => 'financeiro',
        ],
        [
            'name'        => 'Setor Suporte',
            'description' => 'suporte@empresa.com',
            'login'       => 'suporte',
        ],
        [
            'name'        => 'Setor do Compliance',
            'description' => 'compliance@empresa.com',
            'login'       => 'compliance',
        ],
        [
            'name'        => 'Setor de Treinamento',
            'description' => 'suporte@empresa.com',
            'login'       => 'suporte',
        ],
        [
            'name'        => 'Administração da Empresa',
            'description' => 'compliance@empresa.com',
            'login'       => 'gerente',
        ],
    ],
];
