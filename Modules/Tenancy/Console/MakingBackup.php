<?php

namespace Modules\Tenancy\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Modules\Clients\Entities\Client;
use Modules\Tenancy\Emails\TenancyNotConfigured;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakingBackup extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tenancy:making {client}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Making files of a new Client.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = Client::where('slug', $this->argument('client'))->first();
        $default = database_path('database/tenancy.sqlite');

        if (File::exists($default)) {
            $getFile = File::get($default);
            File::put(database_path(sprintf('database/companies/%s.sqlite', $this->argument('client'))), $getFile);
        } else {
            Mail::to(config('mail.developer'))
                ->send(new TenancyNotConfigured($client, __('File database tenancy non exist')));
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['client', InputArgument::REQUIRED, 'The client is required.'],
        ];
    }
}
