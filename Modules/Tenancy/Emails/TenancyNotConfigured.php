<?php

namespace Modules\Tenancy\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Clients\Entities\Client;

class TenancyNotConfigured extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \Modules\Clients\Entities\Client
     */
    private $client;
    /**
     * @var string
     */
    private $message;

    /**
     * Create a new message instance.
     *
     * @param \Modules\Clients\Entities\Client $client
     * @param string                           $message
     */
    public function __construct(Client $client, string $message)
    {
        $this->client = $client;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('tenancy.mail.error', [
            'message' => $this->message
        ]);
    }
}
