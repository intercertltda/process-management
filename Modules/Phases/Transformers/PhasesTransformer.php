<?php

namespace Modules\Phases\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class PhasesTransformer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'reference'      => $this->reference,
            'title'          => $this->title,
            'description'    => $this->description,
            'time_execution' => $this->time_execution,
            'order'          => $this->order,
            'created_at'     => $this->created_at->format('d/m/Y \à\s H:i'),
        ];
    }
}
