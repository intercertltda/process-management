<?php

namespace Modules\Phases\Http\Controllers;

use App\Classes\Collection;
use App\Classes\Find;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Companies\Entities\Company;
use Modules\Companies\Facades\MyCompanies;
use Modules\Departments\Entities\Department;
use Modules\Phases\Entities\Phase;
use Modules\Phases\Http\Requests\StoreRequest;
use Modules\Phases\Http\Requests\UpdateRequest;
use Modules\Phases\Transformers\PhasesTransformer as Transformer;
use Modules\System\Facades\CompareRelationshipArray;
use Modules\Users\Entities\User;

class PhasesController extends Controller
{
    /**
     * PhasesController constructor.
     */
    public function __construct()
    {
//        $this->middleware(['forbid:responsible']);
    }

    /**
     * Display data depends if user is client.
     *
     * @return array
     */
    public function data(): array
    {
        if (is_admin()) {
            return [
                'companies'     => Company::all(),
                'depends'       => Phase::all(),
                'phases'        => Phase::all(),
                'departments'   => Department::all(),
                'collaborators' => User::all(),
            ];
        } else {
            return [
                'companies'     => MyCompanies::get(),
                'depends'       => MyCompanies::phases(),
                'phases'        => MyCompanies::phases(),
                'departments'   => MyCompanies::departments(),
                'collaborators' => MyCompanies::collaborators(),
            ];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->data()['phases'];

        return view('phases::index', [
            'items' => Transformer::collection($data),

            'title'      => __('Phases'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Phases'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data = (object) $this->data();

        return view('phases::create', [
            'collaborators' => Collection::setKey($data->collaborators, 'reference', 'name'),
            'departments'   => Collection::setKey($data->departments, 'reference', 'name'),
            'depends'       => $data->depends->count() > 0 ? Collection::setKey($data->depends, 'reference',
                'title') : [],
            'companies'     => Collection::setKey($data->companies, 'reference', 'social_name'),

            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.index'),
                    'title'   => __('Processes management'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @return mixed
     */
    public function store(StoreRequest $request)
    {
        $fields = (object) $request->all();

        $model = Phase::create([
            'title'          => $fields->title,
            'time_execution' => $fields->time_execution ?? 1,
            'description'    => $fields->description,
            'depends_id'     => isset($fields->depend) ? Find::db('phases', ['reference' => $fields->depend]) : null,
            'department_id'  => Find::db('departments', ['reference' => $fields->department]),
            'company_id'     => Find::db('companies', ['reference' => $fields->company]),
        ]);

        if ($model) {
            foreach ($fields->collaborators as $reference) {
                $model->responsible()
                    ->attach(Find::db('users', ['reference' => $reference]));
            }

            return redirect()
                ->route('phases.edit', $model->reference)
                ->with('success', __('Successfully created content.'));
        } else {
            return redirect()
                ->route('phases.create')
                ->withInput((array) $fields)
                ->with('fail', __('Something went wrong.'));
        }
    }

    /**
     * Show the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $data = Phase::reference($request->reference);

        if (!$data) {
            abort(404, __('Content not found'));
        }

        return view('phases::show', [
            'item'       => $data,

            'title'      => __('Show'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.index'),
                    'title'   => __('Processes management'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Show'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $data = (object) $this->data();
        $model = Phase::reference($request->reference);

        $newDepends = $data->depends->filter(function ($step) use ($request, $model) {
            return $step->reference != $request->reference and $step->order < $model->order;
        });

        return view('phases::edit', [
            'item'          => $model,
            'collaborators' => Collection::setKey($model->company->users, 'reference', 'name'),
            'departments'   => Collection::setKey($model->company->departments, 'reference', 'name'),
            'depends'       => $newDepends->count() > 0 ? Collection::setKey($newDepends, 'reference', 'title') : [],
            'companies'     => Collection::setKey($data->companies, 'reference', 'social_name'),

            'selected' => (object) [
                'collaborators' => Collection::getColumn($model->responsible, 'reference'),
            ],

            'title'      => __('Edit'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('phases.index'),
                    'title'   => __('Processes management'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Edit'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     *
     * @return Response
     */
    public function update(UpdateRequest $request)
    {
        $fields = (object) $request->all();
        $compare = (object) CompareRelationshipArray::check($fields->old_collaborators, $fields->collaborators);
        $companyID = Find::db('companies', ['reference' => $fields->company]);
        $model = Phase::reference($request->reference);

        $model->title = $fields->title;
        $model->time_execution = $fields->time_execution ?? 1;
        $model->description = $fields->description;
        $model->depends_id = Find::db('phases', ['reference' => $fields->department]);
        $model->department_id = Find::db('departments', ['reference' => $fields->department]);
        $model->company_id = $companyID;
        $model->order = DB::table('phases')->where('company_id', $companyID)->orderBy('created_at')->count() + 1;

        if ($model->save()) {
            if (count($compare->news) > 0) {
                foreach ($compare->news as $at) {
                    $model->responsible()->attach(User::reference($at)->id);
                }
            }

            // removal
            if (count($compare->removal) > 0) {
                foreach ($compare->removal as $rm) {
                    $model->responsible()->detach(User::reference($rm)->id);
                }
            }

            return redirect()
                ->route('phases.edit', $request->reference)
                ->with('success', __('Successfully updated content.'));
        } else {
            return redirect()
                ->route('phases.edit', $request->reference)
                ->withInput((array) $fields)
                ->with('fail', __('Something went wrong.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $item = Phase::reference($request->reference);

        if (!$item) {
            abort(404, __('Content not found.'));
        }

        if ($item->delete()) {
            return redirect()
                ->route('phases.index')
                ->with('success', __('Item moved to trash.'));
        } else {
            return redirect()
                ->route('phases.index')
                ->with('fail', __('This item could not be moved to the trash.'));
        }
    }
}
