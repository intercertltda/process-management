<?php

namespace Modules\Phases\Entities;

use App\Classes\Collection;
use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Companies\Entities\Company;
use Modules\Departments\Entities\Department;
use Modules\Processes\Entities\Process;
use Modules\Users\Entities\User;

class Phase extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'phases';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'time_execution', 'depends_id', 'department_id', 'company_id', 'role_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['responsible_names'];

    /**
     * @return BelongsToMany
     */
    public function responsible(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'phases_users', 'phase_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * @return HasOne
     */
    public function department(): HasOne
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    /**
     * @return HasOne
     */
    public function company(): HasOne
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * @return HasOne
     */
    public function depends(): HasOne
    {
        return $this->hasOne(self::class, 'id', 'depends_id');
    }

    /**
     * @return HasMany
     */
    public function processes(): HasMany
    {
        return $this->hasMany(Process::class, 'phase_id');
    }

    /**
     * @return string
     */
    public function getResponsibleNamesAttribute(): string
    {
        $names = Collection::getColumn($this->responsible, 'name');

        return implode(', ', $names);
    }
}
