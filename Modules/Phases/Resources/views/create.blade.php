@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', [
        'widgetTitle' => __('Data of Phase')
    ])
        {{ Form::open([
            'url' => route('phases.store'),
        ]) }}
            <div class="row form-group">
                <div class="col-md-4">
                    {{ Form::label('title', __('Title')) }}
                    {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'required']) }}
                </div>

                <div class="col-md-4">
                    {{ Form::label('time_execution', sprintf('%s (%s)', __('Time of execution'), __('in days'))) }}
                    {{ Form::number('time_execution', old('time_execution') ?? '1', ['class' => 'form-control', 'id' => 'time_execution', 'required', 'min' => '1']) }}
                </div>

                <div class="col-md-4">
                    @component('system::components.select', [
                        'select' => [
                            'name' => 'company',
                            'title' => __('Select company'),
                            'label' => __('Company'),
                            'list' => $companies,
                            'old' => old('company'),
                            'required' => true,
                            'class' => 'js-select-companies'
                        ]
                    ])
                    @endcomponent
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    @component('system::components.select', [
                        'select' => [
                            'name' => 'collaborators',
                            'name-array' => 'collaborators[]',
                            'title' => __('Select user responsible'),
                            'label' => __('Responsible'),
                            'list' => [],
                            'old' => old('collaborators'),
                            'multiple' => true,
                            'required' => true,
                            'disabled' => true
                        ]
                    ])
                    @endcomponent
                </div>

                <div class="col-md-4">
                    @component('system::components.select', [
                        'select' => [
                            'name' => 'department',
                            'title' => __('Select department'),
                            'label' => __('Department'),
                            'list' => [],
                            'old' => old('department'),
                            'required' => true,
                            'disabled' => true
                        ]
                    ])
                    @endcomponent
                </div>

                <div class="col-md-4">
                    @component('system::components.select', [
                        'select' => [
                            'name' => 'depend',
                            'title' => __('Select step'),
                            'label' => __('Depends by'),
                            'list' => [],
                            'old' => old('depend'),
                            'disabled' => true
                        ]
                    ])
                    @endcomponent
                </div>

            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    {{ Form::label('description', __('Description')) }}
                    {{ Form::textarea('description', old('description'), ['class' => 'form-control description', 'id' => 'description', 'required', 'rows' => '4']) }}
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary pull-right">@lang('Create')</button>
                </div>
            </div>
        {{ Form::close() }}
    @endcomponent
@endsection

@push('scripts')
    <script src="{{ asset('js/phases-process.min.js') }}"></script>
@endpush

