@extends('system::layouts.master')
@push('push-buttons')
    <a href="{{ router()->route('phases.create') }}" class="btn btn-success btn-sm"><i class="la la-plus"></i> @lang('Create') @lang('Phase')</a>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('system::components.table', [
                'thead' => [
                    'name' => __('Name'),
                    'processes-no-print' => __('Processes'),
                    'department' => __('Department'),
                    'responsible' => __('Responsible'),
                    'actions-center' => __('Actions')
                ]
            ])
                @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->title }}</td>
                        <td class="no-print">{{ $item->processes->count() }}</td>
                        <td>{{ $item->department->name }}</td>
                        <td>{{ $item->responsible_names }}</td>
                        <td class="td-actions text-center not-print">
                            @include('system::components.actions', [
                                'buttons' => [
                                    'edit' => [
                                        'route' => router()->route('phases.edit', $item->reference),
                                        'class' => '',
                                        'icon' => 'la la-edit',
                                        'label' => __('Edit')
                                    ],

                                    'delete' => [
                                        'route' => '#',
                                        'class' => 'js-confirm-action',
                                        'icon' => 'la la-close',
                                        'label' => __('Delete'),
                                        'attributes' => [
                                            'data-local' => 'etapas',
                                            'data-reference' => $item->reference
                                        ]
                                    ]
                                ]
                            ])
                        </td>
                    </tr>
                @endforeach
            @endcomponent
        </div>
    </div>
@endsection
