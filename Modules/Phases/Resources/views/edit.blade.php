@extends('system::layouts.master')
@push('push-buttons')
    <a href="{{ router()->route('phases.create') }}" class="btn btn-success btn-sm mb-2"><i class="la la-plus"></i> @lang('Create') @lang('Phase')</a>
    {{--<a href="{{ router()->route('manager.processes.create', ['reference' => $item->reference]) }}" class="btn btn-info btn-sm mb-2"><i class="la la-plus"></i> @lang('Create') @lang('Process')</a>--}}
@endpush
@section('content')
    @component('system::components.page-content', [
        'widgetTitle' => __('Data of Phase')
    ])
        {{ Form::open([
                'url' => router()->route('phases.update', $item->reference)
            ]) }}
        {{ Form::hidden('__method', 'PUT') }}
        <div class="row form-group">
            <div class="col-md-4">
                {{ Form::label('title', __('Title')) }}
                {{ Form::text('title', old('title') ?? $item->title, ['class' => 'form-control', 'id' => 'title', 'required']) }}
            </div>

            <div class="col-md-4">
                {{ Form::label('time_execution', sprintf('%s (%s)', __('Time of execution'), __('in days'))) }}
                {{ Form::number('time_execution', old('time_execution') ?? $item->time_execution ?? '1', ['class' => 'form-control', 'id' => 'time_execution', 'required', 'min' => '1']) }}
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'company',
                        'title' => __('Select company for this department'),
                        'label' => __('Company'),
                        'list' => $companies,
                        'old' => old('company') ?? $item->company->reference,
                        'required' => true,
                        'disabled' => true
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                @foreach ($selected->collaborators as $i)
                    {{ Form::hidden('old_collaborators[]', $i) }}
                @endforeach

                @component('system::components.select', [
                    'select' => [
                        'name' => 'collaborators',
                        'name-array' => 'collaborators[]',
                        'title' => __('Select user responsible'),
                        'label' => __('Responsible'),
                        'list' => $collaborators,
                        'old' => old('collaborators') ?? $selected->collaborators,
                        'multiple' => true,
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'department',
                        'title' => __('Select department'),
                        'label' => __('Department'),
                        'list' => $departments,
                        'old' => old('department') ?? $item->department->reference,
                        'required' => true
                    ]
                ])
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'depend',
                        'title' => __('Select step'),
                        'label' => __('Depends by'),
                        'list' => $depends,
                        'old' => old('depend') ?? $item->depends->reference ?? null
                    ]
                ])
                @endcomponent
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                {{ Form::label('description', __('Description')) }}
                {{ Form::textarea('description', old('description') ?? $item->description, ['class' => 'form-control description', 'id' => 'description', 'required', 'rows' => '4']) }}
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Update')</button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent

    @component('system::components.page-content', [
        'widgetTitle' => __('Processes of Phase'),
        'widgetButtons' => [
            [
                'label' => sprintf('%s %s', __('Create'), __('Process')),
                'url' => router()->route('processes.create', ['reference' => $item->reference]),
                'icon' => 'la la-plus'
            ]
        ]
    ])
        @component('system::components.sortable', ['type' => 'processos'])
            @foreach ($item->processes()->orderBy('order')->get() as $process)
                <li data-old-order="{{ ($loop->index + 1) }}"
                    class="list-group-item d-flex justify-content-between"
                    data-reference="{{ $process->reference }}"
                    data-status="{{ $process->status }}">
                    <div class="pull-left">
                        <i class="la la-sort js-sort"></i>
                        <span>{{ $process->title }}</span>
                    </div>
                    <div class="pull-right">

                        <a href="{{ router()->route('processes.edit', $process->reference) }}" class="badge badge-info">Editar</a>
                        <a href="javascript:void(0)" class="badge badge-{{ $process->status === 'on' ? 'danger' : 'success' }} js-status">{{ $process->status === 'on' ? 'Desabilitar' : 'Habilitar' }}</a>
                        <span class="badge badge-primary badge-pill js-sortable-index">{{ ($loop->index + 1) }}</span>
                    </div>
                </li>
            @endforeach
        @endcomponent
    @endcomponent
@endsection

@push('scripts')
    <script src="{{ asset('js/phases-process.min.js') }}"></script>
@endpush
