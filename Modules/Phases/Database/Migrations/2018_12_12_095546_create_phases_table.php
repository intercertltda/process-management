<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('phases', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('department_id')
                ->unsigned();

            $table->integer('depends_id')
                ->unsigned()
                ->nullable();

            $table->string('title');
            $table->string('description')->nullable();
            $table->string('time_execution')->nullable();

            $table->integer('order')
                ->default(1)
                ->nullable();

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('phases', function (Blueprint $table) {
            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('depends_id')
                ->references('id')
                ->on('phases');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('phases');
    }
}
