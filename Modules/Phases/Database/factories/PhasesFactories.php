<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Phases\Entities\Phase::class, function (Faker $faker) {
    $department = factory(\Modules\Departments\Entities\Department::class)->create();

    return [
        'title'          => $faker->words(rand(3, 6), true),
        'description'    => $faker->text(50),
        'time_execution' => rand(1, 10),
        'order'          => rand(0, 20),
        'department_id'  => $department->id,
        'company_id'     => $department->company_id,
    ];
});

