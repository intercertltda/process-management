<?php

namespace Modules\Phases\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Phases\Entities\Phase;
use Modules\Users\Entities\User;

class PhasesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(Phase::class, 50)
            ->create()
            ->each(function ($phase) {
                $phase->responsible()->save(factory(User::class)->create());
            });
    }
}
