<?php

namespace Modules\Zebra\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ZebraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('zebra::index', [
            'title'      => __('Zebra'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard'
                ],
                [
                    'title'   => __('Zebra'),
                    'current' => true,
                    'icon'    => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('zebra::create', [
            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('zebra.index'),
                    'title'   => __('Zebra'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('zebra::show', [
            'title'      => __('Show'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('zebra.index'),
                    'title'   => __('Zebra'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Show'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        return view('zebra::edit', [
            'title'      => __('Edit'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('zebra.index'),
                    'title'   => __('Zebra'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Edit'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
    }
}
