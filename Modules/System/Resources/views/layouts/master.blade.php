<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="user-reference" content="{{ $user->reference }}">
    <meta name="api-url" content="{{ config('app.api') }}">
    <meta name="theme-color" content="#43A2DE">

    <title>{{ app_title(@$title, @$subtitle) }}</title>

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- Favicon -->
    {{--<link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">--}}
    {{--<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">--}}
    {{--<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">--}}


    @include('system::partials.styles')
    @stack('styles')
    @stack('inline-styles')

</head>
<body id="page-top">
    @include('system::components.preloader')


    <div class="page">
        @include('system::partials.header')
        <div class="page-content d-flex align-items-stretch">
            @include('system::partials.sidebar')
            <div class="content-inner">
                <div class="container-fluid">
                    @include('system::partials.subheader')
                    @yield('content')
                </div>

                @include('system::partials.footer')
                <a href="#" class="go-top"><i class="la la-arrow-up"></i></a>

                @include('system::partials.off-sidebar')
            </div>
            <!-- End Content -->
        </div>
        <!-- End Page Content -->
    </div>


    <!-- Begin Success Modal -->
    <div id="delay-modal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="sa-icon sa-success animate" style="display: block;">
                        <span class="sa-line sa-tip animateSuccessTip"></span>
                        <span class="sa-line sa-long animateSuccessLong"></span>
                        <div class="sa-placeholder"></div>
                        <div class="sa-fix"></div>
                    </div>
                    <div class="section-title mt-5 mb-5">
                        <h2 class="text-dark">Meeting successfully created</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Success Modal -->
    <!-- Begin Modal -->
    <div id="modal-view-event" class="modal modal-top fade calendar-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title event-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="media">
                        <div class="media-left align-self-center mr-3">
                            <div class="event-icon"></div>
                        </div>
                        <div class="media-body align-self-center mt-3 mb-3">
                            <div class="event-body"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->


    @stack('modals')

    @include('system::partials.scripts')

    @stack('scripts')

    @stack('inline-scripts')

    <script src="{{ asset('components/bootstrap/js/dist/util.js') }}"></script>
    <script src="{{ asset('components/bootstrap/js/dist/modal.js') }}"></script>

</body>
</html>
