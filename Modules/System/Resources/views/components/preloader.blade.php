<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        <img src="{{ asset('assets/img/site-logo.svg') }}" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>
<!-- End Preloader -->
