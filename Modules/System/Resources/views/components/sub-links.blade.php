<div class="{{ $col ?? 'col-md-6' }}">
    <ul class="nav justify-content-end nav-pills">
        @foreach ($links as $link)
            <li class="nav-item">
                <a class="nav-link {{ in_route($link['route']) ? 'active' : '' }}" href="{{ router()->route($link['route']) }}">{{ $link['text'] }}</a>
            </li>
        @endforeach
    </ul>
</div>
