{{--<div class="btn-group" role="group" aria-label="Buttons Group">--}}
    @foreach ($buttons as $name => $button)
        @php
            $attrs = '';
            if (isset($button['attributes']))
                foreach ($button['attributes'] as $key => $val)
                {
                    $attrs .= sprintf(' %s=%s', $key, $val);
                }
        @endphp
        <a href="{{ $button['route'] }}" class="{{ $button['class'] }}" title="{{ $button['label'] }}"{{ $attrs ?? '' }}><i class="{{ $button['icon'] }} {{ $name }}"></i></a>
    @endforeach
{{--</div>--}}
