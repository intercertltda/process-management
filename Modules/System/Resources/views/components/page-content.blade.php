<div class="row">
    <div class="col-md-12">
        <!-- Page Content -->
        <div class="widget has-shadow">
            @isset($widgetTitle)
                <div class="widget-header bordered d-flex align-items-center">
                    <h2>{{ $widgetTitle }}</h2>
                    @isset($widgetButtons)
                        <div class="widget-options">
                            <div class="btn-group" role="group">
                                @foreach ($widgetButtons as $btn)
                                    @if(isset($btn['url']))
                                        <a href="{{ $btn['url'] ?? '#' }}" class="btn btn-{{ $btn['type'] ?? 'primary' }} ripple {{ $btn['class'] ?? '' }}" {!! $btn['attributes'] ?? '' !!}>
                                            @isset($btn['icon'])<i class="{{ $btn['icon'] }}"></i>@endisset
                                            <span class="d-none d-sm-inline-block">{{ $btn['label'] }}</span>
                                        </a>
                                    @else
                                        <button type="button" class="btn btn-{{ $btn['type'] ?? 'primary' }} ripple {{ $btn['class'] ?? '' }}" {!! $btn['attributes'] ?? '' !!}>
                                            @isset($btn['icon'])<i class="{{ $btn['icon'] }}"></i>@endisset
                                            <span class="d-none d-sm-inline-block">{{ $btn['label'] }}</span>
                                        </button>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endisset
                </div>
            @endisset
            <div class="widget-body {{ $widgetClasses ?? '' }}">
                {{ $slot }}
            </div>
        </div>
        <!-- Page Content -->
    </div>
</div>
