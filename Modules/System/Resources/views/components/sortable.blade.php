<div class="list-ordering" id="list-ordering">
    <ul class="list-group js-sortable" data-type="{{ $type ?? null }}">
        {{ $slot }}
    </ul>
</div>

@push('scripts')
    <script type="text/javascript" src="{{ asset('components/jquery-sortable/source/js/jquery-sortable-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/ordering.min.js') }}"></script>
@endpush

@push('inline-styles')
    <style>
        body.dragging, body.dragging * {
            cursor: move !important;
        }

        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        .js-sortable li {
            list-style-type: none;
        }

        .js-sortable li.placeholder {
            position: relative;
            /** More li styles **/
        }
        .js-sortable li.placeholder:before {
            position: absolute;
            /** Define arrowhead **/
        }
    </style>
@endpush