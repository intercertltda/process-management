<!-- Export -->
<div class="widget has-shadow">
    @isset($widgetTitle)
        <div class="widget-header bordered no-actions d-flex align-items-center">
            <h4>{{ $widgetTitle }}</h4>
        </div>
    @endisset
    <div class="widget-body">
        <div class="row">
            <div class="table-responsive">
                <table id="{{ !isset($exportable) ? 'export-table' : 'sorting-table' }}" class="table mb-0" data-page-length="{{ $length ?? 10 }}" data-order="[[ {{ $order['col'] ?? 1 }}, &quot;{{ $order['type'] ?? 'asc' }}&quot; ]]">
                    <thead>
                    @foreach ($thead as $key => $th)
                        @php
                            if (str_contains($key, 'center')) {
                                $class = 'text-center';
                            } elseif (str_contains($key, 'right')) {
                                $class = 'text-right';
                            } else {
                                $class = 'text-left';
                            }
                        @endphp
                        <th class="{{ $class }} {{ $key }} {{ str_contains($key, ['actions', 'no-print']) ? 'no-print' : '' }}">{{ $th }}</th>
                    @endforeach
                    </thead>
                    <tbody>
                    {{ $slot }}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Export -->

@push('styles')
    <link href="{{ asset('assets/css/datatables/datatables.min.css')}}" rel="stylesheet">
@endpush


@push('scripts')
    <script src="{{ asset('assets/vendors/js/datatables/datatables.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/datatables/buttons.print.min.js') }}"></script>

    <!-- Begin Page Snippets -->
    <script src="{{ asset('js/tables.min.js')}}"></script>
    <!-- End Page Snippets -->
@endpush
