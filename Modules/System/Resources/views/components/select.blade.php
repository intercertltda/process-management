@php
    $selectParams = [
        'class' => sprintf('form-control selectpicker %s %s', $select['name'], $select['class'] ?? ''),
        'id' => $select['name'],
        'title' => $select['title'] ?? 'Selecione um item',
        'data-live-search' => 'true',
        'data-size' => 4,
        isset($select['multiple']) && $select['multiple'] ? 'multiple' : null,
        isset($select['required']) && $select['required'] ? 'required' : null,
        isset($select['disabled']) && $select['disabled'] ? 'disabled' : null,
    ];

    if (isset($select['attr'])) {
        foreach ($select['attr'] as $key => $attr)
            $selectParams[$key] = $attr;
    }
@endphp

@isset($select['label'])
    {{ Form::label($select['name'], $select['label']) }}
@endisset
{{ Form::select($select['name-array'] ?? $select['name'], $select['list'], $select['old'], $selectParams) }}

