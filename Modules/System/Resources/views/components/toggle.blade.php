
<div class="styled-checkbox">
    <input name="{{ $name ?? 'toggle' }}" id="{{ $name ? str_slug($name) : 'toggle' }}" {{ isset($multiple) and $multiple === 'on' ? 'checked' : '' }} type="checkbox" class="input-toggle">
    <label for="input-{{ $name ? str_slug($name) : 'toggle' }}"></label>
</div>