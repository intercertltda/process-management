<!-- Stylesheet of components -->
<link href="{{ asset('components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('components/owl.carousel/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('components/owl.carousel/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
<link href="{{ asset('components/PACE/themes/blue/pace-theme-minimal.css')}}" rel="stylesheet">
<link href="{{ asset('components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" rel="stylesheet">
<link href="{{ asset('components/toastr/build/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">

<!-- Stylesheet -->
<link rel="stylesheet" href="{{ asset('assets/vendors/css/base/elisyam-1.5.css') }}">
<link rel="stylesheet" href="{{ asset('css/app.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/animate/animate.min.css') }}">


<!-- Tweaks for older IEs--><!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

<style>
    .toastr-top-right {
        margin: 15px;
    }
</style>
