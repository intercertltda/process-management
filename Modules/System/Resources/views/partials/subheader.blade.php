<!-- Begin Page Header-->
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">
                {{ $title ?? '' }}@isset($subtitle) <small>{{ $subtitle }}</small>@endisset
                <span class="btn-group" style="margin-left: 10px;">
                    @stack('push-buttons')
                </span>
            </h2>
            <div>
                <ul class="breadcrumb">
                    @foreach ($breadcrumb as $key => $item)
                        @if($item['current'])
                            <li class="breadcrumb-item active" aria-current="page">@isset($item['icon'])<i class="{{ $item['icon'] }}"></i>@endisset {{ $item['title'] }}</li>
                        @else
                            <li class="breadcrumb-item">
                                <a href="{{ $item['url'] }}">
                                    @isset($item['icon'])<i class="{{ $item['icon'] }}"></i>@endisset {{ $item['title'] }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Page Header -->


@if($errors->any() or session()->has('fail') or session()->has('success'))
    @if ($errors->any())
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    @if(session()->has('fail'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <span>{{ session()->get('fail') }}</span>
                </div>
            </div>
        </div>
    @endif
    @if(session()->has('success'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success">
                    <span>{{ session()->get('success') }}</span>
                </div>
            </div>
        </div>
    @endif
@endif
