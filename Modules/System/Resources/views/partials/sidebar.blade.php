<div class="default-sidebar">
    <!-- Begin Side Navbar -->
    <nav class="side-navbar box-scroll sidebar-scroll">
        <!-- Begin Main Navigation -->
        <ul class="list-unstyled">
            @foreach (config('system.menu') as $key => $item)
                @if(in_array(router()->type(), $item['show']))
                    @if(is_check($item['param'], $item['check']))
                        @if($item['type'] == 'link')
                            <li class="{{ in_route($key) ? 'active' : '' }}">
                                <a href="{{ get_route($item['route']) }}">
                                    <i class="{{ $item['icon'] }}"></i><span>{{ $item['text'] }}</span>
                                </a>
                            </li>
                        @elseif($item['type'] == 'items')
                            <li class="{{ in_route($key) ? 'active' : '' }}">
                                <a href="#dropdown-{{ $loop->index }}" aria-expanded="{{ in_route($key) ? 'true' : 'false' }}" data-toggle="collapse">
                                    <i class="{{ $item['icon'] }}"></i><span>{{ $item['text'] }}</span></a>
                                <ul id="dropdown-{{ $loop->index }}" class="collapse list-unstyled {{ in_route($key) ? 'show' : '' }} pt-0">
                                    @foreach ($item['items'] as $subKey => $subItem)
                                        @if(is_check($subItem['param'], $subItem['check']))
                                            <li class="{{ in_route($subItem['route']) ? 'active' : '' }}">
                                                <a href="{{ isset($subItem['route-params']) ? router()->route($subItem['route'], $subItem['route-params']) : get_route($subItem['route']) }}" class="{{ in_route($subItem['route']) ? 'active' : '' }}">{{ $subItem['text'] }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endif
                @endif
            @endforeach
        </ul>
        <!-- End Main Navigation -->
    </nav>
    <!-- End Side Navbar -->
</div>
<!-- End Left Sidebar -->
