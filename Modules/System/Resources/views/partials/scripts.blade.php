

<script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('components/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $.widget.bridge('uibutton', $.ui.button);
        window.paceOptions = {
            ajax: {
                trackMethods: ['GET', 'POST', 'PUT', 'DELETE', 'REMOVE']
            }
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-USER-TOKEN': $('meta[name="user-reference"]').attr('content'),
                'X-ACCESS-TOKEN': '{{ base64_encode(env('API_TOKEN')) }}'
            }
        });
    </script>


    <!-- Begin Page Vendor Js -->
    <script src="{{ asset('components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('components/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('components/clipboard/dist/clipboard.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/nicescroll/nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/js/noty/noty.min.js') }}"></script>
    <script src="{{ asset('components/PACE/pace.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- End Page Vendor Js -->

    <script src="{{ asset('js/system.min.js')}}"></script>

    <script>
        (($) => {
            'use strict';
            $('select.selectpicker').selectpicker();


            $('input.input-toggle').bootstrapToggle({
                onstyle: 'success',
                offstyle: 'primary'
            });
        })(jQuery);
    </script>
