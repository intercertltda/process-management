<?php

return [
    'name' => 'InterSIG - Gestão de Processos',

    'prefix' => 'painel',

    'menu' => [
        'dashboard' => [
            'url'   => '/',
            'route' => 'dashboard.index',
            'icon'  => 'la la-columns',
            'text'  => __('Dashboard'),
            'type'  => 'link',

            'check' => 'permission',
            'param' => ['panel'],

            'show' => ['app', 'tenancy'],
        ],

        'workflow' => [
            'route' => 'workflow.index',
            'icon'  => 'la la-building-o',
            'text'  => __('Workflow'),
            'type'  => 'link',

            'check' => 'role',
            'param' => ['directors', 'responsible'],

            'show' => ['tenancy'],
        ],

        'clients' => [
            'route' => 'clients.index',
            'icon'  => 'la la-puzzle-piece',
            'text'  => __('Clients'),
            'type'  => 'items',

            'check' => 'role',
            'param' => ['admin', 'directors', 'responsible'],

            'show' => ['app'],

            'items' => [
                'clients.index' => [
                    'route' => 'clients.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Listing'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'clients.create' => [
                    'route' => 'clients.create',
                    'icon'  => 'la la-plus',
                    'text'  => __('Create'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],

        'phases' => [
            'route' => 'phases.index',
            'icon'  => 'la la-puzzle-piece',
            'text'  => __('Phases'),
            'type'  => 'items',

            'check' => 'role',
            'param' => ['admin', 'directors', 'responsible'],

            'show' => ['tenancy'],

            'items' => [
                'phases.index' => [
                    'route' => 'phases.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Listing'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'phases.create' => [
                    'route' => 'phases.create',
                    'icon'  => 'la la-plus',
                    'text'  => __('Create'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],
        'viability'     => [
            'route' => 'viability.index',
            'icon'  => 'la la-list-alt',
            'text'  => __('Viability'),
            'type'  => 'items',

            'check' => 'role',
            'param' => ['responsible', 'admin'],

            'show' => ['tenancy'],

            'items' => [
                'viability.index' => [
                    'route' => 'viability.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Listing'),
                    'type'  => 'link',

                    'check' => 'role',
                    'param' => ['admin', 'responsible'],
                ],

                'viability.create' => [
                    'route' => 'viability.create',
                    'icon'  => 'la la-plus',
                    'text'  => __('Create'),
                    'type'  => 'link',

                    'check' => 'role',
                    'param' => ['admin', 'responsible'],
                ],

                'viability.responses' => [
                    'route' => 'viability.responses',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Responses'),
                    'type'  => 'link',

                    'check' => 'role',
                    'param' => ['responsible', 'admin'],
                ],
            ],
        ],
        'departments'   => [
            'route' => 'departments.index',
            'icon'  => 'la la-building',
            'text'  => __('Departments'),
            'type'  => 'items',

            'check' => 'role',
            'param' => ['responsible', 'admin'],

            'show' => ['tenancy'],

            'items' => [
                'departments.index' => [
                    'route' => 'departments.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Listing'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'departments.create' => [
                    'route' => 'departments.create',
                    'icon'  => 'la la-plus',
                    'text'  => __('Create'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],
        'branches'      => [
            'route' => 'branches.index',
            'icon'  => 'la la-building',
            'text'  => __('Branches'),
            'type'  => 'link',

            'check' => 'role',
            'param' => ['responsible', 'admin', 'directors'],

            'show' => ['tenancy'],
        ],
        'leads'         => [
            'route' => 'leads.index',
            'icon'  => 'la la-users',
            'text'  => __('Leads'),
            'type'  => 'items',
            'check' => 'role',
            'param' => ['admin', 'marketing', 'commercial'],

            'show' => ['tenancy'],

            'items' => [
                'leads.index'  => [
                    'route' => 'leads.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Listing'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
                'leads.create' => [
                    'route' => 'leads.create',
                    'icon'  => 'la la-plus',
                    'text'  => __('Create'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],
            ],
        ],
        'users'         => [
            'route' => 'users.index',
            'icon'  => 'la la-users',
            'text'  => __('Collaborators'),
            'type'  => 'items',
            'check' => 'role',
            'param' => ['directors', 'admin'],

            'show' => ['app', 'tenancy'],

            'items' => [
                'users.index' => [
                    'route' => 'users.index',
                    'icon'  => 'la la-list-ul',
                    'text'  => __('Listing'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['panel'],
                ],

                'users.create' => [
                    'route' => 'users.create',
                    'icon'  => 'la la-plus',
                    'text'  => __('Create'),
                    'type'  => 'link',

                    'check' => 'permission',
                    'param' => ['admin'],
                ],
            ],
        ],
        'settings'      => [
            'route' => 'settings.index',
            'icon'  => 'la la-cog',
            'text'  => __('Settings'),
            'type'  => 'items',
            'check' => 'role',
            'param' => ['directors', 'admin'],
            'show'  => ['app', 'tenancy'],
            'items' => [
                'settings.index' => [
                    'route'        => 'settings.index',
                    'route-params' => [
                        'setting' => 'tipos-de-documentos',
                    ],
                    'icon'         => 'la la-list-ul',
                    'text'         => __('Processes'),
                    'type'         => 'link',

                    'check' => 'role',
                    'param' => ['admin'],
                ],
            ],
        ],
    ],

    'settings' => [
        'pages' => [
            [
                'slug' => 'tipos-de-documentos',
                'key'  => 'settings.processes.type.documents',
                'text' => __('Type of documents'),
            ],
        ],
    ],
];
