<?php

namespace Modules\System\Facades;


class ApiRouterFacade
{
    /**
     * Create a new facade instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param string     $route
     * @param array|null $params
     *
     * @return string
     */
    public function route(string $route, array $params = null)
    {
        $params['subdomain'] = 'api';

        if (stripos($route, 'api.') === false) {
            $route = sprintf('api.%s', $route);
        }

        return route($route, $params);
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function url(string $url)
    {
        if (!substr($url, 0, 1) === '/') {
            $url = '/'.$url;
        }

        return sprintf('%s%s', $this->route('index'), $url);
    }
}
