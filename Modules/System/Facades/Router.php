<?php

namespace Modules\System\Facades;


use Illuminate\Support\Facades\Route;

class Router
{
    const SUB_DOMAIN = '{subdomain}';

    /**
     * @var string
     */
    private $section;

    /**
     * @var string
     */
    private $namespace;

    /**
     * Create a new facade instance.
     *
     * @param string|null $section
     * @param string|null $namespace
     */
    public function __construct(string $section = null, string $namespace = null)
    {
        $this->section = $section;
        $this->namespace = $namespace;
    }

    /**
     * @return string
     */
    public function domain(): string
    {
        return sprintf('%s.%s', self::SUB_DOMAIN, config('app.domain'));
    }

    /**
     * @param bool        $hasPrefix
     * @param string|null $only
     * @param bool        $prefixName
     *
     * @return $this
     */
    public function tenancy(bool $hasPrefix = false, string $only = null, $prefixName = true)
    {
        $prefix[] = config('system.prefix') ? config('system.prefix') : null;

        if ($hasPrefix) {
            $prefix[] = __(strtolower($this->section));
        }

        Route::domain($this->domain())
            ->prefix(implode('/', $prefix))
            ->middleware([sprintf('tenancy:%s', $only ?? 'all'), 'web'])
            ->name($prefixName ? sprintf('%s.', strtolower($this->section)) : null)
            ->namespace($this->namespace)
            ->group(sprintf('%s/Routes/web.php', module_path($this->section)));

        return $this;
    }

    /**
     * @param array|null $params
     *
     * @return $this
     */
    public function group(array $params = null)
    {
        $only = $params['tenancy'] ?? null;
        $prefix = $params['prefix'] ?? null;
        $as = $params['as'] ?? null;
        $asPrefix = $params['as_prefix'] ?? null;
        $withPanelPrefix = $params['panel_prefix'] ?? null;

        $section = strtolower($this->section);

        $panel = config('system.prefix');
        $convertPanel = !is_array($panel) ? explode('/', $panel) : $panel;

        if (isset($params)) {
            // middleware
            $middleware[] = 'web';

            if (isset($only)) {
                $tenancyOnly = !is_array($only) ? [$only] : $only;

                $setMiddleware = array_merge($middleware, [sprintf('tenancy:%s', implode(',', $tenancyOnly))]);
            }

            // prefix
            if (isset($prefix)) {
                $convertPrefix = !is_array($prefix) ? explode('/', $prefix) : $prefix;
                $prefixes = array_values(array_filter(array_merge($convertPanel, $convertPrefix)));
                $setPrefix = !is_bool($prefix) ? implode('/', $prefixes) : null;
            }

            // set as
            if (isset($as)) {
                $convertNameAs = !is_array($as) ? explode('.', $as) : $as;

                if (isset($asPrefix)) {
                    $convertNameAsPrefix = !is_array($asPrefix) ? explode('.', $asPrefix) : $asPrefix;

                    $asArr = array_merge($convertNameAsPrefix, $convertNameAs);
                } else {
                    $asArr = $convertNameAs;
                }

                $setAs = implode('.', $asArr);
            }
        }

        // has dot on end string
        if (isset($setAs) && substr($as, (strlen($as) - 1), strlen($as)) !== '.') {
            $setAs = $setAs.'.';
        } else {
            $setAs = $section.'.';
        }

        if (!isset($prefix)) {
            $prefixes = array_values(array_filter(array_merge($convertPanel, [__($section)])));

            $setPrefix = implode('/', $prefixes);
        }

        // check if exists others middleware
        if (isset($params['middleware'])) {
            $setMiddleware = array_merge($setMiddleware ?? [], !is_array($params['middleware']) ? [$params['middleware']] : $params['middleware']);
        }

        if (isset($withPanelPrefix) && is_bool($withPanelPrefix) && $withPanelPrefix) {
            $setPrefix = $panel.'/';
        }

        Route::domain($this->domain())
            ->prefix($setPrefix ?? null)
            ->middleware($setMiddleware ?? ['web'])
            ->name($setAs ?? null)
            ->namespace($this->namespace)
            ->group(sprintf('%s/Routes/web.php', module_path($this->section)));

        return $this;
    }

    /**
     * @param bool        $prefix
     * @param string|null $only
     * @param bool        $prefixName
     *
     * @return $this
     */
    public function api(bool $prefix = false, string $only = null, $prefixName = true)
    {
        Route::domain($this->domain())
            ->prefix($prefix ? __(strtolower($this->section)) : null)
            ->middleware(['tenancy:api', 'api'])
            ->name($prefixName ? sprintf('api.%s.', strtolower($this->section)) : null)
            ->namespace($this->namespace)
            ->group(sprintf('%s/Routes/api.php', module_path($this->section)));

        return $this;
    }

    /**
     * @return string
     */
    public function type()
    {
        switch (request()->subdomain) {
            case 'app':
                return 'app';
            break;
            case 'api':
                return 'api';
            break;
            default:
                return 'tenancy';
            break;
        }
    }

    /**
     * @param string $route
     * @param null   $options
     *
     * @return string
     */
    public function route(string $route, $options = null)
    {
        if (isset($options)) {
            $subdomain = array_key_exists('subdomain', $options) ? $options['subdomain'] : request()->subdomain;
        } else {
            $subdomain = request()->subdomain;
        }

        $params = [
            'subdomain' => $subdomain ?? 'app'
        ];

        if (isset($options)) {
            $params = array_merge($params, $options);
        }

        return Route::has($route) ? route($route, $params) : url('/');
    }

    /**
     * @param string $route
     * @param array  $options
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect(string $route, array $options = null)
    {
        if (isset($options)) {
            $subdomain = array_key_exists('subdomain', $options) ? $options['subdomain'] : request()->subdomain;
        } else {
            $subdomain = request()->subdomain;
        }

        $params['subdomain'] = $subdomain ?? 'app';

        return redirect()
            ->route($route, array_merge($params, $options ?? []));
    }

    /**
     * @param string $controller
     * @param string $item
     */
    public function resources(string $controller, string $item = '{reference}')
    {
        Route::get('/', $controller.'@index')->name('index');
        Route::get(__('router.create'), $controller.'@create')->name('create');
        Route::post('/', $controller.'@store')->name('store');
        Route::get($item, $controller.'@show')->name('show');
        Route::get($item.'/'.__('router.edit'), $controller.'@edit')->name('edit');
        Route::put($item, $controller.'@update')->name('update');
        Route::delete($item, $controller.'@destroy')->name('destroy');
    }
}
