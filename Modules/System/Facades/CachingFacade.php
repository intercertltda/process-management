<?php

namespace Modules\System\Facades;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CachingFacade
{
    /**
     * @return int
     */
    public static function time(): int
    {
        return config('developer.cache.list');
    }

    /**
     * @param string     $name
     * @param array|null $select
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|mixed
     */
    public static function table(string $name, array $select = null, $custom = false)
    {
        $key = !$custom ? $name : now()->format('Y-m-d');

        if (Cache::has($key)) {
            $response = Cache::get($key);
        } else {
            $response = DB::table($name);
            if (isset($select)) {
                $response = $response->select($select);
            }
            $response = $response->get();

            Cache::put($key, $response, self::time());
        }

        return collect($response);
    }

    /**
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|mixed
     */
    public static function users()
    {
        return self::table('users');
    }
}
