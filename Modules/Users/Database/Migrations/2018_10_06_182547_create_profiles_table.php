<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return mixed
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type_other_document')->default('nothing');
            $table->string('other_document')->nullable();
            $table->string('phone')->nullable();
            $table->string('genre')->default('other');

            $table->morphs('profileable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return mixed
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('profiles');
    }
}
