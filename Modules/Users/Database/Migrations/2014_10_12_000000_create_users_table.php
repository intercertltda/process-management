<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Tenancy::migrate(['system', 'tenancy'])->create('users', function (Blueprint $table) {
            $table->increments('id');

            /*
             * Define o Nome do usuário
             */
            $table->string('name');

            /*
             * Define o tipo de Usuário
             * user = Funcionário
             * client = Cliente
             */
            $table->string('type')->default('user');

            $table->string('email')->unique(); // email
            $table->string('login')->unique(); // login
            $table->string('document')->unique()->nullable(); // CPF apenas números

            $table->string('enabled')->default('no');
            $table->string('password');
            $table->timestamp('logged_at')->nullable();

            $table->uuid('reference')->unique();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Tenancy::migrate(['system', 'tenancy'])->dropIfExists('users');
    }
}
