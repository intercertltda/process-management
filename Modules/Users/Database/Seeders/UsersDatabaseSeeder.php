<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Users\Entities\User;

class UsersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (config('tenancy.users') as $user) {
            $modelUser = new User();

            $modelUser->setConnection($this->command->option('database'));

            $modelUser->name = $user['name'];
            $modelUser->email = $user['email'];
            $modelUser->login = $user['login'];
            $modelUser->remember_token = str_random(16);
            $modelUser->document = sprintf('%s%s%s', rand(1111, 9999), rand(1111, 9999), rand(1111, 9999));
            $modelUser->password = bcrypt('12345');

            $modelUser->save();

            $modelUser->assignRole($user['role']);
        }
    }
}
