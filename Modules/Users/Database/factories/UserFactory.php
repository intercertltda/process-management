<?php

$factory->define(\Modules\Users\Entities\User::class, function () {
    $faker = \Faker\Factory::create('pt_BR');

    return [
        'name'           => $faker->name,
        'type'           => 'user',
        'login'          => $faker->unique()->userName.rand(0,100),
        'document'       => $faker->cpf(false),
        'enabled'        => true,
        'email'          => sprintf('example_%s_%s', rand(0,100), $faker->unique()->safeEmail),
        'password'       => bcrypt('12345'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->afterCreating(\Modules\Users\Entities\User::class, function ($user, $faker) {
    $user->profile()->save(factory(\Modules\Users\Entities\Profile::class)->create([
        'profileable_type' => 'Modules\\Users\\Entities\\User',
        'profileable_id'   => $user->id,
    ]));
});
