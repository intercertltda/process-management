<?php

$factory->define(\Modules\Users\Entities\Profile::class, function () {
    $faker = \Faker\Factory::create('pt_BR');
    $genres = ['masc', 'fem', 'other'];
    $docsType = ['nothing', 'id', 'cpf', 'cnpj'];

    $docsTypeSelected = $docsType[rand(0, 1)];

    switch ($docsTypeSelected) {
        case 'id':
            $getDoc = $faker->rg(false);
        break;
        case 'cpf':
            $getDoc = $faker->cpf(false);
        break;
        case 'cnpj':
            $getDoc = $faker->cnpj(false);
        break;
        default:
            $getDoc = '';
        break;
    }

    return [
        'phone' => $faker->phoneNumber,
        'genre' => $genres[rand(0, 2)],

        'type_other_document' => $docsTypeSelected,
        'other_document'      => $getDoc
    ];
});

$factory->afterCreating(\Modules\Users\Entities\Profile::class, function ($profile, $faker) {
    $profile->address()->save(factory(\Modules\Addresses\Entities\Address::class)->create([
        'addressable_type' => 'Modules\\Users\\Entities\\Profile',
        'addressable_id'   => $profile->id,
    ]));
});
