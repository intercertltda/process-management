<?php

namespace Modules\Users\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Addresses\Entities\Address;
use Modules\Calls\Entities\Call;
use Modules\Companies\Entities\Company;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use ModelHelpers,
        SoftDeletes,
        Notifiable,
        Reference,
        HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'enabled', 'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function profile()
    {
        return $this->morphOne(Profile::class, 'profileable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function calls()
    {
        return $this->morphMany(Call::class, 'callable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
