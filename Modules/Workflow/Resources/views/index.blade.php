@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content', [
        'widgetTitle' => __('Workflow management for your company'),
        'widgetButtons' => [
            [
                'label' => __('Settings'),
                'url' => '#',
                'icon' => 'la la-wrench'
            ],
            [
                'label' => __('Order Phases'),
                'url' => '#',
                'icon' => 'la la-sort-alpha-asc'
            ]
        ]
    ])

        <ul class="nav nav-tabs" id="example-one" role="tablist">
            <li class="nav-item phase-complete">
                <a class="nav-link" id="base-tab-documents" data-toggle="tab" href="#tab-documents" role="tab" aria-controls="tab-documents" aria-selected="false"><i class="la la-check"></i> Etapas</a>
            </li>
            <li class="nav-item phase-in-process">
                <a class="nav-link active" id="base-tab-notifications" data-toggle="tab" href="#tab-sda" role="tab" aria-controls="tab-notifications" aria-selected="true"><i class="la la-refresh la-spin"></i> Notificações</a>
            </li>

            <li class="nav-item phase-unlock">
                <a class="nav-link disabled" id="base-tab-sda" data-toggle="tab" href="#tab-sda" role="tab" aria-controls="tab-sda" aria-selected="false"><i class="la la-close"></i> Notificações</a>
            </li>
        </ul>

        <div class="tab-content pt-3">
            <div class="tab-pane fade" id="tab-documents" role="tabpanel" aria-labelledby="base-tab-documents">
                test
            </div>
            <div class="tab-pane fade show active" id="tab-sda" role="tabpanel" aria-labelledby="base-tab-sda">
                Nam sagittis nec velit vitae molestie. Donec eget luctus sem. Nullam tortor tortor, consequat id lacinia nec, tempus in diam. Phasellus vel molestie purus, ac hendrerit risus. Phasellus non purus lacinia purus fringilla hendrerit. Sed pharetra odio a ante volutpat aliquam id non lacus.
            </div>
        </div>

    @endcomponent
@endsection

@push('inline-styles')
    <style>
        .nav-tabs .phase-complete a,
        .nav-tabs .phase-complete i {
            color: #28a745 !important;
        }

        .nav-tabs .phase-complete a.active {
            border-bottom: 2px solid #28a745 !important;
        }

        .nav-tabs .phase-in-process a,
        .nav-tabs .phase-in-process i {
            color: #5bc0de !important;
        }

        .nav-tabs .phase-in-process a.active {
            border-bottom: 2px solid #5bc0de !important;
        }

    </style>
@endpush
