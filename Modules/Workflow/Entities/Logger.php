<?php

namespace Modules\Workflow\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Logger extends Model
{
    use SoftDeletes;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'tenant';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workflow_logger';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['workflow_id', 'type', 'content'];


}
