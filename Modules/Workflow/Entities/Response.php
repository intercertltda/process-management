<?php

namespace Modules\Workflow\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Companies\Entities\Company;
use Modules\Files\Entities\File;
use Modules\Processes\Entities\Process;
use Modules\Users\Entities\User;

class Response extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'tenant';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workflow_responses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'process_id', 'data', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function process()
    {
        return $this->hasOne(Process::class, 'id', 'process_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function client()
    {
        return $this->morphTo('response_able');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function who_approve()
    {
        return $this->hasOne(User::class, 'id', 'who_approved_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany(File::class, 'attachable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logger()
    {
        return $this->hasMany(Logger::class, 'id', 'workflow_id');
    }

    /**
     * @return mixed
     */
    public function getDataAttribute()
    {
        return json_decode($this->attributes['data']);
    }
}
