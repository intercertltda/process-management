<?php

namespace Modules\Workflow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Company;
use Modules\Workflow\Facades\Workflow;

class WorkflowController extends Controller
{
    public function __construct()
    {
        $this->middleware(['is:admin,representative']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->company) {
            $company = Company::reference($request->company);
            if (!$company->first()) {
                abort(404);
            }
        } else {
            $company = $request->user()->companies->first();
        }

        return view('workflow::index', [
            'title'      => __('Workflow'),
            'subtitle'   => null,

            'workflow' => new Workflow($company),

            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard'
                ],
                [
                    'title'   => __('Workflow'),
                    'current' => true,
                    'icon'    => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('workflow::create', [
            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('workflow.index'),
                    'title'   => __('Workflow'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request): Response
    {
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('workflow::show', [
            'title'      => __('Show'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('workflow.index'),
                    'title'   => __('Workflow'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Show'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        return view('workflow::edit', [
            'title'      => __('Edit'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('workflow.index'),
                    'title'   => __('Workflow'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Edit'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request): Response
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request): Response
    {
    }
}
