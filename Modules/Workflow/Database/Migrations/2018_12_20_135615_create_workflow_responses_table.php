<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateWorkflowResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('workflow_responses', function (Blueprint $table) {
            $table->increments('id');

            // Company response
            $table->integer('workflow_id')
                ->unsigned();

            // Process response
            $table->integer('process_id')
                ->unsigned();

            // Who approve this workflow
            $table->integer('who_approved_id')
                ->unsigned()
                ->nullable();

            // Data of response
            $table->longText('data');

            /*
             * status of workflow
             *
             * types:
             * unlock - Not showing in workflow
             * in-progress - In progress
             * standby - process paused
             * refused - process refused because data incorrect
             * finished - process complete
             */
            $table->string('status')->default('unlock');

            // Who response this workflow
            $table->nullableMorphs('response_able');

            $table->timestamp('finished_at')->nullable();

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('workflow_responses', function (Blueprint $table) {
            $table->foreign('workflow_id')
                ->references('id')
                ->on('workflow')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('process_id')
                ->references('id')
                ->on('processes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('who_approved_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('workflow_responses');
    }
}
