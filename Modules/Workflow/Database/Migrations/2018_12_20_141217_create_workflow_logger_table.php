<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateWorkflowLoggerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('workflow_logger', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('workflow_id')
                ->unsigned();

            /*
             * Types:
             * comment - Comment of approve user
             * attr - Commend of uploader
             */
            $table->string('type')->default('comment');
            $table->longText('content');

            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('workflow_logger', function (Blueprint $table) {
            $table->foreign('workflow_id')
                ->references('id')
                ->on('workflow')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('workflow_logger');
    }
}
