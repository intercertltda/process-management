<?php

namespace Modules\Workflow\Facades;

use Modules\Companies\Entities\Company;

class Workflow
{
    /**
     * @var \Modules\Companies\Entities\Company
     */
    public $company;

    /**
     * Workflow constructor.
     *
     * @param \Modules\Companies\Entities\Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

}
