<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('leads', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')
                ->unsigned()
                ->nullable();

            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('interested');

            $table->uuid('token');

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('leads', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('leads');
    }
}
