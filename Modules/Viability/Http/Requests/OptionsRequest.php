<?php

namespace Modules\Viability\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OptionsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'question' => 'required',
            'option'   => 'required',
            'type'     => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'question.required' => 'A questão é obrigatória.',
            'option.required'   => 'A opção é obrigatória.',
            'type.required'     => 'O tipo é obrigatório.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
