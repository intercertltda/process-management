<?php

namespace Modules\Viability\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'   => 'required',
            'company' => 'required',
            'type'    => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required'   => 'O titúlo é obrigatório.',
            'company.required' => 'A empresa é obrigatória.',
            'type.required'    => 'O tipo é obrigatório.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
