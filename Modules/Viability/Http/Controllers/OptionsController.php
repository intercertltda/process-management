<?php

namespace Modules\Viability\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Viability\Entities\Options;
use Modules\Viability\Entities\Question;
use Modules\Viability\Http\Requests\OptionsRequest;

class OptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $question = Question::reference($request->reference);

        if (!$question) {
            throw new \Exception('Questão não encontrada.');
        }

        return response()
            ->json($question->options, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OptionsRequest $request
     *
     * @return Response
     */
    public function store(OptionsRequest $request)
    {
        $question = Question::reference($request->question);

        $option = new Options();

        $option->question_id = $question->id;
        $option->option = $request->option;

        if ($option->save()) {
            return response()
                ->json([
                    'message' => __('Successfully created content.'),
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.'),
                ], 400);
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $option = Options::reference($request->reference);

        if (!$option) {
            return response()
                ->json([
                    'message' => __('Resource not found.'),
                ], 404);
        }

        return response()
            ->json($option->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OptionsRequest $request
     *
     * @return Response
     */
    public function update(OptionsRequest $request)
    {
        $option = Options::reference($request->reference);

        $option->questions = $request->option;

        if ($option->save()) {
            return response()
                ->json([
                    'message' => __('Successfully created content.'),
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be saved.'),
                ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $option = Options::reference($request->reference);

        if ($option->delete()) {
            return response()
                ->json([
                    'message' => __('Item moved to trash.'),
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be moved to the trash.'),
                ], 400);
        }
    }
}
