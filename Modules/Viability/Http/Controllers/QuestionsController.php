<?php

namespace Modules\Viability\Http\Controllers;

use App\Classes\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Companies\Entities\Company;
use Modules\Companies\Facades\MyCompanies;
use Modules\Viability\Entities\Question;
use Modules\Viability\Http\Requests\QuestionsRequest;

class QuestionsController extends Controller
{
    /**
     * MainController constructor.
     */
    public function __construct()
    {
//        $this->middleware(['forbid:responsible']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (is_client()) {
            $items = MyCompanies::collection('questions');
        } else {
            $items = Question::all();
        }

        return view('viability::index', [
            'items' => $items,

            'title'      => __('Viability'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Steps'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $companies = is_client() ? MyCompanies::get() : Company::all();
        $treatment = Collection::setKey($companies, 'reference', 'social_name');

        return view('viability::create', [
            'title'    => __('Create'),
            'subtitle' => null,
            'submit'   => __('Create form of viability'),

            'companies' => $treatment,

            'types' => config('viability.types'),

            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param QuestionsRequest $request
     *
     * @return Response
     */
    public function store(QuestionsRequest $request)
    {
        $company = Company::reference($request->company);

        try {
            $via = new Question([
                'title'      => $request->title,
                'company_id' => $company->id,
                'type'       => $request->type,
            ]);

            if ($via->save()) {
                return redirect()
                    ->route('viability.edit', $via->reference)
                    ->with('success', 'Questão criada com sucesso!');
            } else {
                return redirect()
                    ->route('viability.create')
                    ->with('fail', 'Não foi possível criar a questão.')
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            return redirect()
                ->route('viability.create')
                ->with('fail', sprintf('[%s:%s] %s', $exception->getFile(), $exception->getLine(), $exception->getMessage()))
                ->withInput($request->all());
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('viability::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $companies = is_client() ? MyCompanies::get() : Company::all();
        $treatment = Collection::setKey($companies, 'reference', 'social_name');
        $question = Question::reference($request->reference);

        return view('viability::edit', [
            'title'    => __('Edit'),
            'subtitle' => null,
            'submit'   => __('Create form of viability'),

            'item' => $question,

            'companies' => $treatment,
            'types'     => config('viability.types'),

            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            $via = Question::reference($request->reference);

            $via->title = $request->title;
            $via->type = $request->type;

            if ($via->save()) {
                return redirect()
                    ->route('viability.edit', $request->reference)
                    ->with('success', 'Questão atualizada com sucesso!');
            } else {
                return redirect()
                    ->route('viability.edit', $request->reference)
                    ->with('fail', 'Não foi possível atuailizar essa questão.')
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            return redirect()
                ->route('viability.create')
                ->with('fail', sprintf('[%s:%s] %s', $exception->getFile(), $exception->getLine(), $exception->getMessage()))
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $model = Question::reference($request->reference);

        if (!$model->count()) {
            return response()
                ->json(['message' => __('Resource not found.')], 402);
        }

        if ($model->delete()) {
            return response()
                ->json([
                    'message' => __('Item moved to trash.'),
                ], 200);
        } else {
            return response()
                ->json([
                    'message' => __('This item could not be moved to the trash.'),
                ], 402);
        }
    }
}
