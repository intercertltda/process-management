<?php

return [
    'name' => 'Viability',

    'types' => [
        'answer'           => 'Resposta simples',
        'options-unique'   => 'Escolha única',
        'options-multiple' => 'Múltipla escolha',
    ],
];
