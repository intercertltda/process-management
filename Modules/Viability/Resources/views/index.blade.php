@extends('system::layouts.master')
@push('push-buttons')
    <a href="{{ router()->route('viability.create') }}" class="btn btn-success btn-sm"><i class="la la-plus"></i> @lang('Create') @lang('Question')</a>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('system::components.table', [
                'thead' => [
                    'name' => __('Question'),
                    'responses' => __('Responses'),
                    'department' => __('Type'),
                    'modified' => __('Modified At'),
                    'actions-center' => __('Actions')
                ]
            ])
                @foreach ($items as $item)
                    <tr>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->responses->count() }}</td>
                        <td>{{ config('viability.types.' . $item->type) }}</td>
                        <td>{{ $item->updated_at->format('d/m/Y \à\s H:i') }}</td>
                        <td class="td-actions text-center not-print">
                            @include('system::components.actions', [
                                'buttons' => [
                                    'edit' => [
                                        'route' => router()->route('viability.edit', $item->reference),
                                        'class' => '',
                                        'icon' => 'la la-edit',
                                        'label' => __('Edit')
                                    ],

                                    'delete' => [
                                        'route' => '#',
                                        'class' => 'js-confirm-action',
                                        'icon' => 'la la-close',
                                        'label' => __('Delete'),
                                        'attributes' => [
                                            'data-local' => 'viabilidade',
                                            'data-reference' => $item->reference
                                        ]
                                    ]
                                ]
                            ])
                        </td>
                    </tr>
                @endforeach
            @endcomponent
        </div>
    </div>
@endsection
