@extends('system::layouts.master')
@section('content')
    @component('system::components.page-content')
        {{ Form::open([
            'url' => router()->route('viability.store'),
        ]) }}
        <div class="row form-group">
            <div class="col-md-4">
                {{ Form::label('title', __('Title')) }}
                {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'required']) }}
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'company',
                        'title' => __('Select company'),
                        'label' => __('Company'),
                        'list' => $companies,
                        'old' => old('company'),
                        'required' => true,
                        'class' => 'js-select-companies'
                    ]
                ])
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'type',
                        'title' => __('Select type'),
                        'label' => __('Type'),
                        'list' => $types,
                        'old' => old('type'),
                        'required' => true,
                        'class' => 'js-select-type'
                    ]
                ])
                @endcomponent
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Save') <i class="la la-angle-right"></i></button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent
@endsection
