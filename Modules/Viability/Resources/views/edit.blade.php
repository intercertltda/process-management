@extends('system::layouts.master')
@push('push-buttons')
    <a href="javascript:void(0);" data-toggle="modal" data-target="#createOption" class="btn btn-success btn-sm"><i class="la la-plus"></i> @lang('Create') @lang('Option')</a>
@endpush
@section('content')
    @component('system::components.page-content')
        {{ Form::open([
            'url' => router()->route('viability.update', $item->reference),
        ]) }}
        <div class="row form-group">
            <div class="col-md-4">
                {{ Form::label('title', __('Title')) }}
                {{ Form::text('title', $item->title ?? old('title'), ['class' => 'form-control', 'id' => 'title', 'required']) }}
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'company',
                        'title' => __('Select company'),
                        'label' => __('Company'),
                        'list' => $companies,
                        'old' => $item->company->reference ?? old('company'),
                        'required' => true,
                        'class' => 'js-select-companies'
                    ]
                ])
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'type',
                        'title' => __('Select type'),
                        'label' => __('Type'),
                        'list' => $types,
                        'old' => $item->type ?? old('type'),
                        'required' => true,
                        'class' => 'js-select-type'
                    ]
                ])
                @endcomponent
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Save') <i class="la la-angle-right"></i></button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent


    @component('system::components.table', [
        'thead' => [
            'option' => __('Option'),
            'modified' => __('Modified At'),
            'actions-center' => __('Actions')
        ],

        'order' => [
            'col' => 1,
            'type' => 'desc'
        ],

        'exportable' => true,
    ])
        @foreach ($item->options as $op)
            <tr>
                <td>{{ $op->option }}</td>
                <td>{{ $op->updated_at->format('d/m/Y \à\s H:i') }}</td>
                <td class="td-actions text-center not-print">
                    @include('system::components.actions', [
                        'buttons' => [
                            'edit' => [
                                'route' => 'javascript:void(0);',
                                'class' => '',
                                'icon' => 'la la-edit',
                                'label' => __('Edit'),
                                'attributes' => [
                                    'data->reference' => $op->reference,
                                    'data-toggle' => 'modal',
                                    'data-target' => '#updateOption'
                                ]
                            ],

                            'delete' => [
                                'route' => 'javascript:void(0);',
                                'class' => 'js-confirm-action',
                                'icon' => 'la la-close',
                                'label' => __('Delete'),
                                'attributes' => [
                                    'data-local' => 'viabilidade',
                                    'data-reference' => $op->reference
                                ]
                            ]
                        ]
                    ])
                </td>
            </tr>
        @endforeach
    @endcomponent
@endsection

@push('modals')
    <div id="createOption" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cadastre uma Opção para essa questão</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12">
                            {{ Form::text('option', '', ['class' => 'form-control', 'placeholder' => 'Escreva uma frase...', 'id' => 'set-question']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow" data-dismiss="modal">@lang('Cancel')</button>
                    <button id="setQuestion" type="button" class="btn btn-primary">@lang('Insert')</button>
                </div>
            </div>
        </div>
    </div>

    <div id="updateOption" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Opção</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        {{ Form::hidden('reference', '') }}

                        <div class="col-12">
                            {{ Form::text('option', '', ['class' => 'form-control', 'placeholder' => 'Escreva uma frase...', 'id' => 'set-question']) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow" data-dismiss="modal">@lang('Cancel')</button>
                    <button id="setQuestion" type="button" class="btn btn-primary">@lang('Insert')</button>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('inline-scripts')
    <script>
        (($) => {
            'use strict';

            const reference = '{{ $item->reference }}',
                type = '{{ $item->type }}',
                modal = $('#createOption'),
                field = $('#set-question'),

                updateOptionModal = $('#updateOption');

            updateOptionModal.on('shown.bs.modal', (e) => {
                let modal = $(e.currentTarget),
                    reference = $(e.relatedTarget).data('reference'),
                    option = modal.find('[name=option]');

                console.log(reference);

                $.ajax({
                    url: '/api/viability/options/' + reference,
                    method: 'GET',

                    success: (response) => {
                        window.intersig.setMessage(response.message);
                    },

                    error: (err) => {
                        // let response = err.responseJSON;

                        console.log(err);

                        // window.intersig.setMessage(response.message, 'error');
                    }
                });
            });

            $('#setQuestion').click((e) => {
                if (field.val() !== '') {
                    $.ajax({
                        url: '/api/viability/options',
                        method: 'POST',
                        data: {
                            question: reference,
                            option: field.val(),
                            type: type
                        },

                        dataType: 'json',

                        success: (response) => {
                            window.intersig.setMessage(response.message);

                            setTimeout(() => {
                                field.val('');
                                modal.modal('hide');
                            }, 1500);

                            setTimeout(() => {
                                window.location.reload();
                            }, 3000);
                        },

                        error: (err) => {
                            let response = $.parseJSON(err.responseText);
                            console.log(response);

                            window.intersig.setMessage(response.message, 'error');
                        }
                    });
                } else {
                    window.intersig.setMessage('Preencha o campo', 'error');
                }
            });
        })(jQuery);
    </script>
@endpush
