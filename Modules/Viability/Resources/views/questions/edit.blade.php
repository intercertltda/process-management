@extends('system::layouts.master')
@push('push-buttons')
    <a href="javascript:void(0);" data-toggle="modal" data-target="#createOption" class="btn btn-success btn-sm"><i class="la la-plus"></i> @lang('Create') @lang('Option')</a>
@endpush
@section('content')
    @component('system::components.page-content')
        {{ Form::open([
            'url' => route('viability.update', $item->reference),
        ]) }}
        <div class="row form-group">
            <div class="col-md-4">
                {{ Form::label('title', __('Title')) }}
                {{ Form::text('title', $item->title ?? old('title'), ['class' => 'form-control', 'id' => 'title', 'required']) }}
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'company',
                        'title' => __('Select company'),
                        'label' => __('Company'),
                        'list' => $companies,
                        'old' => $item->company->reference ?? old('company'),
                        'required' => true,
                        'class' => 'js-select-companies'
                    ]
                ])
                @endcomponent
            </div>

            <div class="col-md-4">
                @component('system::components.select', [
                    'select' => [
                        'name' => 'type',
                        'title' => __('Select type'),
                        'label' => __('Type'),
                        'list' => $types,
                        'old' => $item->type ?? old('type'),
                        'required' => true,
                        'class' => 'js-select-type'
                    ]
                ])
                @endcomponent
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('Save') <i class="la la-angle-right"></i></button>
            </div>
        </div>
        {{ Form::close() }}
    @endcomponent


    @component('system::components.table', [
        'thead' => [
            'option' => __('Option'),
            'value' => __('Value'),
            'modified' => __('Modified At'),
            'actions-center' => __('Actions')
        ],

        'exportable' => false,
    ])
        @foreach ($item->options as $op)
            <tr>
                <td>{{ $op->option }}</td>
                <td>{{ $op->value }}</td>
                <td>{{ $op->updated_at->format('d/m/Y \à\s H:i') }}</td>
                <td class="td-actions text-center not-print">
                    @include('system::components.actions', [
                        'buttons' => [
                            'edit' => [
                                'route' => route('viability.edit', $op->reference),
                                'class' => '',
                                'icon' => 'la la-edit',
                                'label' => __('Edit')
                            ],

                            'delete' => [
                                'route' => '#',
                                'class' => 'js-confirm-action',
                                'icon' => 'la la-close',
                                'label' => __('Delete'),
                                'attributes' => [
                                    'data-local' => 'viabilidade',
                                    'data-reference' => $op->reference
                                ]
                            ]
                        ]
                    ])
                </td>
            </tr>
        @endforeach
    @endcomponent
@endsection

@push('modals')
    <div id="createOption" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Title</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Donec non lectus nec est porta eleifend. Morbi ut dictum augue, feugiat condimentum est. Pellentesque tincidunt justo nec aliquet tincidunt. Integer dapibus tellus non neque pulvinar mollis. Maecenas dictum laoreet diam, non convallis lorem sagittis nec. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc venenatis lacus arcu, nec ultricies dui vehicula vitae.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-shadow" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
@endpush
