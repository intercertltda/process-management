<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Viability\Entities\Options::class, function (Faker $faker) {
    return [
        'option' => $faker->text(50)
    ];
});
