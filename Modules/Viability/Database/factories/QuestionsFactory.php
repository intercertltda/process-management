<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Viability\Entities\Question::class, function (Faker $faker) {
    return [
        'type'  => 'options-unique',
        'title' => $faker->text(50),
    ];
});

$factory->afterCreating(\Modules\Viability\Entities\Question::class, function ($question, $faker) {
    factory(\Modules\Viability\Entities\Options::class, 5)
        ->create([
            'question_id' => $question->id
        ]);
});
