<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateViabilityResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('viability_responses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('question_id')
                ->unsigned();

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['tenancy'])->table('viability_responses', function (Blueprint $table) {
            $table->foreign('question_id')
                ->references('id')
                ->on('viability_questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('viability_responses');
    }
}
