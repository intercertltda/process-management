<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'viability/options',
    'as'     => 'viability.questions.options.',
], function () {
    Route::get('', 'OptionsController@index')->name('index');
    Route::post('', 'OptionsController@store')->name('store');
    Route::put('{reference}', 'OptionsController@update')->name('update');
    Route::delete('{reference}', 'OptionsController@delete')->name('destroy');
});
