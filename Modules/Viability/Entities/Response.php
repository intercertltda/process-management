<?php

namespace Modules\Viability\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Response extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'viability_responses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'response',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'responseable_id',
        'responseable_type',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function response()
    {
        return $this->morphTo();
    }

    /**
     * @return mixed
     */
    public function getResponseAttribute()
    {
        return json_decode($this->attributes['response']);
    }
}
