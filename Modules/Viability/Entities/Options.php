<?php

namespace Modules\Viability\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Options extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'viability_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['option', 'question_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
