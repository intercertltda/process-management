<?php

namespace Modules\Calls\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Call extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'duration', 'date',
];

    protected $dates = ['date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'type', 'status', 'agent_id', 'client_id', 'collaborator_id',
];

    /**
     * @return MorphTo
     */
    public function callable(): MorphTo
    {
        return $this->morphTo();
    }
}
