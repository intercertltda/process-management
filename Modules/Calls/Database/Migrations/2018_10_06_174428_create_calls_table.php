<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['tenancy'])->create('calls', function (Blueprint $table) {
            $table->increments('id');

            // ['first_contact','validate_information','other_subject']
            $table->string('type')->default('first_contact');

            // ['success','fail']
            $table->string('status')->default('fail');
            $table->string('description')->nullable();
            $table->string('duration', 5);
            $table->timestamp('date');

            $table->morphs('callable');

            $table->uuid('reference')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['tenancy'])->dropIfExists('calls');
    }
}
