<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('check/slug', function () {

    $client = DB::connection('system')
        ->table('clients')
        ->where('slug', request()->slug)
        ->first();

    if ($client) {
        return response()
            ->json([
                'message' => __('Url is available')
            ], 200);
    } else {
        return response()
            ->json([
                'message' => __('Url is not available')
            ], 400);
    }
})->name('check.slug');

use Modules\System\Facades\RouteFacade;

RouteFacade::api([
    'prefix' => 'clients',
    'middleware' => ['auth', 'role:admin|directors'],
], 'ApiController');
