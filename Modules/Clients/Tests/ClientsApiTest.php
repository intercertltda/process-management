<?php

namespace Modules\Clients\Tests;

use Faker\Factory;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Clients\Entities\Client;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Reference;
use Tests\TestCase;

class ClientsApiTest extends TestCase
{
    use WithoutMiddleware;

    protected $faker;

    public function setUp()
    {
        $this->faker = Factory::create('pt_BR');

        parent::setUp();
    }

    /**
     * Testing resource listing.
     *
     * @return void
     */
    public function testIndex()
    {
        $this->withoutMiddleware();

        $response = $this->get(api()->route('clients.index'));
        $thatObject = json_decode($response->content());

        $this->assertIsArray($thatObject);
        $response->assertStatus(200);
    }

    /**
     * Testing store resource.
     *
     * @return void
     */
    public function testStore()
    {
        $this->withoutMiddleware();

        $clientName = $this->faker->company;

        $response = $this->post(api()->route('clients.store'), [
            'client' => [
                'name' => $clientName,
                'slug' => str_slug($clientName),
                'document' => $this->faker->cpf(),
                'email' => $this->faker->unique()->email,
                'phone' => $this->faker->phoneNumber,
            ],

            'representative' => [
                'name' => $this->faker->name,
                'document' => $this->faker->cpf(),
                'email' => $this->faker->unique()->email,
                'phone' => $this->faker->phoneNumber,
            ],

            'address' => [
                'street'            => $this->faker->streetName,
                'neighborhood'      => $this->faker->citySuffix,
                'number'            => $this->faker->buildingNumber,
                'code_postal'       => $this->faker->postcode,
                'city'              => $this->faker->city,
                'state'             => $this->faker->stateAbbr,
            ]
        ]);

        $response->assertStatus(200);
    }

    /**
     * Testing showing element by reference.
     *
     * @return void
     */
    public function testShow()
    {
        $this->withoutMiddleware();

        $client = Client::all()->last();

        $response = $this->get(api()->route('clients.show', ['reference' => $client->reference]));

        $response->assertStatus(200);
    }
}
