<?php

namespace Modules\Clients\Tests;

use Faker\Factory;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Clients\Entities\Client;
use Tests\TestCase;

class ClientsAppTest extends TestCase
{
    use WithoutMiddleware;

    protected $faker;

    public function setUp()
    {
        $this->faker = Factory::create('pt_BR');

        parent::setUp();
    }

    /**
     * Testing store resource.
     *
     * @return void
     */
    public function testStore()
    {
        $this->withoutMiddleware();

        $route = router()->route('clients.store', ['subdomain' => 'app']);
        $clientName = $this->faker->company;

        $response = $this->post($route, [
            'client' => [
                'name' => $clientName,
                'slug' => str_slug($clientName),
                'document' => $this->faker->cpf(),
                'email' => $this->faker->unique()->email,
                'phone' => $this->faker->phoneNumber,
            ],

            'representative' => [
                'name' => $this->faker->name,
                'document' => $this->faker->cpf(),
                'email' => $this->faker->unique()->email,
                'phone' => $this->faker->phoneNumber,
            ],

            'address' => [
                'street'            => $this->faker->streetName,
                'neighborhood'      => $this->faker->citySuffix,
                'number'            => $this->faker->buildingNumber,
                'code_postal'       => $this->faker->postcode,
                'city'              => $this->faker->city,
                'state'             => $this->faker->stateAbbr,
            ]
        ]);

        $response->assertSessionHas('success', __('requests.success.created'));
    }

    /**
     * Testing update resource.
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->withoutMiddleware();
        $data = Client::all()->last();
        $route = router()->route('clients.update', [
            'subdomain' => 'app',
            'reference' => $data->reference
        ]);

        $clientName = $this->faker->company;

        $response = $this->put($route, [
            '_method' => 'PUT',
            'client' => [
                'name' => $clientName,
                'slug' => str_slug($clientName),
                'document' => $this->faker->cpf(),
                'email' => $this->faker->unique()->email,
                'phone' => $this->faker->phoneNumber,
            ],

            'representative' => [
                'name' => $this->faker->name,
                'document' => $this->faker->cpf(),
                'email' => $this->faker->unique()->email,
                'phone' => $this->faker->phoneNumber,
            ],

            'address' => [
                'street'            => $this->faker->streetName,
                'neighborhood'      => $this->faker->citySuffix,
                'number'            => $this->faker->buildingNumber,
                'code_postal'       => $this->faker->postcode,
                'city'              => $this->faker->city,
                'state'             => $this->faker->stateAbbr,
            ]
        ]);

        $response->assertSessionHas('success', __('requests.success.updated'));
    }

    /**
     * Testing deleting resource.
     *
     * @return void
     */
    public function testDelete()
    {
        $this->withoutMiddleware();
        $data = Client::all()->last();
        $route = router()->route('clients.delete', [
            'subdomain' => 'app',
            'reference' => $data->reference
        ]);

        $response = $this->delete($route, [
            '_method' => 'DELETE'
        ]);

        $response->assertSessionHas('success', __('requests.success.deleted'));
    }
}
