<?php

namespace Modules\Clients\Http\Requests;

use App\Rules\ValidateDocument;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Addresses\Http\Requests\AddressArray;

class ClientRequest extends FormRequest
{
    protected $requests = [
        AddressArray::class,
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'client.name'     => 'required',
            'client.document' => ['required', new ValidateDocument()],
            'client.phone'    => 'required',
            'client.email'    => 'required|email',
            'client.slug'     => 'required|regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/',

            'representative.name'     => 'required',
            'representative.document' => ['required', new ValidateDocument()],
            'representative.phone'    => 'required',
            'representative.email'    => 'required|email',
        ];

        foreach ($this->requests as $source) {
            $rules = array_merge($rules, (new $source())->rules());
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        $messages = [
            'client.name.required'     => 'O nome do cliente é obrigatório.',
            'client.document.required' => 'O CNPJ é obrigatório.',
            'client.document.valid'    => 'O CNPJ precisa ser válido.',
            'client.phone.required'    => 'O telefone é obrigatório.',
            'client.email.required'    => 'O e-mail é obrigatório.',
            'client.email.email'       => 'O e-mail precisa ser válido.',
            'client.slug.required'     => 'A url precisa ser válido.',
            'client.slug.regex'        => 'A url precisa ser válido.',

            'representative.name.required'     => 'O nome do representante é obrigatório.',
            'representative.document.required' => 'O CPF do representante é obrigatório.',
            'representative.document.valid'    => 'O CPF do representante precisa ser válido.',
            'representative.phone.required'    => 'O telefone do representante é obrigatório.',
            'representative.email.required'    => 'O e-mail do representante é obrigatório.',
            'representative.email.email'       => 'O e-mail do representante precisa ser válido.',
        ];

        foreach ($this->requests as $source) {
            $messages = array_merge($messages, (new $source())->messages());
        }

        return $messages;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
