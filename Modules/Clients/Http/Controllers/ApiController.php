<?php

namespace Modules\Clients\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Clients\Entities\Client;
use Modules\Clients\Entities\Representative;
use Modules\Clients\Http\Requests\ClientRequest;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()
            ->json(Client::all()->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('clients::create', [
            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => route('clients.index'),
                    'title'   => __('Clients'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientRequest $request
     *
     * @return Response
     */
    public function store(ClientRequest $request)
    {
        try {
            $client = new Client();

            $client->name = $request->input('client.name');
            $client->email = $request->input('client.email');
            $client->slug = $request->input('client.slug');
            $client->document = $request->input('client.document');
            $client->phone = $request->input('client.phone');

            if ($client->save()) {
                $client->representative()->create($request->input('representative'));
                $client->address()->create($request->input('address'));

                return response()
                    ->json([
                        'message' => __('requests.success.created'),
                        'data'    => $client
                    ], 200);
            } else {
                return response()
                    ->json([
                        'message' => __('requests.fail.created'),
                        'data'    => $client
                    ], 400);
            }
        } catch (\Exception $exception) {
            \Log::error($exception);

            $message = env('APP_DEBUG') ? $exception->getMessage() : __('requests.errors.exception');

            return response()
                ->json([
                    'message' => $message,
                    'data'    => $client
                ], 400);
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $client = Client::reference($request->reference);

        if (!$client) {
            return response()
                ->json([
                    'message' => __('requests.notfound')
                ], 404);
        }

        return response()
            ->json($client->toArray(), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientRequest $request
     *
     * @return Response
     */
    public function update(ClientRequest $request)
    {
        try {
            $client = Client::reference($request->reference);

            $client->name = $request->input('client.name');
            $client->email = $request->input('client.email');
            $client->slug = $request->input('client.slug');
            $client->document = $request->input('client.document');
            $client->phone = $request->input('client.phone');

            $representative = Representative::find($client->representative->id);

            $representative->name = $request->input('representative.name');
            $representative->email = $request->input('representative.email');
            $representative->document = $request->input('representative.document');
            $representative->phone = $request->input('representative.phone');

            if ($client->save() && $representative->save()) {
                $client->address->update($request->input('address'));

                return router()
                    ->redirect('clients.edit', ['reference' => $client->reference])
                    ->with('success', __('requests.success.updated'));
            } else {
                return router()
                    ->redirect('clients.edit', ['reference' => $client->reference])
                    ->with('fail', __('requests.fail.updated'))
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            \Log::error($exception);

            $message = env('APP_DEBUG') ? $exception->getMessage() : __('requests.errors.exception');

            return router()
                ->redirect('clients.create')
                ->with('fail', $message)
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $client = Client::reference($request->reference);

        if ($client->delete()) {
            return router()
                ->redirect('clients.index')
                ->with('success', __('requests.success.deleted'));
        } else {
            return redirect()
                ->route('clients.index')
                ->with('fail', __('requests.fail.deleted'));
        }
    }
}
