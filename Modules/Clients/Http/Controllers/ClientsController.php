<?php

namespace Modules\Clients\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Clients\Entities\Client;
use Modules\Clients\Entities\Representative;
use Modules\Clients\Http\Requests\ClientRequest;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('clients::index', [
            'title'      => __('Clients'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard'
                ],
                [
                    'title'   => __('Clients'),
                    'current' => true,
                    'icon'    => null
                ]
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('clients::create', [
            'title'      => __('Create'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('clients.index'),
                    'title'   => __('Clients'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Create'),
                    'current' => true,
                    'icon'    => null,
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ClientRequest $request
     *
     * @return Response
     */
    public function store(ClientRequest $request)
    {
        try {
            $client = new Client();

            $client->name = $request->input('client.name');
            $client->email = $request->input('client.email');
            $client->slug = $request->input('client.slug');
            $client->document = $request->input('client.document');
            $client->phone = $request->input('client.phone');

            if ($client->save()) {
                $client->representative()->create($request->input('representative'));
                $client->address()->create($request->input('address'));

                return router()
                    ->redirect('clients.edit', ['reference' => $client->reference])
                    ->with('success', __('requests.success.created'));
            } else {
                return router()
                    ->redirect('clients.create')
                    ->with('fail', __('requests.fail.created'))
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            \Log::error($exception);

            $message = env('APP_DEBUG') ? $exception->getMessage() : __('requests.errors.exception');

            return router()
                ->redirect('clients.create')
                ->with('fail', $message)
                ->withInput($request->all());
        }
    }

    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('clients::show', [
            'title'      => __('Show'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('clients.index'),
                    'title'   => __('Clients'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Show'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        return view('clients::edit', [
            'title'      => __('Edit'),
            'subtitle'   => null,
            'breadcrumb' => [
                [
                    'url'     => router()->route('dashboard.index'),
                    'title'   => __('Dashboard'),
                    'current' => false,
                    'icon'    => 'mdi mdi-view-dashboard',
                ],
                [
                    'url'     => router()->route('clients.index'),
                    'title'   => __('Clients'),
                    'current' => false,
                    'icon'    => null,
                ],
                [
                    'title'   => __('Edit'),
                    'current' => true,
                    'icon'    => null,
                ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientRequest $request
     *
     * @return Response
     */
    public function update(ClientRequest $request)
    {
        try {
            $client = Client::reference($request->reference);

            $client->name = $request->input('client.name');
            $client->email = $request->input('client.email');
            $client->slug = $request->input('client.slug');
            $client->document = $request->input('client.document');
            $client->phone = $request->input('client.phone');

            $representative = Representative::find($client->representative->id);

            $representative->name = $request->input('representative.name');
            $representative->email = $request->input('representative.email');
            $representative->document = $request->input('representative.document');
            $representative->phone = $request->input('representative.phone');

            if ($client->save() && $representative->save()) {
                $client->address->update($request->input('address'));

                return router()
                    ->redirect('clients.edit', ['reference' => $client->reference])
                    ->with('success', __('requests.success.updated'));
            } else {
                return router()
                    ->redirect('clients.edit', ['reference' => $client->reference])
                    ->with('fail', __('requests.fail.updated'))
                    ->withInput($request->all());
            }
        } catch (\Exception $exception) {
            \Log::error($exception);

            $message = env('APP_DEBUG') ? $exception->getMessage() : __('requests.errors.exception');

            return router()
                ->redirect('clients.create')
                ->with('fail', $message)
                ->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $client = Client::reference($request->reference);

        if ($client->delete()) {
            return router()
                ->redirect('clients.index')
                ->with('success', __('requests.success.deleted'));
        } else {
            return redirect()
                ->route('clients.index')
                ->with('fail', __('requests.fail.deleted'));
        }
    }
}
