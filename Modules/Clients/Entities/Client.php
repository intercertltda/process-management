<?php

namespace Modules\Clients\Entities;

use App\Traits\ModelHelpers;
use App\Traits\Reference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Addresses\Entities\Address;

class Client extends Model
{
    use ModelHelpers,
        SoftDeletes,
        Reference;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'system';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'document', 'phone', 'slug', 'status'];

    /**
     * @return HasOne
     */
    public function representative(): HasOne
    {
        return $this->hasOne(Representative::class, 'client_id', 'id');
    }

    /**
     * @return MorphOne
     */
    public function address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable');
    }
}
