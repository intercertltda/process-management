@php
    $name = old('name');
    $document = old('document-cpf');
    $email = old('email');
    $phone = old('phone');
    $login = old('login');
    $genre = old('genre');
@endphp
@extends('system::layouts.master')
@section('content')
    {{ Form::open([
        'url' => router()->route('users.store'),
    ]) }}
    {{ csrf_field() }}
        @component('system::components.page-content', [
            'widgetTitle' => __('Data of Client')
        ])

            <div class="row form-group">
                <div class="col-12 col-md-3">
                    {{ Form::label('client_name', __('Name')) }}
                    {{ Form::text('client[name]', old('client.name'), ['class' => 'form-control', 'id' => 'client_name', 'required']) }}
                    <p class="help-block">URL da empresa: <strong id="print-slug">{name}</strong>.{{ config('app.domain') }}</p>
                </div>

                <div class="col-12 col-md-3">
                    {{ Form::label('client_document', __('Document').' (CNPJ)') }}
                    {{ Form::text('client[document]', old('client.document'), ['class' => 'form-control document-cnpj', 'id' => 'client_document', 'required']) }}
                </div>

                <div class="col-12 col-md-3">
                    {{ Form::label('client_phone', __('Phone')) }}
                    {{ Form::text('client[phone]', old('client.phone'), ['class' => 'form-control phone', 'id' => 'client_phone', 'required']) }}
                </div>

                <div class="col-12 col-md-3">
                    {{ Form::label('client_email', __('E-mail')) }}
                    {{ Form::email('client[email]', old('client.email'), ['class' => 'form-control', 'id' => 'client_email', 'required']) }}
                </div>
            </div>
        @endcomponent

        @component('system::components.page-content', [
            'widgetTitle' => __('Legal Representative')
        ])

            <div class="row form-group">
                <div class="col-12 col-md-3">
                    {{ Form::label('representative_name', __('Name')) }}
                    {{ Form::text('representative[name]', old('representative.name'), ['class' => 'form-control', 'id' => 'representative_name', 'required']) }}
                </div>

                <div class="col-12 col-md-3">
                    {{ Form::label('representative_document', __('Document').' (CPF)') }}
                    {{ Form::text('representative[document]', old('representative.document'), ['class' => 'form-control document-cpf', 'id' => 'representative_document', 'required']) }}
                </div>

                <div class="col-12 col-md-3">
                    {{ Form::label('representative_phone', __('Phone')) }}
                    {{ Form::text('representative[phone]', old('representative.phone'), ['class' => 'form-control phone', 'id' => 'representative_phone', 'required']) }}
                </div>

                <div class="col-12 col-md-3">
                    {{ Form::label('representative_email', __('E-mail')) }}
                    {{ Form::email('representative[email]', old('representative.email'), ['class' => 'form-control', 'id' => 'representative_email', 'required']) }}
                </div>
            </div>
        @endcomponent


        @component('system::components.page-content', [
            'widgetTitle' => __('Data of Address')
        ])
            @component('addresses::form', [
                'isArray' => true
            ])
            @endcomponent
        @endcomponent


        <div class="row form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">Cadastrar</button>
            </div>
        </div>
    {{ Form::close() }}
@endsection
@push('styles')
    <style>
        .radio-multiple label {
            margin-bottom: 0;
            line-height: 40px;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('components/inputmask/dist/jquery.inputmask.bundle.min.js') }}"></script>
@endpush

@push('inline-scripts')
    <script type="text/javascript">
        (($) => {
            'use strict';

            $('.phone').inputmask({mask:'(99) [9] 9999-9999'});
            $('.document-cnpj').inputmask({mask:'99.999.999/9999-99'});
            $('.document-cpf').inputmask({mask:'999.999.999-99'});

            $('#client_name').on('keyup keydown', (e) => {
                let slug = $(e.currentTarget).val();

                $('#print-slug').text(slug.slugify());
            }).on('change', (e) => {
                let slug = $(e.currentTarget).val();

                $.ajax({
                    url: '{{ api()->route('clients.check.slug') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        slug: slug.slugify()
                    },
                    success: (response) => {

                        window.intersig.setMessage(response.message);
                    },
                    error: (res) => {
                        let response = res.responseJSON;

                        window.intersig.setMessage(response.message, 'error');
                    }
                });
            });
        })(jQuery);
    </script>
@endpush
