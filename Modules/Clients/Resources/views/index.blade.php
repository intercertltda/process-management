@extends('system::layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @php
                            $thead['name'] = 'Nome';
                            $thead['document'] = 'Documento';
                            $thead['email'] = 'E-mail';
                            $thead['actions'] = 'Ações';
                        @endphp
                        @component('system::components.table', [
                            'thead' => $thead
                        ])
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ raank()->document($item->document)->masked() }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td class="text-center">
                                        <a href="{{ router()->route('users.edit', ['reference' => $item->reference]) }}" class="btn btn-primary"><i class="la la-edit"></i></a>
                                        <a href="{{ router()->route('users.show', ['reference' => $item->reference]) }}" class="btn btn-info"><i class="la la-eye"></i></a>
                                        <a href="#" data-local="colaboradores" data-reference="{{ $item->reference }}" class="btn btn-danger js-confirm-action"><i class="la la-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
