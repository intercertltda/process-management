<?php

namespace Modules\Clients\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Clients\Entities\Client;

class ClientsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (config('tenancy.clients') as $client) {
            $modelClient = new Client();

            $modelClient->setConnection('system');

            $modelClient->name = $client['name'];
            $modelClient->slug = $client['slug'];
            $modelClient->document = '0000000000';

            $modelClient->save();
        }
    }
}
