<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Tenancy\Facades\TenancyFacade as Tenancy;

class CreateClientsRepresentativeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Tenancy::migrate(['system'])->create('clients_representative', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')
                ->unsigned();

            $table->string('name');
            $table->string('document');
            $table->string('phone');
            $table->string('email');

            $table->softDeletes();
            $table->timestamps();
        });

        Tenancy::migrate(['system'])->table('clients_representative', function (Blueprint $table) {
            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Tenancy::migrate(['system'])->dropIfExists('clients_representative');
    }
}
