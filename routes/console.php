<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Schema;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('test:mail', function () {
    \Mail::send('emails::templates.tests', [
        'title' => 'Testando envio de e-mails.',
    ], function ($m) {
        $m->to('raank92@gmail.com', 'Felipe Rank')
            ->subject('Teste de envio');
    });
});

Artisan::command('clear:data', function () {
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
});

Artisan::command('drop:table {table}', function ($table) {
    Schema::disableForeignKeyConstraints();
    Schema::drop($table);
    Schema::enableForeignKeyConstraints();

    $this->info(sprintf('Table {%s} is dropped', $table));
});

Artisan::command('drop:session {key}', function ($key) {
    session()->pull($key);
    session()->flush();

    $this->info('Session as dropped.');
});

Artisan::command('foreign:off', function () {
    Schema::disableForeignKeyConstraints();
});

Artisan::command('foreign:on', function () {
    Schema::enableForeignKeyConstraints();
});

Artisan::command('truncate:table {table}', function ($table) {
    $this->call('foreign:off');

    \DB::table($table)->truncate();

    $tableUsers = sprintf('%s_users', $table);
    if (Schema::hasTable($tableUsers)) {
        \DB::table($tableUsers)->truncate();
    }

    $this->call('foreign:on');

    $this->info("Truncate table \"{$table}\" as successful!");
});

Artisan::command('drop:table {table}', function ($table) {
    $this->call('foreign:off');

    \Schema::drop($table);

    $this->call('foreign:on');

    $this->info("Dropped table \"{$table}\" as successful!");
});

Artisan::command('db:truncate', function () {
    $this->call('foreign:off');

    $tables = ['addresses', 'companies', 'departments', 'phases', 'processes', 'profiles', 'users', 'viability_questions', 'viability_options', 'viability_responses'];

    foreach ($tables as $table) {
        if (Schema::hasTable($table)) {
            \DB::table($table)->truncate();
        }

        $tableUsers = sprintf('%s_users', $table);
        if (Schema::hasTable($tableUsers)) {
            \DB::table($tableUsers)->truncate();
        }
    }

    $this->call('foreign:on');

    $this->info("Truncate all tables as successful!");
});

Artisan::command('test:mail', function () {
    Mail::to('raank92@gmail.com', 'Felipe Rank')
        ->send(new \App\Mail\TestMail());
});

