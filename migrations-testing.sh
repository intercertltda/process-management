#!/usr/bin/env bash

php artisan module:migrate Addresses --env=testing
php artisan module:migrate Auth --env=testing
php artisan module:migrate Permissions --seed --env=testing
php artisan module:migrate Users --seed --env=testing
php artisan module:migrate Departments --seed --env=testing
php artisan module:migrate Manager --seed --env=testing
